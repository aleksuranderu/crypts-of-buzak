﻿using AutoMapper;

namespace App.DAL.EF;

public class AutomapperConfig : Profile
{
    public AutomapperConfig()
    {
        CreateMap<App.DAL.DTO.Character, App.Domain.Character>().ReverseMap();
        CreateMap<App.DAL.DTO.Dungeon, App.Domain.Dungeon>().ReverseMap();
        CreateMap<App.DAL.DTO.Effect, App.Domain.Effect>().ReverseMap();
        CreateMap<App.DAL.DTO.Item, App.Domain.Item>().ReverseMap();
        CreateMap<App.DAL.DTO.Identity.AppUser, App.Domain.Identity.AppUser>().ReverseMap();
        CreateMap<App.DAL.DTO.Skill, App.Domain.Skill>().ReverseMap();
        CreateMap<App.DAL.DTO.Monster, App.Domain.Monster>().ReverseMap();
        CreateMap<App.DAL.DTO.MonsterList, App.Domain.MonsterList>().ReverseMap();
        CreateMap<App.DAL.DTO.InventoryItem, App.Domain.InventoryItem>().ReverseMap();
        CreateMap<App.DAL.DTO.Friend, App.Domain.Friend>().ReverseMap();
        CreateMap<App.DAL.DTO.DungeonZone, App.Domain.DungeonZone>().ReverseMap();
        CreateMap<App.DAL.DTO.DropTableItem, App.Domain.DropTableItem>().ReverseMap();
        CreateMap<App.DAL.DTO.GroupStatus.InRaidSkill, App.Domain.GroupStatus.InRaidSkill>().ReverseMap();
        CreateMap<App.DAL.DTO.GroupStatus.InRaidItem, App.Domain.GroupStatus.InRaidItem>().ReverseMap();
        CreateMap<App.DAL.DTO.GroupStatus.InRaidEffect, App.Domain.GroupStatus.InRaidEffect>().ReverseMap();
        CreateMap<App.DAL.DTO.GroupStatus.GroupStatus, App.Domain.GroupStatus.GroupStatus>().ReverseMap();
        CreateMap<App.DAL.DTO.GroupStatus.BattleCharacter, App.Domain.GroupStatus.BattleCharacter>().ReverseMap();
        CreateMap<App.DAL.DTO.SkillBook, App.Domain.SkillBook>().ReverseMap();
    }
}