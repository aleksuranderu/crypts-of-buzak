﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts.Base;
using Base.DAL.EF;

namespace App.DAL.EF.Repositories;

public class MonsterRepository : BaseEntityRepository<App.DAL.DTO.Monster, App.Domain.Monster, int, ApplicationDbContext>, IMonsterRepository
{
    public MonsterRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.Monster, App.Domain.Monster> mapper) : base(dbContext, mapper)
    {
    }
}