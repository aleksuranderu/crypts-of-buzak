﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class CharacterRepository : BaseEntityRepository<App.DAL.DTO.Character, App.Domain.Character, Guid, ApplicationDbContext>, ICharacterRepository
{
    public CharacterRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.Character, App.Domain.Character> mapper) : base(dbContext, mapper)
    {
        
    }

    public async Task<Character?> FirstOrDefaultSensitiveAsync(Guid userId, bool noTracking = true)
    {
        return Mapper.Map(
            await CreateQuery(noTracking)
                .Include(u => u.InventoryItems)!
                .ThenInclude(u => u.Item)
                .Include(x => x.SkillBook)!
                .ThenInclude(x => x.Skill)
                .FirstOrDefaultAsync(x => x.Id == userId));
    }
}