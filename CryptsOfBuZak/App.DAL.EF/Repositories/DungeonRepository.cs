﻿using App.Contracts.DAL;
using App.Domain;
using Base.Contracts;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;
using Dungeon = App.DAL.DTO.Dungeon;

namespace App.DAL.EF.Repositories;

public class DungeonRepository : BaseEntityRepository<App.DAL.DTO.Dungeon, App.Domain.Dungeon, int, ApplicationDbContext>, IDungeonRepository
{
    public DungeonRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.Dungeon, App.Domain.Dungeon> mapper) : base(dbContext, mapper)
    {
        
    }
    
    public async Task<Dungeon?> FirstOrDefaultByIdAsync(int dungeonId, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Include(d => d.DungeonZones)
            .ThenInclude(d => d.MonsterLists)
            .ThenInclude(m => m.Monster)
            .Where(d => d.Id == dungeonId);
        
        return Mapper.Map(await query.FirstOrDefaultAsync());
    }
}