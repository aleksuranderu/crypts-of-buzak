﻿using App.Contracts.DAL;
using App.DAL.DTO.GroupStatus;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class InRaidSkillRepository : BaseEntityRepository<App.DAL.DTO.GroupStatus.InRaidSkill, App.Domain.GroupStatus.InRaidSkill, Guid, ApplicationDbContext>, IInRaidSkillRepository
{
    public InRaidSkillRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.GroupStatus.InRaidSkill, App.Domain.GroupStatus.InRaidSkill> mapper) : base(dbContext, mapper)
    {
        
    }
    public async Task<IEnumerable<App.DAL.DTO.GroupStatus.InRaidSkill>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Include(x => x.Skill)
            .Where(x => x.GroupStatusId == groupStatusId);
        return (await query.ToListAsync()).Select(x=> Mapper.Map(x)!);
    }
}