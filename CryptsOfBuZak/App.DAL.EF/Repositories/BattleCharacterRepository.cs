﻿using App.Contracts.DAL;
using App.Domain.GroupStatus;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class BattleCharacterRepository : BaseEntityRepository<App.DAL.DTO.GroupStatus.BattleCharacter, App.Domain.GroupStatus.BattleCharacter, Guid, ApplicationDbContext>, IBattleCharacterRepository
{
    public BattleCharacterRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.GroupStatus.BattleCharacter, App.Domain.GroupStatus.BattleCharacter> mapper) : base(dbContext, mapper)
    {
    }
    
    public async Task<IEnumerable<App.DAL.DTO.GroupStatus.BattleCharacter>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Where(x => x.GroupStatusId == groupStatusId);
        return (await query.ToListAsync()).Select(x=> Mapper.Map(x)!);
    }
}