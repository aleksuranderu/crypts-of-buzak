﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class DungeonZoneRepository: BaseEntityRepository<App.DAL.DTO.DungeonZone, App.Domain.DungeonZone, int, ApplicationDbContext>, IDungeonZoneRepository
{
    public DungeonZoneRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.DungeonZone, App.Domain.DungeonZone> mapper) : base(dbContext, mapper)
    {
    }

    public async Task<IEnumerable<DungeonZone>> GetAllByDungeonIdAsync(int dungeonId, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Where(x => x.DungeonId == dungeonId);
        return (await query.ToListAsync()).Select(x=> Mapper.Map(x)!);
    }
}