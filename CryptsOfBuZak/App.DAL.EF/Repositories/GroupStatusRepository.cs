﻿using App.Contracts.DAL;
using App.DAL.DTO.GroupStatus;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class GroupStatusRepository : BaseEntityRepository<App.DAL.DTO.GroupStatus.GroupStatus, App.Domain.GroupStatus.GroupStatus, Guid, ApplicationDbContext>, IGroupStatusRepository
{
    public GroupStatusRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.GroupStatus.GroupStatus, App.Domain.GroupStatus.GroupStatus> mapper) : base(dbContext, mapper)
    {
        
    }

    // Safe one, use this one in case if you're not sure that everything is not null.
    public async Task<GroupStatus?> GetByLeaderIdAsync(Guid userid, bool noTracking = true)
    {
        
        return Mapper.Map(
            await CreateQuery(noTracking)
                .FirstOrDefaultAsync(c => c.GroupLeaderId == userid));
    }
    
    //May throw exception, if anything is null.
    public async Task<GroupStatus?> GetByLeaderIdSensitive(Guid userid, bool noTracking = true)
    {
        
        return Mapper.Map(
                await CreateQuery(noTracking)
                    .Include(x => x.Dungeon)
                    .ThenInclude(x => x!.DungeonZones)
                    .Include(x => x.BattleCharacters)
                    .Include(x => x.GroupLeader)
                    .ThenInclude(x => x!.InventoryItems)!
                    .ThenInclude(x => x.Item)
                    .Include(x => x.InRaidEffects)!
                    .ThenInclude(x => x.Effect)
                    .Include(x => x.InRaidSkills)!
                    .ThenInclude(x => x.Skill)
                    .Include(x => x.InRaidItems)!
                    .ThenInclude(i => i.Item)
                    .Include(x => x.GroupLeader)
                    .ThenInclude(x => x!.SkillBook)!
                    .ThenInclude( x => x.Skill)
                    .FirstOrDefaultAsync(c => c.GroupLeaderId == userid));
    }

    //May throw exception, if anything is null.
    public async Task<GroupStatus?> GetByLeaderIdFightPrepared(Guid userid, bool noTracking = true)
    {
        return Mapper.Map(
            await CreateQuery(noTracking)
                .Include(x => x.BattleCharacters)
                .Include(x => x.InRaidEffects)!
                .ThenInclude(e => e.Effect)
                .Include(x => x.InRaidItems)!
                .ThenInclude(i => i.Item)
                .Include(s => s.InRaidSkills)
                .Include(x => x.Dungeon)
                .ThenInclude(d => d!.DungeonZones)!
                .ThenInclude(d => d.MonsterLists)!
                .ThenInclude(m => m.Monster)
                .ThenInclude(m => m!.DropTable)!
                .ThenInclude(d => d.Item)
                
                .FirstOrDefaultAsync(c => c.GroupLeaderId == userid));
    }
}