﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts.Base;
using Base.DAL.EF;

namespace App.DAL.EF.Repositories;

public class MonsterListRepository : BaseEntityRepository<App.DAL.DTO.MonsterList, App.Domain.MonsterList, int, ApplicationDbContext>, IMonsterListRepository
{
    public MonsterListRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.MonsterList, App.Domain.MonsterList> mapper) : base(dbContext, mapper)
    {
    }
}