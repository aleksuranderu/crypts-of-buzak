﻿using App.Contracts.DAL;
using App.Contracts.DAL.Identity;
using App.DAL.DTO.Identity;
using Base.Contracts.Base;
using Base.DAL.EF;

namespace App.DAL.EF.Repositories.Identity;

public class AppUserRepository : BaseEntityRepository<App.DAL.DTO.Identity.AppUser, App.Domain.Identity.AppUser, Guid, ApplicationDbContext>, IAppUserRepository
{
    public AppUserRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.Identity.AppUser, App.Domain.Identity.AppUser> mapper) : base(dbContext, mapper)
    {
    }
}