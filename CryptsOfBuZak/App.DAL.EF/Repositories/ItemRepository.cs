﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class ItemRepository : BaseEntityRepository<App.DAL.DTO.Item, App.Domain.Item, int, ApplicationDbContext>, IItemRepository
{
    public ItemRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.Item, App.Domain.Item> mapper) : base(dbContext, mapper)
    {
    }
    
    // Returns sellable (not null) items that can be sold at this level.
    public async Task<IEnumerable<Item>> GetAllAvailableByLevel(int level, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Include(e => e.Effect)
            .Where(x => x.MinLevelToBuy != null && x.MinLevelToBuy.Value <= level);
        return (await query.ToListAsync()).Select(x=> Mapper.Map(x)!);
    }
}