﻿using App.Contracts.DAL;
using App.DAL.DTO.GroupStatus;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class InRaidEffectRepository : BaseEntityRepository<App.DAL.DTO.GroupStatus.InRaidEffect, App.Domain.GroupStatus.InRaidEffect, Guid, ApplicationDbContext>, IInRaidEffectRepository
{
    public InRaidEffectRepository(ApplicationDbContext dbContext, IMapper<InRaidEffect, Domain.GroupStatus.InRaidEffect> mapper) : base(dbContext, mapper)
    {
    }
    
    public async Task<IEnumerable<App.DAL.DTO.GroupStatus.InRaidEffect>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Include(e => e.Effect)
            .Where(x => x.GroupStatusId == groupStatusId);
        return (await query.ToListAsync()).Select(x=> Mapper.Map(x)!);
    }
}