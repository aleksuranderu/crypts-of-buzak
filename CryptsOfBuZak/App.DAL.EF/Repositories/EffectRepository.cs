﻿using App.Contracts.DAL;
using App.Domain;
using Base.Contracts;
using Base.Contracts.Base;
using Base.DAL.EF;

namespace App.DAL.EF.Repositories;

public class EffectRepository : BaseEntityRepository<App.DAL.DTO.Effect, App.Domain.Effect, int, ApplicationDbContext>, IEffectRepository
{ 
    public EffectRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.Effect, App.Domain.Effect> mapper) : base(dbContext, mapper)
    {
    }
}