﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class SkillBookRepository: BaseEntityRepository<App.DAL.DTO.SkillBook, App.Domain.SkillBook, int, ApplicationDbContext>, ISkillBookRepository
{
    public SkillBookRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.SkillBook, App.Domain.SkillBook> mapper) : base(dbContext, mapper)
    {
    }
    
    public async Task<IEnumerable<App.DAL.DTO.SkillBook>> GetAllByCharacterIdAsync(Guid characterId, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Include(x => x.Skill)
            .Where(x => x.CharacterId == characterId);
        return (await query.ToListAsync()).Select(x=> Mapper.Map(x)!);
    }
    
    public bool Exists(Guid characterId, int skillId)
    {
        return RepoDbSet.Any(f => f.CharacterId.Equals(characterId) && f.SkillId.Equals(skillId));
    }

    public async Task<SkillBook?> FirstOrDefaultAsync(Guid characterId, int skillId, bool noTracking = true)
    {
        return Mapper.Map(
            await CreateQuery(noTracking).
            FirstOrDefaultAsync(
                a => 
                    a.CharacterId.Equals(characterId) &&
                    a.SkillId.Equals(skillId)));
    }
}