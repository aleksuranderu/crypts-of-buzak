﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts.Base;
using Base.DAL.EF;

namespace App.DAL.EF.Repositories;

public class DropTableItemRepository : BaseEntityRepository<App.DAL.DTO.DropTableItem, App.Domain.DropTableItem, Guid, ApplicationDbContext>, IDropTableItemRepository
{
    public DropTableItemRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.DropTableItem, App.Domain.DropTableItem> mapper) : base(dbContext, mapper)
    {
    }
}