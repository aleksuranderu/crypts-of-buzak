﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class FriendRepository : BaseEntityRepository<App.DAL.DTO.Friend, App.Domain.Friend, int, ApplicationDbContext>, IFriendRepository
{
    public FriendRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.Friend, App.Domain.Friend> mapper) : base(dbContext, mapper)
    {
    }

    public bool Exists(Guid characterId, Guid friendId)
    {
        return RepoDbSet.Any(f => f.CharacterId.Equals(characterId) && f.FriendId.Equals(friendId));
    }
    
    public async Task<IEnumerable<App.DAL.DTO.Friend>> GetAllByIdAsync(Guid characterId, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Include(u => u.FriendCharacter)
            .Where(c => c.CharacterId == characterId);
        return (await query.ToListAsync()).Select(x=> Mapper.Map(x)!);
    }

    public async Task<Friend?> FirstOrDefaultAsync(Guid characterId, Guid friendId, bool noTracking = true)
    {
        return Mapper.Map(
            await CreateQuery(noTracking).
                FirstOrDefaultAsync(
                    a => 
                        a.CharacterId.Equals(characterId) &&
                        a.FriendId.Equals(friendId)));
    }
}