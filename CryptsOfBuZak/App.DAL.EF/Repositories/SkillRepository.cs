﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts.Base;
using Base.DAL.EF;

namespace App.DAL.EF.Repositories;

public class SkillRepository : BaseEntityRepository<App.DAL.DTO.Skill, App.Domain.Skill, int, ApplicationDbContext>, ISkillRepository
{
    public SkillRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.Skill, App.Domain.Skill> mapper) : base(dbContext, mapper)
    {
    }
    
    
}