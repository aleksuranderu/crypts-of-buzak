﻿using App.Contracts.DAL;
using App.DAL.DTO;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class InventoryItemRepository : BaseEntityRepository<App.DAL.DTO.InventoryItem, App.Domain.InventoryItem, int, ApplicationDbContext>, IInventoryItemRepository
{
    public InventoryItemRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.InventoryItem, App.Domain.InventoryItem> mapper) : base(dbContext, mapper)
    {
    }

    public async Task<InventoryItem?> FirstOrDefaultAsync(Guid characterId, int itemId, bool noTracking = true)
    {
        return Mapper.Map(
            await CreateQuery(noTracking)
                .Include(x => x.Item)
                .FirstOrDefaultAsync(
                    a => 
                        a.CharacterId.Equals(characterId) &&
                        a.ItemId.Equals(itemId)));
    }

    public async Task<IEnumerable<App.DAL.DTO.InventoryItem>> GetAllByCharacterIdAsync(Guid characterId, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Include(x => x.Item)
            .Where(x => x.CharacterId == characterId);
        return (await query.ToListAsync()).Select(x=> Mapper.Map(x)!);
    }
    
    public bool Exists(Guid itemOwnerId, int itemId)
    {
        return RepoDbSet.Any(x => x.CharacterId == itemOwnerId && x.ItemId == itemId);
    }

    public async Task<InventoryItem?> RemoveAsync(Guid characterId, int itemId)
    {
        var entity = await FirstOrDefaultAsync(characterId, itemId);
        if (entity == null)
        {
            // TODO: implement custom exception for entity not found
            throw new NullReferenceException($"Entity {typeof(InventoryItem).Name} with item id {itemId} for character {characterId} was not found");
        }
        return Remove(entity);
    }
}