﻿using App.Contracts.DAL;
using App.DAL.DTO.GroupStatus;
using Base.Contracts.Base;
using Base.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace App.DAL.EF.Repositories;

public class InRaidItemRepository : BaseEntityRepository<App.DAL.DTO.GroupStatus.InRaidItem, App.Domain.GroupStatus.InRaidItem, Guid, ApplicationDbContext>, IInRaidItemRepository
{
    public InRaidItemRepository(ApplicationDbContext dbContext, IMapper<App.DAL.DTO.GroupStatus.InRaidItem, App.Domain.GroupStatus.InRaidItem> mapper) : base(dbContext, mapper)
    {
    }
    
    public async Task<IEnumerable<App.DAL.DTO.GroupStatus.InRaidItem>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true)
    {
        var query = CreateQuery(noTracking);
        query = query
            .Include(x => x.Item)
            .Where(x => x.GroupStatusId == groupStatusId);
        return (await query.ToListAsync()).Select(x=> Mapper.Map(x)!);
    }
}