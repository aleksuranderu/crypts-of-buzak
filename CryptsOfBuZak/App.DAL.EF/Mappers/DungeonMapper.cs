﻿using App.DAL.DTO;
using AutoMapper;
using Base.Contracts;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class DungeonMapper : BaseMapper<App.DAL.DTO.Dungeon, App.Domain.Dungeon>
{
    public DungeonMapper(IMapper mapper) : base(mapper)
    {
    }
}