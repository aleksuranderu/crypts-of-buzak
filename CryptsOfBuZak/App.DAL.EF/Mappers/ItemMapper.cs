﻿using App.DAL.DTO;
using AutoMapper;
using Base.Contracts;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class ItemMapper : BaseMapper<App.DAL.DTO.Item, App.Domain.Item>
{
    public ItemMapper(IMapper mapper) : base(mapper)
    {
    }
}