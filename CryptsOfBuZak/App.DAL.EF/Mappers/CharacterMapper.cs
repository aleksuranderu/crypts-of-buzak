﻿using App.DAL.DTO;
using AutoMapper;
using Base.Contracts;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class CharacterMapper : BaseMapper<App.DAL.DTO.Character, App.Domain.Character>
{
    public CharacterMapper(IMapper mapper) : base(mapper)
    {
    }
}