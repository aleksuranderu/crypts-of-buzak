﻿using App.DAL.DTO;
using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class MonsterMapper: BaseMapper<App.DAL.DTO.Monster, Domain.Monster>
{
    public MonsterMapper(IMapper mapper) : base(mapper)
    {
    }
}