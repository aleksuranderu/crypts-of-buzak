﻿using App.DAL.DTO;
using AutoMapper;
using Base.Contracts;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class EffectMapper : BaseMapper<App.DAL.DTO.Effect, App.Domain.Effect>
{
    public EffectMapper(IMapper mapper) : base(mapper)
    {
    }
}