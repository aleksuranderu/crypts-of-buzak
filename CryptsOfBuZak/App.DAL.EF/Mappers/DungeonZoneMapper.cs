﻿using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class DungeonZoneMapper: BaseMapper<App.DAL.DTO.DungeonZone, App.Domain.DungeonZone>
{
    public DungeonZoneMapper(IMapper mapper) : base(mapper)
    {
    }
}