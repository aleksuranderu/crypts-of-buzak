﻿using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class DropTableItemMapper: BaseMapper<App.DAL.DTO.DropTableItem, App.Domain.DropTableItem>
{
    public DropTableItemMapper(IMapper mapper) : base(mapper)
    {
    }
}