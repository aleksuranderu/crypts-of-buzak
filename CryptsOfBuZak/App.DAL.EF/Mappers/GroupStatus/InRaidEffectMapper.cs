﻿using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers.GroupStatus;

public class InRaidEffectMapper: BaseMapper<App.DAL.DTO.GroupStatus.InRaidEffect, App.Domain.GroupStatus.InRaidEffect>
{
    public InRaidEffectMapper(IMapper mapper) : base(mapper)
    {
    }
}