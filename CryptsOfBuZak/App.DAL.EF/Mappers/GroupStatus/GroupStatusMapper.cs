﻿using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers.GroupStatus;

public class GroupStatusMapper: BaseMapper<App.DAL.DTO.GroupStatus.GroupStatus, App.Domain.GroupStatus.GroupStatus>
{
    public GroupStatusMapper(IMapper mapper) : base(mapper)
    {
    }
}