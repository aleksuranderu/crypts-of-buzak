﻿using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers.GroupStatus;

public class BattleCharacterMapper: BaseMapper<App.DAL.DTO.GroupStatus.BattleCharacter, App.Domain.GroupStatus.BattleCharacter>
{
    public BattleCharacterMapper(IMapper mapper) : base(mapper)
    {
    }
}