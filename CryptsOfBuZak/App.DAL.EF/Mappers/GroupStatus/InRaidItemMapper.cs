﻿using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers.GroupStatus;

public class InRaidItemMapper: BaseMapper<App.DAL.DTO.GroupStatus.InRaidItem, App.Domain.GroupStatus.InRaidItem>
{
    public InRaidItemMapper(IMapper mapper) : base(mapper)
    {
    }
}