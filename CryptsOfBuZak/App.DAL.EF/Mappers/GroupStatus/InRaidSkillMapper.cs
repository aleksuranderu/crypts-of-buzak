﻿using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers.GroupStatus;

public class InRaidSkillMapper: BaseMapper<App.DAL.DTO.GroupStatus.InRaidSkill, App.Domain.GroupStatus.InRaidSkill>
{
    public InRaidSkillMapper(IMapper mapper) : base(mapper)
    {
    }
}