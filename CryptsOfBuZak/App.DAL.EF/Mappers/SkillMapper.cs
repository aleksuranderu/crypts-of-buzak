﻿using App.Domain;
using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class SkillMapper: BaseMapper<App.DAL.DTO.Skill, App.Domain.Skill>
{
    public SkillMapper(IMapper mapper) : base(mapper)
    {
    }
}