﻿using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class FriendMapper: BaseMapper<App.DAL.DTO.Friend, App.Domain.Friend>
{
    public FriendMapper(IMapper mapper) : base(mapper)
    {
    }
}