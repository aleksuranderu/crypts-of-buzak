﻿using App.Domain;
using App.Domain.GroupStatus;
using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class InventoryItemMapper: BaseMapper<App.DAL.DTO.InventoryItem, App.Domain.InventoryItem>
{
    public InventoryItemMapper(IMapper mapper) : base(mapper)
    {
    }
}