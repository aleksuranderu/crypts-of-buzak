﻿using App.Domain;
using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class MonsterListMapper: BaseMapper<App.DAL.DTO.MonsterList, App.Domain.MonsterList>
{
    public MonsterListMapper(IMapper mapper) : base(mapper)
    {
    }
}