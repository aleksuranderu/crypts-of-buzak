﻿using AutoMapper;
using Base.DAL;

namespace App.DAL.EF.Mappers;

public class SkillBookMapper : BaseMapper<App.DAL.DTO.SkillBook, App.Domain.SkillBook>
{
    public SkillBookMapper(IMapper mapper) : base(mapper)
    {
    }
}