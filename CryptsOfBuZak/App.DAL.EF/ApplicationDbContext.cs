﻿using App.Domain;
using App.Domain.GroupStatus;
using App.Domain.Identity;
using Base.Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace App.DAL.EF;

public class ApplicationDbContext : IdentityDbContext<AppUser, AppRole, Guid>
{
    public DbSet<RefreshToken> RefreshTokens { get; set; } = default!;
    public DbSet<Character> Characters { get; set; } = default!;
    public DbSet<Effect> Effects { get; set; } = default!;
    public DbSet<Skill> Skills { get; set; } = default!;
    public DbSet<Item> Items { get; set; } = default!;
    public DbSet<Monster> Monsters { get; set; } = default!;
    public DbSet<MonsterList> MonsterLists { get; set; } = default!;
    public DbSet<DungeonZone> DungeonZones { get; set; } = default!;
    public DbSet<Dungeon> Dungeons { get; set; } = default!;
    public DbSet<InRaidEffect> InRaidEffects { get; set; } = default!;
    public DbSet<InRaidItem> InRaidItems { get; set; } = default!;
    public DbSet<InRaidSkill> InRaidSkills { get; set; } = default!;
    public DbSet<BattleCharacter> BattleCharacters { get; set; } = default!;
    public DbSet<GroupStatus> GroupStatuses { get; set; } = default!;
    public DbSet<InventoryItem> InventoryItems { get; set; } = default!;
    public DbSet<DropTableItem> DropTableItems { get; set; } = default!;
    public DbSet<Friend> Friends { get; set; } = default!;
    public DbSet<SkillBook> SkillBook { get; set; } = default!;

    protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
    {
        base.ConfigureConventions(configurationBuilder);
        
        // if InMemory database then configure converter for every LangStr property.
        if (Database.ProviderName == "Microsoft.EntityFrameworkCore.InMemory")
        {
            configurationBuilder
                .Properties<LangStr>()
                .HaveConversion<LangStrConverter>();
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        if (Database.ProviderName == "Microsoft.EntityFrameworkCore.InMemory")
        {
            // FixEntitiesForInMemory(modelBuilder);
        }


        
        modelBuilder.Entity<Friend>()
            .HasKey(k => new {k.CharacterId, k.FriendId});

        modelBuilder.Entity<Friend>()
            .HasOne(f => f.FriendCharacter)
            .WithMany(c => c.FriendList)
            .HasForeignKey(f => f.FriendId);

        modelBuilder.Entity<Friend>()
            .HasOne(f => f.Character)
            .WithMany()
            .HasForeignKey(f => f.CharacterId);
        
        modelBuilder.Entity<Skill>()
            .HasOne(s => s.Effect)
            .WithMany()
            .HasForeignKey(s => s.EffectId);
        
        modelBuilder.Entity<SkillBook>()
            .HasKey(sb => new {sb.CharacterId, sb.SkillId});

        modelBuilder.Entity<SkillBook>()
            .HasOne(f => f.Character)
            .WithMany(c => c.SkillBook)
            .HasForeignKey(f => f.CharacterId);

        modelBuilder.Entity<SkillBook>()
            .HasOne(f => f.Skill)
            .WithMany()
            .HasForeignKey(f => f.SkillId);
        
        modelBuilder.Entity<Item>(it =>
            {
                it.HasOne(i => i.Effect)
                    .WithMany()
                    .HasForeignKey(i => i.EffectId);

                it.HasOne(i => i.Skill)
                    .WithMany()
                    .HasForeignKey(i => i.SkillId);
            }
        );

        modelBuilder.Entity<MonsterList>()
            .HasOne(m => m.Monster)
            .WithMany()
            .HasForeignKey(m => m.MonsterId);

        modelBuilder.Entity<DungeonZone>()
            .HasMany(d => d.MonsterLists)
            .WithOne(m => m.DungeonZone)
            .HasForeignKey(m => m.DungeonZoneId);

        modelBuilder.Entity<DungeonZone>()
            .HasOne(d => d.Dungeon)
            .WithMany(d => d.DungeonZones)
            .HasForeignKey(d => d.DungeonId);
        
        modelBuilder.Entity<BattleCharacter>()
            .HasOne(b => b.GroupStatus)
            .WithMany(g => g.BattleCharacters)
            .HasForeignKey(b => b.GroupStatusId);
        
        modelBuilder.Entity<GroupStatus>()
            .HasOne(g => g.Dungeon)
            .WithMany()
            .HasForeignKey(g => g.DungeonId);

        modelBuilder.Entity<GroupStatus>()
            .HasOne(g => g.GroupLeader)
            .WithMany()
            .HasForeignKey(g => g.GroupLeaderId);
        
        modelBuilder.Entity<InRaidEffect>()
            .HasOne(e => e.Effect)
            .WithMany()
            .HasForeignKey(e => e.EffectId);

        modelBuilder.Entity<InRaidEffect>()
            .HasOne(b => b.GroupStatus)
            .WithMany(g => g.InRaidEffects)
            .HasForeignKey(b => b.GroupStatusId);
        
        modelBuilder.Entity<InventoryItem>()
            .HasKey(i => new {i.CharacterId, i.ItemId});
        
        modelBuilder.Entity<InventoryItem>()
            .HasOne(i => i.Item)
            .WithMany()
            .HasForeignKey(i => i.ItemId);
        
        modelBuilder.Entity<InventoryItem>()
            .HasOne(i => i.Character)
            .WithMany(c => c.InventoryItems)
            .HasForeignKey(i => i.CharacterId);
        
        modelBuilder.Entity<InRaidItem>()
            .HasOne(i => i.Item)
            .WithMany()
            .HasForeignKey(i => i.ItemId);
        
        modelBuilder.Entity<InRaidItem>()
            .HasOne(b => b.GroupStatus)
            .WithMany(g => g.InRaidItems)
            .HasForeignKey(b => b.GroupStatusId);
        
        modelBuilder.Entity<InRaidSkill>()
            .HasOne(s => s.Skill)
            .WithMany()
            .HasForeignKey(s => s.SkillId);
        
        modelBuilder.Entity<InRaidSkill>()
            .HasOne(b => b.GroupStatus)
            .WithMany(g => g.InRaidSkills)
            .HasForeignKey(b => b.GroupStatusId);

        modelBuilder.Entity<DropTableItem>()
            .HasOne(d => d.Item)
            .WithMany()
            .HasForeignKey(d => d.ItemId);
    }
    
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public override int SaveChanges()
    {
        FixEntities(this);
        return base.SaveChanges();
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        FixEntities(this);
        return base.SaveChangesAsync(cancellationToken);
    }

    private void FixEntities(ApplicationDbContext context)
    {
        var dateProperties = context.Model.GetEntityTypes()
            .SelectMany(t => t.GetProperties())
            .Where(p => p.ClrType == typeof(DateTime))
            .Select(z => new
            {
                ParentName = z.DeclaringEntityType.Name,
                PropertyName = z.Name
            });

        var editedEntitiesInTheDbContextGraph = context.ChangeTracker.Entries()
            .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified)
            .Select(x => x.Entity);
        

        foreach (var entity in editedEntitiesInTheDbContextGraph)
        {
            var entityFields = dateProperties.Where(d => d.ParentName == entity.GetType().FullName);

            foreach (var property in entityFields)
            {
                var prop = entity.GetType().GetProperty(property.PropertyName);

                if (prop == null)
                    continue;

                var originalValue = prop.GetValue(entity) as DateTime?;
                if (originalValue == null)
                    continue;

                prop.SetValue(entity, DateTime.SpecifyKind(originalValue.Value, DateTimeKind.Utc));
            }
        }

    }
}

public class LangStrConverter : ValueConverter<LangStr, string>
{
    public LangStrConverter()
        : base(
            v => SerialiseLangStr(v),
            v => DeserializeLangStr(v))
    {
    }
    private static string SerialiseLangStr(LangStr lStr) => System.Text.Json.JsonSerializer.Serialize(lStr);
    private static LangStr DeserializeLangStr(string jsonStr) =>
        System.Text.Json.JsonSerializer.Deserialize<LangStr>(jsonStr) ?? new LangStr();
}