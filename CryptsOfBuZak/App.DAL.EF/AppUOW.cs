﻿using App.Contracts.DAL;
using App.Contracts.DAL.Identity;
using App.DAL.EF.Mappers;
using App.DAL.EF.Mappers.GroupStatus;
using App.DAL.EF.Mappers.Identity;
using App.DAL.EF.Repositories;
using App.DAL.EF.Repositories.Identity;
using AutoMapper;
using Base.DAL.EF;
using DAL.App;

namespace App.DAL.EF;

public class AppUOW : BaseUOW<ApplicationDbContext>, IAppUnitOfWork
{
    private readonly AutoMapper.IMapper _mapper;
    public AppUOW(ApplicationDbContext dbContext, AutoMapper.IMapper mapper) : base(dbContext)
    {
        _mapper = mapper;
    }
    
    private IAppUserRepository? _appUsers;

    public virtual IAppUserRepository AppUsers =>
        _appUsers ??= new AppUserRepository(UOWDbContext, new AppUserMapper(_mapper));
    
    private IDungeonRepository? _dungeons;
    public virtual IDungeonRepository Dungeons =>
        _dungeons ??= new DungeonRepository(UOWDbContext, new DungeonMapper(_mapper));
    
    private IEffectRepository? _effects;

    public virtual IEffectRepository Effects =>
        _effects ??= new EffectRepository(UOWDbContext, new EffectMapper(_mapper));
    
    private IItemRepository? _items;

    public virtual IItemRepository Items =>
        _items ??= new ItemRepository(UOWDbContext, new ItemMapper(_mapper));
    
    private ICharacterRepository? _characters;
    public virtual ICharacterRepository Characters =>
        _characters ??= new CharacterRepository(UOWDbContext, new CharacterMapper(_mapper));

    private ISkillRepository? _skills;
    public virtual ISkillRepository Skills =>
        _skills ??= new SkillRepository(UOWDbContext, new SkillMapper(_mapper));

    private IMonsterRepository? _monsters;
    public virtual IMonsterRepository Monsters =>
        _monsters ??= new MonsterRepository(UOWDbContext, new MonsterMapper(_mapper));

    private IMonsterListRepository? _monsterLists;
    public virtual IMonsterListRepository MonsterLists =>
        _monsterLists ??= new MonsterListRepository(UOWDbContext, new MonsterListMapper(_mapper));

    private IInventoryItemRepository? _inventoryItems;
    public virtual IInventoryItemRepository InventoryItems =>
        _inventoryItems ??= new InventoryItemRepository(UOWDbContext, new InventoryItemMapper(_mapper));

    private IFriendRepository? _friends;
    public virtual IFriendRepository Friends =>
        _friends ??= new FriendRepository(UOWDbContext, new FriendMapper(_mapper));

    private IDungeonZoneRepository? _dungeonZones;
    public virtual IDungeonZoneRepository DungeonZones =>
        _dungeonZones ??= new DungeonZoneRepository(UOWDbContext, new DungeonZoneMapper(_mapper));

    private IDropTableItemRepository? _dropTableItems;
    public virtual IDropTableItemRepository DropTableItems =>
        _dropTableItems ??= new DropTableItemRepository(UOWDbContext, new DropTableItemMapper(_mapper));

    private IInRaidSkillRepository? _inRaidSkills;
    public virtual IInRaidSkillRepository InRaidSkills =>
        _inRaidSkills ??= new InRaidSkillRepository(UOWDbContext, new InRaidSkillMapper(_mapper));

    private IInRaidItemRepository? _inRaidItems;
    public virtual IInRaidItemRepository InRaidItems =>
        _inRaidItems ??= new InRaidItemRepository(UOWDbContext, new InRaidItemMapper(_mapper));

    private IInRaidEffectRepository? _inRaidEffects;
    public virtual IInRaidEffectRepository InRaidEffects =>
        _inRaidEffects ??= new InRaidEffectRepository(UOWDbContext, new InRaidEffectMapper(_mapper));

    private IGroupStatusRepository? _groupStatuses;
    public virtual IGroupStatusRepository GroupStatuses =>
        _groupStatuses ??= new GroupStatusRepository(UOWDbContext, new GroupStatusMapper(_mapper));

    private IBattleCharacterRepository? _battleCharacters;
    public virtual IBattleCharacterRepository BattleCharacters =>
        _battleCharacters ??= new BattleCharacterRepository(UOWDbContext, new BattleCharacterMapper(_mapper));

    private ISkillBookRepository? _skillBook;
    public virtual ISkillBookRepository SkillBooks =>
        _skillBook ??= new SkillBookRepository(UOWDbContext, new SkillBookMapper(_mapper));

}