﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IInRaidSkillRepository : IEntityRepository<App.DAL.DTO.GroupStatus.InRaidSkill, Guid>
{
    Task<IEnumerable<App.DAL.DTO.GroupStatus.InRaidSkill>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true);
}