﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IEffectRepository : IEntityRepository<App.DAL.DTO.Effect, int>
{
    
}