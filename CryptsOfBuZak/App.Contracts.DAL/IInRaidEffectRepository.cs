﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IInRaidEffectRepository : IEntityRepository<App.DAL.DTO.GroupStatus.InRaidEffect, Guid>
{
    Task<IEnumerable<App.DAL.DTO.GroupStatus.InRaidEffect>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true);
}