﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IInRaidItemRepository: IEntityRepository<App.DAL.DTO.GroupStatus.InRaidItem, Guid>
{
    Task<IEnumerable<App.DAL.DTO.GroupStatus.InRaidItem>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true);
}