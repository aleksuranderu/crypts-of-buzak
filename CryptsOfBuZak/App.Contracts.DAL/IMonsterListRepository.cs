﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IMonsterListRepository : IEntityRepository<App.DAL.DTO.MonsterList, int>
{
    
}