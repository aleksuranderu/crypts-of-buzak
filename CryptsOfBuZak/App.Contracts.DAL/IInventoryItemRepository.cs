﻿using Base.Contracts.DAL;
using App.DAL.DTO;

namespace App.Contracts.DAL;

public interface IInventoryItemRepository: IEntityRepository<App.DAL.DTO.InventoryItem, int>
{
    Task<InventoryItem?> FirstOrDefaultAsync(Guid characterId, int itemId, bool noTracking = true);
    Task<IEnumerable<App.DAL.DTO.InventoryItem>> GetAllByCharacterIdAsync(Guid characterId, bool noTracking = true);
    bool Exists(Guid ItemOwnerId, int itemId);
    
    Task<InventoryItem?> RemoveAsync(Guid characterId, int itemId);
}