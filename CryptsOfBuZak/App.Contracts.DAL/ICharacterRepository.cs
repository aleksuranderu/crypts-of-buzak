﻿using App.DAL.DTO;
using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface ICharacterRepository : IEntityRepository<App.DAL.DTO.Character, Guid>
{
    // Task<IEnumerable<App.DAL.DTO.Character>> GetAllAsync(Guid userId, bool noTracking = true);
    Task<Character?> FirstOrDefaultSensitiveAsync(Guid userId, bool noTracking = true);
}

public interface ICharacterRepositoryCustom<TEntity>
{
    Task<IEnumerable<TEntity>> GetAllAsync(Guid userId, bool noTracking = true);
}