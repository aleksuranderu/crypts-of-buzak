﻿using App.Domain;
using Base.Contracts.DAL;
using Item = App.DAL.DTO.Item;

namespace App.Contracts.DAL;

public interface IItemRepository : IEntityRepository<App.DAL.DTO.Item, int>
{
    Task<IEnumerable<Item>> GetAllAvailableByLevel(int level, bool noTracking = true);
}