﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IBattleCharacterRepository : IEntityRepository<App.DAL.DTO.GroupStatus.BattleCharacter, Guid>
{
    Task<IEnumerable<App.DAL.DTO.GroupStatus.BattleCharacter>> GetAllByGroupStatusIdAsync(Guid groupStatusId,
        bool noTracking = true);
}