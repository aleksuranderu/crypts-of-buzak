﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface ISkillRepository : IEntityRepository<App.DAL.DTO.Skill, int>
{
    
}