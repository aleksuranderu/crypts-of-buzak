﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IMonsterRepository : IEntityRepository<App.DAL.DTO.Monster, int>
{
    
}