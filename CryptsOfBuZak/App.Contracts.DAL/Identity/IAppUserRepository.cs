﻿using App.DAL.DTO.Identity;
using Base.Contracts.DAL;

namespace App.Contracts.DAL.Identity;

public interface IAppUserRepository : IEntityRepository<App.DAL.DTO.Identity.AppUser, Guid>
{
}