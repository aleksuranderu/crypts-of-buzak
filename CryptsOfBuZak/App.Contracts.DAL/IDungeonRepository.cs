﻿using App.DAL.DTO;
using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IDungeonRepository : IEntityRepository<App.DAL.DTO.Dungeon, int>
{
    Task<Dungeon?> FirstOrDefaultByIdAsync(int dungeonId, bool noTracking = true);
    
    
}