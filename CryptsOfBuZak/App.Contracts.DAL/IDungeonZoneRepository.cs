﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IDungeonZoneRepository : IEntityRepository<App.DAL.DTO.DungeonZone, int>
{
    Task<IEnumerable<App.DAL.DTO.DungeonZone>> GetAllByDungeonIdAsync(int dungeonId, bool noTracking = true);
}