﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface ISkillBookRepository : IEntityRepository<App.DAL.DTO.SkillBook, int>
{
    Task<IEnumerable<App.DAL.DTO.SkillBook>> GetAllByCharacterIdAsync(Guid characterId, bool noTracking = true);

    bool Exists(Guid characterId, int skillId);

    Task<App.DAL.DTO.SkillBook?> FirstOrDefaultAsync(Guid characterId, int skillId, bool noTracking = true);
}