﻿using App.Contracts.DAL.Identity;
using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IAppUnitOfWork : IUnitOfWork
{
    IAppUserRepository AppUsers { get; }
    IDungeonRepository Dungeons { get; }
    IEffectRepository Effects { get; }
    IItemRepository Items { get; }
    ICharacterRepository Characters { get; }
    ISkillRepository Skills { get; }
    IMonsterRepository Monsters { get; }
    IMonsterListRepository MonsterLists { get; }
    IInventoryItemRepository InventoryItems { get; }
    IInRaidItemRepository InRaidItems { get; }
    IInRaidSkillRepository InRaidSkills { get; }
    IInRaidEffectRepository InRaidEffects { get; }
    IGroupStatusRepository GroupStatuses { get; }
    IFriendRepository Friends { get; }
    IDungeonZoneRepository DungeonZones { get; }
    IDropTableItemRepository DropTableItems { get; }
    IBattleCharacterRepository BattleCharacters { get; }
    ISkillBookRepository SkillBooks { get; }
}