﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IDropTableItemRepository : IEntityRepository<App.DAL.DTO.DropTableItem, Guid>
{
    
}