﻿using App.DAL.DTO.GroupStatus;
using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IGroupStatusRepository : IEntityRepository<App.DAL.DTO.GroupStatus.GroupStatus, Guid>
{
    Task<GroupStatus?> GetByLeaderIdAsync(Guid userid, bool noTracking = true);
    Task<GroupStatus?> GetByLeaderIdSensitive(Guid userid, bool noTracking = true);
    Task<GroupStatus?> GetByLeaderIdFightPrepared(Guid userid, bool noTracking = true);
}