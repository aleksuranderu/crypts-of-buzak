﻿using Base.Contracts.DAL;

namespace App.Contracts.DAL;

public interface IFriendRepository : IEntityRepository<App.DAL.DTO.Friend, int>
{
    bool Exists(Guid characterId, Guid friendId);
    
    Task<IEnumerable<App.DAL.DTO.Friend>> GetAllByIdAsync(Guid characterId, bool noTracking = true);
    
    Task<App.DAL.DTO.Friend?> FirstOrDefaultAsync(Guid characterId, Guid friendId, bool noTracking = true);
}