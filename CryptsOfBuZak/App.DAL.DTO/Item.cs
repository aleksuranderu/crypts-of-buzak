﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Base.Domain;
using Domain;

namespace App.DAL.DTO;

public class Item : DomainEntityId<int>
{
    [Column(TypeName = "jsonb")]
    public LangStr Name { get; set; } = new();
    [Column(TypeName = "jsonb")]
    public LangStr Description { get; set; } = new();
    public EItemCategory Category { get; set; }
    public int? SkillId { get; set; }
    public int? EffectId { get; set; }
    public Effect? Effect { get; set; }
    public Skill? Skill { get; set; }
    public int Price { get; set; }
    public int? MinLevelToBuy { get; set; }
}