﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Base.Domain;

namespace App.DAL.DTO;

public class Skill : DomainEntityId<int>
{
    [Display(ResourceType = typeof(App.Resources.App.Domain.Effect), Name = nameof(Name))]
    [Column(TypeName = "jsonb")]

    public LangStr Name { get; set; } = new LangStr();
    [Display(ResourceType = typeof(App.Resources.App.Domain.Effect), Name = nameof(Description))]
    [Column(TypeName = "jsonb")]
    public LangStr Description { get; set; } = new LangStr();
    public int EffectId { get; set; }
    public int Cooldown { get; set; }
    public int ManaCost { get; set; }
    [Display(ResourceType = typeof(App.Resources.App.Domain.Item), Name = nameof(Effect))]
    public Effect? Effect { get; set; }   
}