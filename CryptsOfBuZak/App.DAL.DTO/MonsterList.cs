﻿using Base.Domain;

namespace App.DAL.DTO;

public class MonsterList : DomainEntityId<int>
{
    public int MonsterAmount { get; set; }
    public int MonsterId { get; set; }
    public int DungeonZoneId { get; set; }
    
    public Monster? Monster { get; set; }
    public DungeonZone? DungeonZone { get; set; }
}