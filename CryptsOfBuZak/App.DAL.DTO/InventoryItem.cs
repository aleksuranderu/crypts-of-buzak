﻿namespace App.DAL.DTO;

public class InventoryItem: AbstractItem
{
    public Guid CharacterId { get; set; }
    
    public bool Used { get; set; }
}