﻿using Base.Domain;

namespace App.DAL.DTO;

public class DungeonZone : DomainEntityId<int>
{
    public int ZoneNumber { get; set; }
    public ICollection<MonsterList>? MonsterLists { get; set; }
    
    public int DungeonId { get; set; }
    public Dungeon? Dungeon { get; set; }
}