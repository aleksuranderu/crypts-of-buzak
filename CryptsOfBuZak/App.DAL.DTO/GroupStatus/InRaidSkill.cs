﻿using Base.Domain;

namespace App.DAL.DTO.GroupStatus;

public class InRaidSkill: DomainEntityId<Guid>
{
    public Guid GroupStatusId { get; set; }
    public int SkillId { get; set; }  // Use Id instead of object so changes may be done during playtime.
    public Skill? Skill { get; set; }
    public int CurrentCooldown { get; set; }  // Turns left to cast again. Ready to cast if equals 0
}