﻿using App.Domain.AbstractThings;
using Base.Domain;

namespace App.DAL.DTO.GroupStatus;

public class InRaidItem: DomainEntityId<Guid>
{
    public int ItemId { get; set; } // Use Id instead of object so changes may be done during playtime.
    public Item? Item { get; set; }
    public int Amount { get; set; }
    public Guid GroupStatusId { get; set; }
}