﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.DAL.DTO.Identity;
using Base.Domain;

namespace App.DAL.DTO;

public class Character : DomainEntityId<Guid>
{
    [Display(ResourceType = typeof(App.Resources.App.Domain.Character), Name = nameof(Name))]
    [MaxLength(64)] public string Name { get; set; } = default!;

    [ForeignKey("AppUser")]
    public Guid AppUserId { get; set; }
    [Display(ResourceType = typeof(App.Resources.App.Domain.Character), Name = nameof(AppUser))]
    public AppUser? AppUser { get; set; }
    
    public virtual GroupStatus.GroupStatus? LeaderGroup { get; set; }

    public int Level { get; set; }
    public int ExperiencePoints { get; set; }
    public int HealthPoints { get; set; }
    public int ManaPoints { get; set; }
    public int AttackStat { get; set; }
    public int DefenceStat { get; set; }
    public int Money { get; set; }
    
    public ICollection<InventoryItem>? InventoryItems { get; set; }
    public ICollection<Friend>? FriendList { get; set; }
    public ICollection<SkillBook>? SkillBook { get; set; }
}