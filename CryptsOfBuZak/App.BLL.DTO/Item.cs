﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Base.Domain;
using Domain;

namespace App.BLL.DTO;

public class Item : DomainEntityId<int>
{
    [Display(ResourceType = typeof(App.Resources.App.Domain.Item), Name = nameof(Name))]
    [Column(TypeName = "jsonb")]
    public LangStr Name { get; set; } = new();
    [Display(ResourceType = typeof(App.Resources.App.Domain.Item), Name = nameof(Description))]
    [Column(TypeName = "jsonb")]
    public LangStr Description { get; set; } = new();
    [Display(ResourceType = typeof(App.Resources.App.Domain.Item), Name = nameof(Category))]
    public EItemCategory Category { get; set; }
    public int? SkillId { get; set; }
    public int? EffectId { get; set; }
    [Display(ResourceType = typeof(App.Resources.App.Domain.Item), Name = nameof(Effect))]
    public Effect? Effect { get; set; }
    [Display(ResourceType = typeof(App.Resources.App.Domain.Item), Name = nameof(Skill))]
    public Skill? Skill { get; set; }
    public int Price { get; set; }
    public int? MinLevelToBuy { get; set; }
}