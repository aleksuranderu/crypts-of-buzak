﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Base.Domain;

namespace App.BLL.DTO;

public class Dungeon : DomainEntityId<int>
{
    [Column(TypeName = "jsonb"),
     Display(ResourceType = typeof(App.Resources.App.Domain.Dungeon), Name = nameof(Name))]
    public LangStr Name { get; set; } = new();
    [Column(TypeName = "jsonb"),
     Display(ResourceType = typeof(App.Resources.App.Domain.Dungeon), Name = nameof(Description))]
    public LangStr Description { get; set; } = new();
    [Display(ResourceType = typeof(App.Resources.App.Domain.Dungeon), Name = nameof(AverageLevel))]
    public int AverageLevel { get; set; }
    public ICollection<DungeonZone>? DungeonZones { get; set; }
}