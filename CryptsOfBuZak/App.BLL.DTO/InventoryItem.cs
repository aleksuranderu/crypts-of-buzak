﻿using System;
using Base.Domain;

namespace App.BLL.DTO;

public class InventoryItem: AbstractItem
{
    public Guid CharacterId { get; set; }
    
    public bool Used { get; set; }
}