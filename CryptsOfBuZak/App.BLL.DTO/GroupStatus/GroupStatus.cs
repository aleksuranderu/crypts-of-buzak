﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Base.Domain;

namespace App.BLL.DTO.GroupStatus;

public class GroupStatus: DomainEntityId<Guid>
{
    public Guid GroupLeaderId { get; set; }
    public Character? GroupLeader { get; set; }

    public Guid BattleCharacterLeaderId { get; set; }
    
    [MaxLength(4)]
    public ICollection<BattleCharacter>? BattleCharacters { get; set; }
    public int? DungeonZoneNumber { get; set; }
    public List<InRaidSkill>? InRaidSkills { get; set; }
    public ICollection<InRaidItem>? InRaidItems { get; set; }
    public ICollection<InRaidEffect>? InRaidEffects { get; set; }
    public int? DungeonId { get; set; }
    public Dungeon? Dungeon { get; set; }
    public string GameHistory { get; set; } = "";
}