﻿using System;
using Base.Domain;

namespace App.BLL.DTO.GroupStatus;

public class InRaidEffect : DomainEntityId<Guid>
{
    public Guid GroupStatusId { get; set; }
    public int EffectId { get; set; }  // Use Id instead of object so changes may be done during playtime.
    public Effect? Effect { get; set; }
    public int Duration { get; set; }  // remove effect if 0
}