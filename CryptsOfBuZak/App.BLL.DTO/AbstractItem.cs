﻿using System;
using Base.Domain;

namespace App.BLL.DTO;

public class AbstractItem : DomainEntityId<int>
{
        public int ItemId { get; set; } // Use Id instead of object so changes may be done during playtime.
        public Item? Item { get; set; }
        public int Amount { get; set; }
}