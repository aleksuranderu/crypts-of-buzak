﻿
using Base.Domain;

namespace App.BLL.DTO;

public class SkillBook : DomainEntityId<int>
{
    public Guid CharacterId { get; set; }
    public Character? Character { get; set; }

    public int SkillId { get; set; }
    public Skill? Skill { get; set; }
}