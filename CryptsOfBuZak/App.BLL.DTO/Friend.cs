﻿using System;
using Base.Domain;

namespace App.BLL.DTO;

public class Friend: DomainEntityId<int>
{
    public Guid? CharacterId { get; set; }
    public Character? Character { get; set; }
    
    public Guid? FriendId { get; set; }
    public Character? FriendCharacter { get; set; }
    
    public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
}