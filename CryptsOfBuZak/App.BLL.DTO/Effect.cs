﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Base.Domain;

namespace App.BLL.DTO;

public class Effect : DomainEntityId<int>
{
    [Display(ResourceType = typeof(App.Resources.App.Domain.Effect), Name = nameof(Name))]
    [Column(TypeName = "jsonb")]
    public LangStr Name { get; set; } = new();

    [Display(ResourceType = typeof(App.Resources.App.Domain.Effect), Name = nameof(Description))]
    [Column(TypeName = "jsonb")]
    public LangStr Description { get; set; } = new();
    public int Duration { get; set; }
    public int HpModifier { get; set; }
    public int MpModifier { get; set; }
    public int AtkModifier { get; set; }
    public int DefModifier { get; set; }
}