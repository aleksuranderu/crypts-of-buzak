﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.BLL.DTO.Identity;
using App.Domain.GroupStatus;
using Base.Domain;

namespace App.BLL.DTO;

public class Character : DomainEntityId<Guid>
{
    [Display(ResourceType = typeof(App.Resources.App.Domain.Character), Name = nameof(Name))]
    [MaxLength(64)] public string Name { get; set; } = default!;

    [ForeignKey("AppUser")]
    public Guid AppUserId { get; set; }
    [Display(ResourceType = typeof(App.Resources.App.Domain.Character), Name = nameof(AppUser))]
    public AppUser? AppUser { get; set; }
    
    public GroupStatus.GroupStatus? LeaderGroup { get; set; }

    public int Level { get; set; } = 1;
    public int ExperiencePoints { get; set; } = 0;
    public int HealthPoints { get; set; } = 10;
    public int ManaPoints { get; set; } = 1;
    public int AttackStat { get; set; } = 4;
    public int DefenceStat { get; set; } = 1;
    public int Money { get; set; } = 50;
    
    public ICollection<InventoryItem>? InventoryItems { get; set; }
    public ICollection<Friend>? FriendList { get; set; }
    public ICollection<SkillBook>? SkillBook { get; set; }
}