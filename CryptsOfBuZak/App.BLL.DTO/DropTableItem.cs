﻿using System;
using Base.Domain;

namespace App.BLL.DTO;

public class DropTableItem : DomainEntityId<Guid>
{
    public int ItemId { get; set; }
    public Item? Item { get; set; }

    public int MonsterId { get; set; }
    public Monster? Monster { get; set; }
    
    public int DropMinimumAmount { get; set; }
    public int DropMaximumAmount { get; set; }
    public float DropChance { get; set; }  // 0 is 0% 1 is 100%.
}