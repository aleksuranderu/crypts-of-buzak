﻿namespace App.Public.DTO.v1;

public class InventoryItem
{
    public int ItemId { get; set; }
    public int Amount { get; set; }
    
    public Guid CharacterId { get; set; }

    public bool Used { get; set; }
}