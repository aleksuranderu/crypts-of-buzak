﻿namespace App.Public.DTO.v1;

public class DungeonZone
{
    public int Id { get; set; }
    public int ZoneNumber { get; set; }
    public int DungeonId { get; set; }
}