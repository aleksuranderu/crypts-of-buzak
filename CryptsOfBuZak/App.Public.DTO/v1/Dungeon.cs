﻿namespace App.Public.DTO.v1;

public class Dungeon
{
    public int Id { get; set; }
    public string Name { get; set; } = default!;
    public string Description { get; set; } = default!;
    public int AverageLevel { get; set; } = default!;
}