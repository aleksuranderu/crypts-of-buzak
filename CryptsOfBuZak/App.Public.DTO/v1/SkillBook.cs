﻿namespace App.Public.DTO.v1;

public class SkillBook
{
    public Guid CharacterId { get; set; }
    public int SkillId { get; set; }
}