﻿namespace App.Public.DTO.v1;

public class Friend
{
    public Guid CharacterId { get; set; }

    public Guid FriendId { get; set; }
}