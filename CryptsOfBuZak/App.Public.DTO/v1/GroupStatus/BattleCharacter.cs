﻿namespace App.Public.DTO.v1.GroupStatus;

public class BattleCharacter
{
    public Guid Id { get; set; }
    
    public Guid GroupStatusId { get; set; }

    public string? Name { get; set; }
    public int Level { get; set; }
    public long Experience { get; set; }
    public int MaxHp { get; set; }
    public int CurrentHp { get; set; }
    public int MaxMp { get; set; }
    public int CurrentMp { get; set; }
    public int AtkStat { get; set; }
    public int DefStat { get; set; }
}