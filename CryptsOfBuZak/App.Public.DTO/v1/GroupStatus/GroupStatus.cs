﻿namespace App.Public.DTO.v1.GroupStatus;

public class GroupStatus
{
    public Guid Id { get; set; }
    
    public Guid GroupLeaderId { get; set; }
    public Character? GroupLeader { get; set; }
    
    public Guid BattleCharacterLeaderid { get; set; }
    
    public ICollection<BattleCharacter>? BattleCharacters { get; set; }
    public int? DungeonZoneNumber { get; set; }
    public ICollection<InRaidObjects.InRaidSkill>? InRaidSkills { get; set; }
    public ICollection<InRaidObjects.InRaidItem>? InRaidItems { get; set; }
    public ICollection<InRaidObjects.InRaidEffect>? InRaidEffects { get; set; }
    public int DungeonId { get; set; }
    public string? GameHistory { get; set; }
}