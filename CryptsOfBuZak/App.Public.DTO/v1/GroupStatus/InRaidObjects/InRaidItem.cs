﻿
namespace App.Public.DTO.v1.GroupStatus.InRaidObjects;

public class InRaidItem
{
    public Guid Id { get; set; }
    
    public Guid GroupStatusId { get; set; }

    public int ItemId { get; set; }
    public int Amount { get; set; }
}