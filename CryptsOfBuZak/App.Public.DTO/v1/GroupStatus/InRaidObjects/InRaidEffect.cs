﻿namespace App.Public.DTO.v1.GroupStatus.InRaidObjects;

public class InRaidEffect
{
    public Guid Id { get; set; }
    
    public Guid GroupStatusId { get; set; }
    public int EffectId { get; set; }  // Use Id instead of object so changes may be done during playtime.
    public int Duration { get; set; }  // remove effect if 0
}