﻿using Base.Domain;

namespace App.Public.DTO.v1.GroupStatus.InRaidObjects;

public class InRaidSkill
{
    public Guid Id { get; set; }
    
    public Guid GroupStatusId { get; set; }
    public int SkillId { get; set; }  // Use Id instead of object so changes may be done during playtime.
    public int CurrentCooldown { get; set; }  // Turns left to cast again. Ready to cast if equals 0
}