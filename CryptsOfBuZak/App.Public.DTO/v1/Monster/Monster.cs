﻿using Base.Domain;

namespace App.Public.DTO.v1.Monster;

public class Monster
{
    public int Id { get; set; }

    public string Name { get; set; } = default!;

    public int HealthPoints { get; set; }
    public int AttackStat { get; set; }
    public int DefenceStat { get; set; }
}