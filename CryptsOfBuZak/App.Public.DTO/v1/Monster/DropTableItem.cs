﻿namespace App.Public.DTO.v1.Monster;

public class DropTableItem
{
    public Guid? Id { get; set; }
    
    public int ItemId { get; set; }
    public int MonsterId { get; set; }

    public int DropMinimumAmount { get; set; }
    public int DropMaximumAmount { get; set; }
    public float DropChance { get; set; }  // 0 is 0% 1 is 100%.
}