﻿using Base.Domain;

namespace App.Public.DTO.v1.Monster;

public class MonsterList
{
    public int Id { get; set; }
    
    public int MonsterAmount { get; set; }
    public int MonsterId { get; set; }
    public int DungeonZoneId { get; set; }
}