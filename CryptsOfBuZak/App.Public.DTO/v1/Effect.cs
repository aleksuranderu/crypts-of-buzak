﻿using Base.Domain;

namespace App.Public.DTO.v1;

public class Effect
{
    public int Id { get; set; }
    
    public string Name { get; set; } = default!;

    public string Description { get; set; } = default!;
    public int Duration { get; set; }
    public int HpModifier { get; set; }
    public int MpModifier { get; set; }
    public int AtkModifier { get; set; }
    public int DefModifier { get; set; }
}