﻿namespace App.Public.DTO.v1;

public class Skill
{
    public int Id { get; set; }
    
    public string Name { get; set; } = default!;
    public string Description { get; set; } = default!;
    
    public int EffectId { get; set; }
    public int Cooldown { get; set; }
    public int ManaCost { get; set; }
}