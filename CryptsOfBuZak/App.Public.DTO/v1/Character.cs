﻿namespace App.Public.DTO.v1;

public class Character
{
    public Guid Id { get; set; }
    
    public string Name { get; set; } = default!;
    
    public int Level { get; set; } = 1;
    public int ExperiencePoints { get; set; }
    public int HealthPoints { get; set; }
    public int ManaPoints { get; set; }
    public int AttackStat { get; set; }
    public int DefenceStat { get; set; }
    public int Money { get; set; }
}