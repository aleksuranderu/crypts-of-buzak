﻿using AutoMapper;

namespace App.Public.DTO.v1;

public class AutomapperConfig : Profile
{
    public AutomapperConfig()
    {
        CreateMap<App.BLL.DTO.Character, App.Public.DTO.v1.Character>().ReverseMap();
        CreateMap<App.BLL.DTO.Dungeon, App.Public.DTO.v1.Dungeon>().ReverseMap();
        CreateMap<App.BLL.DTO.Skill, App.Public.DTO.v1.Skill>().ReverseMap();
        CreateMap<App.BLL.DTO.Effect, App.Public.DTO.v1.Effect>().ReverseMap();
        CreateMap<App.BLL.DTO.Item, App.Public.DTO.v1.Item>().ReverseMap();
        CreateMap<App.BLL.DTO.DungeonZone, App.Public.DTO.v1.DungeonZone>().ReverseMap();
        CreateMap<App.BLL.DTO.SkillBook, App.Public.DTO.v1.SkillBook>().ReverseMap();
        CreateMap<App.BLL.DTO.Friend, App.Public.DTO.v1.Friend>().ReverseMap();
        CreateMap<App.BLL.DTO.InventoryItem, App.Public.DTO.v1.InventoryItem>().ReverseMap();
        CreateMap<App.BLL.DTO.DropTableItem, App.Public.DTO.v1.Monster.DropTableItem>().ReverseMap();
        CreateMap<App.BLL.DTO.Monster, App.Public.DTO.v1.Monster.Monster>().ReverseMap();
        CreateMap<App.BLL.DTO.MonsterList, App.Public.DTO.v1.Monster.MonsterList>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.GroupStatus, App.Public.DTO.v1.GroupStatus.GroupStatus>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.BattleCharacter, App.Public.DTO.v1.GroupStatus.BattleCharacter>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.InRaidEffect, App.Public.DTO.v1.GroupStatus.InRaidObjects.InRaidEffect>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.InRaidSkill, App.Public.DTO.v1.GroupStatus.InRaidObjects.InRaidSkill>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.InRaidItem, App.Public.DTO.v1.GroupStatus.InRaidObjects.InRaidItem>().ReverseMap();
    }
}