﻿using Base.Domain;
using Domain;

namespace App.Public.DTO.v1;

public class Item
{
    public int Id { get; set; }
    
    public string Name { get; set; } = default!;
    public string Description { get; set; } = default!;
    
    public int Category { get; set; }
    public int? SkillId { get; set; }
    public int? EffectId { get; set; }
    
    public int Price { get; set; }
    
    public int? MinLevelToBuy { get; set; }
}