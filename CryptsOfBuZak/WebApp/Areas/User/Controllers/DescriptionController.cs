﻿using App.BLL.DTO;
using App.Contracts.BLL;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.User.Controllers;

[Area("User")]
[Authorize(Roles = "user")]

public class DescriptionController : Controller
{
    private readonly IAppBLL _bll;
    
    public DescriptionController(IAppBLL bll)
    {
        _bll = bll;
    }

    public async Task<IActionResult> ItemDescription(int id)
    {
        Item? item = await _bll.Items.FirstOrDefaultAsync(id);
        if (item == null)
        {
            return NotFound();
        }
        
        if( (item.Category == EItemCategory.Artifact || item.Category == EItemCategory.Consumable) && item.EffectId != null )
        {
            Effect? effect = await _bll.Effects.FirstOrDefaultAsync(item.EffectId.Value);
            if (effect != null)
            {
                item.Effect = effect;
            }
            
        }
        else if (item.Category == EItemCategory.SkillBook && item.SkillId != null)
        {
            Skill? skill = await _bll.Skills.FirstOrDefaultAsync(item.SkillId.Value);
            if (skill != null)
            {
                item.Skill = skill;
            }
        }
        return View(item);
    }
    
    public async Task<IActionResult> EffectDescription(int id)
    {
        Effect? effect = await _bll.Effects.FirstOrDefaultAsync(id);
        if (effect == null)
        {
            return NotFound();
        }
        return View(effect);
    }
    
    public async Task<IActionResult> SkillDescription(int id)
    {
        Skill? skill = await _bll.Skills.FirstOrDefaultAsync(id);
        if (skill == null)
        {
            return NotFound();
        }

        Effect? effect = await _bll.Effects.FirstOrDefaultAsync(skill.EffectId);
        if (effect != null)
        {
            skill.Effect = effect;
        }
        else
        {
            return BadRequest();
        }
        return View(skill);
    }
}