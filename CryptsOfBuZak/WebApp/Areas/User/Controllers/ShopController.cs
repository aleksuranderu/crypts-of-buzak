﻿using App.BLL.CryptsOfBuzak;
using App.BLL.DTO;
using App.Contracts.BLL;
using Base.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.User.Controllers;

[Area("User")]
[Authorize(Roles = "user")]
public class ShopController : Controller
{
    
    private readonly IAppBLL _bll;

    public ShopController(IAppBLL bll)
    {
        _bll = bll;
    }
    
    //GET
    public async Task<IActionResult> Index()
    {
        Character? character = _bll.Characters.FirstOrDefault(User.GetUserId());
        
        if (character == null)
        {
            return NotFound();
        }
        
        character.InventoryItems = (await _bll.InventoryItems.GetAllByCharacterIdAsync(character.Id)).ToList();

        List<Item> itemList = (await _bll.Items.GetAllAvailableByLevel(character.Level)).ToList();

        ViewData["BuyList"] = itemList;
        
        return View(character);
    }
    
    [HttpPost]
    public async Task<IActionResult> Index(int itemId, int amount, bool sell)
    {
        // throw new NotImplementedException();


        Character? character = await _bll.Characters.FirstOrDefaultAsync(User.GetUserId());

        if (character == null)
        {
            return NotFound();
        }

        if (sell)
        {
            InventoryItem? item = await _bll.InventoryItems.FirstOrDefaultAsync(User.GetUserId(), itemId);
            
            if (item == null)
            {
                ViewData["ErrorMessage"] = "Item not found!";
            }
            else if (item.Item == null)
            {
                ViewData["ErrorMessage"] = "Item references null, not found!";
            }
            else if (item.CharacterId != User.GetUserId())
            {
                ViewData["ErrorMessage"] = "You don't own this item!";
            }
            else if (amount < 1)
            {
                ViewData["ErrorMessage"] = "You can't sell negative or zero items! Seriously, how you gonna do this?";
            }
            else if (item.Amount < amount)
            {
                ViewData["ErrorMessage"] = "You trying to sell more than you have!";
            }
            else
            {
                if (item.Amount - amount > 0)
                {
                    item.Amount -= amount;
                    character.Money += item.Item.Price * amount;
                
                    item.Item = null;
                    _bll.InventoryItems.Update(item);

                }
                else
                {
                    await _bll.InventoryItems.RemoveAsync(User.GetUserId(), itemId);
                    character.Money += item.Item.Price * amount;
                }

                _bll.Characters.Update(character);
                await _bll.SaveChangesAsync();
            }
        }
        else
        {
            Item? item = await _bll.Items.FirstOrDefaultAsync(itemId);

            if (item == null)
            {
                ViewData["ErrorMessage"] = "Item not found!";
            }
            else if (amount <= 0)
            {
                ViewData["ErrorMessage"] = "You can't buy negative or zero items! Seriously, how you gonna do this?";
            }
            else if (item.Price * amount > character.Money)
            {
                ViewData["ErrorMessage"] = "You can't afford this, you don't have enough money!";
            }
            else if (item.MinLevelToBuy == null || character.Level < item.MinLevelToBuy)
            {
                ViewData["ErrorMessage"] = "You can't buy this!";
            }
            else
            {
                ViewData["ErrorMessage"] = $"You successfully bought {amount} {item.Name}! Current Balance: {character.Money}denga";
                ViewData["Test"] = character;
            
                character.Money -= item.Price * amount;
                _bll.Characters.Update(character);
            
                InventoryItem? inventoryItem = 
                    await _bll.InventoryItems.FirstOrDefaultAsync(User.GetUserId(), itemId);

                if (inventoryItem == null)
                {
                    inventoryItem = Core.CreateInventoryItem(User.GetUserId(), itemId, amount);
                }
                else
                {
                    inventoryItem.Amount += amount;
                    inventoryItem.Item = null;
                }
            
                _bll.InventoryItems.Update(inventoryItem);
                await _bll.SaveChangesAsync();
            }
        }
        
        List<Item> itemList = (await _bll.Items.GetAllAvailableByLevel(character.Level)).ToList();

        ViewData["BuyList"] = itemList;
        
        character.InventoryItems = (await _bll.InventoryItems.GetAllByCharacterIdAsync(character.Id)).ToList();

        return View(character);
    }
}