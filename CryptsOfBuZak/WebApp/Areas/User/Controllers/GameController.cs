﻿using System.Configuration;
using App.BLL.CryptsOfBuzak;
using App.BLL.DTO;
using App.Contracts.BLL;
using App.BLL.DTO.GroupStatus;
using Base.Extensions;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using BattleCharacter = App.BLL.DTO.GroupStatus.BattleCharacter;
using GroupStatus = App.BLL.DTO.GroupStatus.GroupStatus;
using InRaidEffect = App.BLL.DTO.GroupStatus.InRaidEffect;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace WebApp.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "user")]
    public class GameController : Controller
    {
        private readonly IAppBLL _bll;

        public GameController(IAppBLL bll)
        {
            _bll = bll;
        }
        
        // GET
        public async Task<IActionResult> Index()
        {

            // Guid name = _bll.AppUsers.FirstOrDefaultAsync(User.GetUserId()).Result?.CharacterId ?? default;
            bool check = await _bll.Characters.FirstOrDefaultAsync(User.GetUserId()) == null;
            
            if (check)
            {
                return RedirectToAction(nameof(CreateCharacter));
            }

            GroupStatus? gs = await _bll.GroupStatuses.GetByLeaderIdAsync(User.GetUserId());
            if (gs != null)
            {
                if (gs.DungeonZoneNumber != null)
                {
                    return RedirectToAction(nameof(Camp));
                }
            }

            var res = (await _bll.Dungeons.GetAllAsync()).ToList();
            foreach (var dungeon in res)
            {
                var dungeonZones = (await _bll.DungeonZones.GetAllByDungeonIdAsync(dungeon.Id)).ToList();
                dungeon.DungeonZones = dungeonZones;
            }

            return View(res);
        }
        
        // GET: Game/CreateCharacter
        public IActionResult CreateCharacter()
        {
            // bool check = _bll.AppUsers.FirstOrDefault(User.GetUserId())!.CharacterId != default;
            
            bool check = _bll.Characters.FirstOrDefault(User.GetUserId()) != null;
            if (check)
            {
                return RedirectToAction(nameof(Index), new {var = check});
            }
            return View();
        }

        // GET: Game/FormParty
        public async Task<IActionResult> FormParty(int? id)
        {
            Character? character = _bll.Characters.FirstOrDefault(User.GetUserId());
            
            if (character == null)
            {
                return RedirectToAction(nameof(CreateCharacter));
            }
            
            GroupStatus? res = await _bll.GroupStatuses.FirstOrDefaultAsync(User.GetUserId());
            bool createNew = false;
            
            if(res == null)
            {
                // Group Doesn't exist yet, create one.
                res = Core.CreateGroupStatus(character);
                createNew = true;
            }

            if (res.DungeonId == null && id == null)
            {
                // Can't find any dungeon.
                return NotFound();
            }

            // If id is null - get dungeon id from groupStatus
            if(id == null && res.DungeonId != null)
            {
                id = res.DungeonId;
            }
            
            // Check if Dungeon actually exists and if group already exists
            if (await _bll.Dungeons.FirstOrDefaultAsync(id!.Value) == null || res.DungeonZoneNumber != null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            res.DungeonId = id;

            if (createNew)
            {
                _bll.GroupStatuses.Add(res);
            }
            else
            {
                _bll.GroupStatuses.Update(res);
            }
            
            await _bll.SaveChangesAsync();
            
            character.FriendList = (await _bll.Friends.GetAllByIdAsync(character.Id)).ToList();
            res.Dungeon = await _bll.Dungeons.FirstOrDefaultAsync(id.Value);
            res.Dungeon!.DungeonZones = (await _bll.DungeonZones.GetAllByDungeonIdAsync(id.Value)).ToList();
            res.GroupLeader = character;

            return View(res);
        }

        [HttpPost, ActionName("InviteToParty")]
        public async Task<IActionResult> PartyInvite(Guid id)
        {
            GroupStatus? gs = _bll.GroupStatuses.FirstOrDefault(User.GetUserId());
            // if BattleCharacters aren't null - adventure already in progress.
            //TODO! Check DungeonZoneNumber instead of BattleCharacters.
            if (gs?.BattleCharacters == null)
            {
                return RedirectToAction(nameof(Index));
            }

            if (gs.BattleCharacters!.Count > 0)
            {
                return RedirectToAction(nameof(Index));
            }

            List<Guid> guidList = new List<Guid>();
            
            if (gs.GameHistory != "")
            {
                guidList = JsonSerializer.Deserialize<List<Guid>>(gs.GameHistory)!;
            }
            if (guidList.Contains(id) || guidList.Count > 3)
            {
                //Such friend already exists in party! || Party is already full, maximum 3 friends!
                return RedirectToAction(nameof(FormParty));
            }

            // Checks whether a character with such guid in your friendList.
            Character? friendCharacter = (await _bll.Friends.GetAllByIdAsync(User.GetUserId())).ToList()
                .FirstOrDefault(x => x.FriendId == id)?.FriendCharacter;

            if (friendCharacter == null)
            {
                //Friend with such Id is not your friend or doesn't exist!
                return RedirectToAction(nameof(FormParty));
            }
            
            // Good ending.
            guidList.Add(friendCharacter.Id);
            gs.GameHistory = JsonSerializer.Serialize(guidList);
            _bll.GroupStatuses.Update(gs);
            await _bll.SaveChangesAsync();
            
            return RedirectToAction(nameof(FormParty));
        }
        
        [HttpPost, ActionName("RemoveFromParty")]
        public async Task<IActionResult> PartyRemove(Guid id)
        {
            var groupStatus = await _bll.GroupStatuses.FirstOrDefaultAsync(User.GetUserId());
            if (groupStatus == null)
            {
                return RedirectToAction(nameof(Index));
            }
            //Checks that groupstatus have BattleCharacters and GameHistory
            if (groupStatus.BattleCharacters == null || string.IsNullOrEmpty(groupStatus.GameHistory))
            {
                return RedirectToAction(nameof(FormParty));
            }

            List<Guid> guidList = JsonSerializer.Deserialize<List<Guid>>(groupStatus.GameHistory)!;
            
            if (guidList.All(x => x != id))
            {
                //Character with such id doesn't exist in your party!
                return RedirectToAction(nameof(FormParty));
            }

            guidList.Remove(id);
            groupStatus.GameHistory = JsonSerializer.Serialize(guidList);
            _bll.GroupStatuses.Update(groupStatus);
            await _bll.SaveChangesAsync();

            return RedirectToAction(nameof(FormParty));
        }

        // Properly Assign dungeon to GroupStatus. Confirms that Adventure has started.
        [HttpPost, ActionName("StartAdventure")]
        public async Task<IActionResult> StartParty(int? id)
        {
            //TODO!!! Check Dungeon and DungeonZone from Groupstatus. If not null - redirect to Camp or Index?
            GroupStatus? groupstatus = await _bll.GroupStatuses.FirstOrDefaultAsync(User.GetUserId());
            
            if (groupstatus == null)
            {
                //Group doesn't exist!
                return RedirectToAction(nameof(Index));
            }
            
            if (id == null && groupstatus.DungeonId != null)
            {
                id = groupstatus.DungeonId.Value;
            }
            else if (id == null)
            {
                return RedirectToAction(nameof(Index));
            }

            Dungeon? dungeon = await _bll.Dungeons.FirstOrDefaultByIdAsync(id.Value);

            if (dungeon?.DungeonZones == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            List<Guid> guidList = new List<Guid>();
            
            if (!groupstatus.GameHistory.IsNullOrEmpty())
            {
                guidList = JsonSerializer.Deserialize<List<Guid>>(groupstatus.GameHistory) ?? new List<Guid>();
            }
            
            groupstatus.BattleCharacters = new List<BattleCharacter>();
            
            // Create leader's battleCharacter.
            Character character = (await _bll.Characters.FirstOrDefaultAsync(User.GetUserId()))!;
            BattleCharacter leader =
                Core.CreateBattleCharacter(character);
            leader.GroupStatusId = groupstatus.Id;
            
            _bll.BattleCharacters.Add(leader);
            
            groupstatus.BattleCharacters.Add(leader);
            groupstatus.BattleCharacterLeaderId = leader.Id;
            groupstatus.DungeonId = id;
            
            // Create leader's teammates battleCharacters .
            foreach (var friend in (await _bll.Friends.GetAllByIdAsync(User.GetUserId())).ToList())
            {
                Character? friendCharacter = friend.FriendCharacter;
                if (friendCharacter != null && guidList.Exists(x => x == friend.FriendId))
                {
                    // pay friends for hiring. tax is 50%.
                    friendCharacter.Money += (friendCharacter.Level * 5);
                    character.Money -= (friendCharacter.Level * 10);
                    
                    BattleCharacter bc = Core.CreateBattleCharacter(friendCharacter);
                    bc.GroupStatusId = groupstatus.Id;
                    _bll.BattleCharacters.Add(bc);
                    _bll.Characters.Update(friendCharacter);
                    groupstatus.BattleCharacters!.Add(bc);
                }
            }

            _bll.Characters.Update(character);

            // Assign start Dungeon level. Picks lowest zoneNumber as first
            if (dungeon.DungeonZones.Count > 0)
            {
                int startLevel = dungeon.DungeonZones.Min(x => x.ZoneNumber);
                groupstatus.DungeonZoneNumber = startLevel;
            }
            else
            {
                //Can't Start an Empty Dungeon!
                return RedirectToAction(nameof(Index));
            }

            //Make all items unused
            List<InventoryItem> inventoryItems = 
                (await _bll.InventoryItems.GetAllByCharacterIdAsync(User.GetUserId())).ToList();

            foreach (var inventoryItem in inventoryItems)
            {
                if (inventoryItem.Used)
                {
                    inventoryItem.Used = false;
                    inventoryItem.Item = null;
                    _bll.InventoryItems.Update(inventoryItem);
                }
            }
            
            groupstatus.GameHistory = "";
            groupstatus.BattleCharacters = null;
            _bll.GroupStatuses.Update(groupstatus);
            await _bll.SaveChangesAsync();
            
            // Adventure has started! Good luck!
            return RedirectToAction(nameof(Camp));
        }

        public async Task<IActionResult> Camp()
        {
            GroupStatus? groupStatus = await _bll.GroupStatuses.GetByLeaderIdSensitive(User.GetUserId());

            if (groupStatus == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            // Auto-use artifacts. If effect from artifact already exists or artifact is used - do nothing.
            List<InventoryItem> artifacts =
                groupStatus.GroupLeader!.InventoryItems!
                    .Where(x => x.Item is {Category: EItemCategory.Artifact}).ToList();

            foreach (var artifact in artifacts)
            {
                if (artifact.Item != null && artifact.Used == false)
                {
                    InRaidEffect? inRaidEffect =
                        groupStatus.InRaidEffects!
                            .FirstOrDefault(x => x.EffectId == artifact.Item.EffectId);

                    Effect? effect = await _bll.Effects.FirstOrDefaultAsync(artifact.Item.EffectId!.Value);

                    if (effect != null)
                    {
                        if (inRaidEffect == null)
                        {
                            inRaidEffect = Core.CreateInRaidEffect(groupStatus.Id, effect);
                            _bll.InRaidEffects.Add(inRaidEffect);
                            inRaidEffect.Effect = effect;
                            groupStatus.InRaidEffects?.Add(inRaidEffect);
                            artifact.Used = true;
                        }
                    }
                }
            }

            foreach (var effect in groupStatus.InRaidEffects!)
            {
                foreach (var battleChar in groupStatus.BattleCharacters!)
                {
                    battleChar.AtkStat += effect.Effect!.AtkModifier;
                    battleChar.DefStat += effect.Effect!.DefModifier;
                    battleChar.CurrentHp += effect.Effect!.HpModifier;
                    battleChar.CurrentMp += effect.Effect!.MpModifier;
                }
            }
            await _bll.SaveChangesAsync();
            return View(groupStatus);
        }

        [HttpPost, ActionName("UseItemInCamp")]
        public async Task<IActionResult> UseItem(int itemId)
        {
            GroupStatus groupStatus = (await _bll.GroupStatuses.GetByLeaderIdSensitive(User.GetUserId()))!;

            if (groupStatus.GroupLeader?.InventoryItems == null || groupStatus.InRaidEffects == null)
            {
                return RedirectToAction(nameof(Index));
            }

            InventoryItem? inventoryItem = (await _bll.InventoryItems
                .GetAllByCharacterIdAsync(User.GetUserId()))
                .FirstOrDefault(x => x.ItemId == itemId) ;

            if (inventoryItem?.Item == null)
            {
                return RedirectToAction(nameof(Index));
            }

            if (inventoryItem.Used)
            {
                // item is already used!
                return RedirectToAction(nameof(Camp));
            }

            if (inventoryItem.Item.Category == EItemCategory.SkillBook)
            {
                if (inventoryItem.Item.SkillId == null || groupStatus.GroupLeader.SkillBook == null)
                {
                    // Skill is null.
                    return RedirectToAction(nameof(Index));
                }
                
                if (groupStatus.GroupLeader.SkillBook.Any(x => x.SkillId ==  inventoryItem.Item.SkillId))
                {
                    // Skill is already learned!
                    return RedirectToAction(nameof(Camp));
                }
                
                // Skill is not learned yet, add to skillbook.
                SkillBook skillBook = Core.CreateSkillBook(groupStatus.GroupLeaderId, inventoryItem.Item.SkillId.Value);
                _bll.SkillBooks.Add(skillBook);

            } else if (inventoryItem.Item.Category == EItemCategory.Consumable)
            {
                if (inventoryItem.Item.EffectId != null)
                {
                    var inRaidEffect =
                        groupStatus.InRaidEffects.FirstOrDefault(x => x.EffectId == inventoryItem.Item.EffectId);

                    if (inRaidEffect != null)
                    {
                        // Effect already exist. prolong.
                        inRaidEffect.Duration += inRaidEffect.Effect!.Duration;
                        inRaidEffect.Effect = null;
                        _bll.InRaidEffects.Update(inRaidEffect);
                    }
                    else
                    {
                        // Effect doesn't exist yet. Create
                        if (inventoryItem.Item.EffectId != null)
                        {
                            Effect? effect = (await _bll.Effects.FirstOrDefaultAsync(inventoryItem.Item.EffectId.Value));
                            if (effect != null)
                            {
                                inRaidEffect = Core.CreateInRaidEffect(groupStatus.Id, effect);
                                _bll.InRaidEffects.Add(inRaidEffect);
                            }
                        }
                    }
                }
                else
                {
                    // Item is null
                    return RedirectToAction(nameof(Index));
                }
            }
            else
            {
                // You can't use anything except skillBooks and consumables Here!.
                return RedirectToAction(nameof(Index));
            }
            
            inventoryItem.Used = true;
            inventoryItem.Amount--;
            inventoryItem.Item = null;
            _bll.InventoryItems.Update(inventoryItem);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Camp));
        }
        
        [HttpPost, ActionName("UseSkillInCamp")]
        public async Task<IActionResult> UseSkill(int skillBookId)
        {
            GroupStatus groupStatus = (await _bll.GroupStatuses.GetByLeaderIdSensitive(User.GetUserId()))!;
            BattleCharacter? battleCharacter = (await _bll.BattleCharacters.FirstOrDefaultAsync(groupStatus.BattleCharacterLeaderId));
            
            if (groupStatus.GroupLeader == null || battleCharacter == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            if (groupStatus.InRaidSkills == null || groupStatus.GroupLeader!.SkillBook == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            List<InRaidSkill> inRaidSkills = groupStatus.InRaidSkills;
            SkillBook? skillBook = groupStatus.GroupLeader.SkillBook.FirstOrDefault(x => x.SkillId == skillBookId);

            if (skillBook?.Skill == null)
            {
                return RedirectToAction(nameof(Index));
            }

            if (inRaidSkills.Any(x => x.SkillId == skillBook.SkillId))
            {
                //Skill is on cooldown, don't cast
                return RedirectToAction(nameof(Camp));
            }
            else
            {
                //Skill is not on cooldown, cast!

                if (skillBook.Skill.EffectId == default || skillBook.Skill.ManaCost > battleCharacter.CurrentMp)
                {
                    return RedirectToAction(nameof(Camp));
                }

                Effect? effect = _bll.Effects.FirstOrDefault(skillBook.Skill.EffectId);

                if (effect == null)
                {
                    return RedirectToAction(nameof(Index));
                }

                InRaidEffect? inRaidEffect = groupStatus.InRaidEffects!.FirstOrDefault(x => x.EffectId == skillBook.Skill.EffectId);

                if (inRaidEffect == null)
                {
                    // There is no similar active effects, create one.
                    inRaidEffect = Core.CreateInRaidEffect(groupStatus.Id, effect);
                    //TODO! Make Separate Method for InRaidEffect. Use it in UseItem and UseSkill.
                    _bll.InRaidEffects.Add(inRaidEffect);
                }
                else
                {
                    // similar active effect exists, prolong.
                    inRaidEffect.Duration += effect.Duration;
                    inRaidEffect.Effect = null;
                    _bll.InRaidEffects.Update(inRaidEffect);
                }
            }
            
            battleCharacter.CurrentMp -= skillBook.Skill.ManaCost;
            _bll.BattleCharacters.Update(battleCharacter);
            _bll.InRaidSkills.Add(Core.CreateInRaidSkill(groupStatus.Id, skillBook.Skill));
            await _bll.SaveChangesAsync();
            
            return RedirectToAction(nameof(Camp));
        }

        [HttpPost, ActionName("NextCamp")]
        public async Task<IActionResult> Fight()
        {
            GroupStatus? groupStatus = await _bll.GroupStatuses.GetByLeaderIdFightPrepared(User.GetUserId());
            if (groupStatus == null)
            {
                return RedirectToAction(nameof(Index));
            }

            if (groupStatus.Dungeon == null || groupStatus.DungeonZoneNumber == null || groupStatus.BattleCharacters == null)
            {
                return RedirectToAction(nameof(Index));
            }

            if (groupStatus.DungeonZoneNumber == -1)
            {
                return RedirectToAction(nameof(Camp));
            }
            
            if (groupStatus.Dungeon.DungeonZones == null)
            {
                return RedirectToAction(nameof(Index));
            }

            // Check if dungeon doesn't contains dungeonZone with such dungeonZoneNumber
            if (groupStatus.Dungeon.DungeonZones.All(x => x.ZoneNumber != groupStatus.DungeonZoneNumber))
            {
                return RedirectToAction(nameof(Index));
            }

            GameSession gameSession = new GameSession(groupStatus);
            gameSession.Fight();
            
            GroupStatus afterFightGroupStatus = gameSession.GetGroupStatus();
            
            // Update status of battleCharacters.
            if (afterFightGroupStatus.BattleCharacters != null)
            {
                foreach (var battleCharacter in afterFightGroupStatus.BattleCharacters)
                {
                    _bll.BattleCharacters.Update(battleCharacter);
                }
            }

            // Assign next dungeon Zone. If End of dungeon or all members can't continue fight - assign zoneNumber -1.
            if (afterFightGroupStatus.Dungeon != null)
            {
                
                //TODO! put this in GameSession.cs
                if (afterFightGroupStatus.BattleCharacters.All(x => x.CurrentHp <= 0))
                {
                    afterFightGroupStatus.DungeonZoneNumber = -1;
                    afterFightGroupStatus.GameHistory += "Nobody can continue adventure.\nYou lost!";
                }
                else
                {
                    afterFightGroupStatus.DungeonZoneNumber = afterFightGroupStatus.Dungeon.DungeonZones!
                        .OrderBy(x => x.ZoneNumber)
                        .FirstOrDefault(x => x.ZoneNumber > afterFightGroupStatus.DungeonZoneNumber)
                        ?.ZoneNumber ?? -1;
                    if (afterFightGroupStatus.DungeonZoneNumber == -1)
                    {
                        afterFightGroupStatus.GameHistory += "Your group has passed all levels!\nCongratulations!";
                    }
                }
            }
            
            // Decrement Effect Duration. Delete if Duration is 0. Record Changes.
            if (afterFightGroupStatus.InRaidEffects != null)
            {
                foreach (var inRaidEffect in afterFightGroupStatus.InRaidEffects)
                {
                    inRaidEffect.Duration--;
                    inRaidEffect.Effect = null;
                    _bll.InRaidEffects.Update(inRaidEffect);
                }
            }

            // Decrement Skill Cooldown. Delete if Cooldown is 0. Record Changes.
            if (afterFightGroupStatus.InRaidSkills != null)
            {
                foreach (var inRaidSkill in afterFightGroupStatus.InRaidSkills)
                {
                    inRaidSkill.CurrentCooldown--;

                    if (inRaidSkill.CurrentCooldown < 0)
                    {
                        await _bll.InRaidSkills.RemoveAsync(inRaidSkill.Id);
                    }
                    else
                    {
                        inRaidSkill.Skill = null;
                        _bll.InRaidSkills.Update(inRaidSkill);
                    }
                }
            }

            // Add new drops in db. If drop already exist - update.
            List<InRaidItem> inRaidItems = (await _bll.InRaidItems.GetAllByGroupStatusIdAsync(User.GetUserId())).ToList();
            if (afterFightGroupStatus.InRaidItems != null)
            {

                foreach (var inRaidItem in afterFightGroupStatus.InRaidItems)
                {
                    inRaidItem.Item = null;

                    if (inRaidItems.Any(i => i.ItemId == inRaidItem.ItemId))
                    {
                        _bll.InRaidItems.Update(inRaidItem);
                    }
                    else
                    {
                        _bll.InRaidItems.Add(inRaidItem);
                    }
                }
            }

            // Make items unused. Record Changes.
            List<InventoryItem> invItems = (await _bll.InventoryItems.GetAllByCharacterIdAsync(User.GetUserId())).ToList();
            foreach (var invItem in invItems)
            {
                invItem.Used = false;
                invItem.Item = null;
                _bll.InventoryItems.Update(invItem);
            }
            
            // Change NavigationProperties to null to prevent further conflicts and errors.
            afterFightGroupStatus.Dungeon = null;
            afterFightGroupStatus.BattleCharacters = null;
            afterFightGroupStatus.InRaidEffects = null;
            afterFightGroupStatus.InRaidItems = null;
            afterFightGroupStatus.InRaidSkills = null;
            afterFightGroupStatus.GroupLeader = null;
            _bll.GroupStatuses.Update(afterFightGroupStatus);
            await _bll.SaveChangesAsync();
            
            // Return to Camp,
            return RedirectToAction(nameof(Camp));
        }

        public async Task<IActionResult> EndOfAdventure(bool retreat=false)
        {
            Character? character = await _bll.Characters.FirstOrDefaultAsync(User.GetUserId());
            GroupStatus? groupStatus = await _bll.GroupStatuses.GetByLeaderIdSensitive(User.GetUserId());

            if (character == null || groupStatus?.BattleCharacters == null)
            {
                return RedirectToAction(nameof(Index));
            }

            if (retreat)
            {
                foreach (var battleChar in groupStatus.BattleCharacters)
                {
                    battleChar.CurrentHp = 0;
                }
            }
            
            bool lost = groupStatus.BattleCharacters.All(x => x.CurrentHp <= 0);
            
            //Give all exp and stats back to original character.
            BattleCharacter? battleCharacter = groupStatus.BattleCharacters
                .FirstOrDefault(x => x.Id == groupStatus.BattleCharacterLeaderId);

            if (battleCharacter == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            character = Core.BattleCharacterToCharacterStats(battleCharacter, character);
            
            // Transfer loot and progress only if won. Otherwise delete group only.
            if (!lost)
            {

                //Convert all InRaidItems to InventoryItems, then delete InRaidItems.
                if (groupStatus.InRaidItems != null)
                {
                    List<InventoryItem> inventoryItems =
                        (await _bll.InventoryItems.GetAllByCharacterIdAsync(User.GetUserId())).ToList();
                    
                    // Convert InRaidItem to InventoryItem, then delete InRaidItem
                    foreach (var inRaidItem in groupStatus.InRaidItems)
                    {
                        InventoryItem? inventoryItem = 
                            inventoryItems.FirstOrDefault(x => x.ItemId == inRaidItem.ItemId);

                        if (inventoryItem == null)
                        {
                            inventoryItem = Core.CreateInventoryItem(User.GetUserId(), inRaidItem.ItemId, inRaidItem.Amount);
                            inventoryItem.Used = false;
                            _bll.InventoryItems.Add(inventoryItem);
                        }
                        else
                        {
                            inventoryItem.Amount += inRaidItem.Amount;
                            inventoryItem.Used = false;
                            inventoryItem.Item = null;
                            _bll.InventoryItems.Update(inventoryItem);
                        }
                        await _bll.InRaidItems.RemoveAsync(inRaidItem.Id);
                    }
                    
                }
            }
            
            //Delete all belonging InRaidSkills
            List<InRaidEffect> inRaidEffects = 
                (await _bll.InRaidEffects.GetAllByGroupStatusIdAsync(groupStatus.Id)).ToList();

            foreach (var inRaidEffect in inRaidEffects)
            {
                await _bll.InRaidEffects.RemoveAsync(inRaidEffect.Id);
            }
            
            //Delete all belonging InRaidEffects
            List<InRaidSkill> inRaidSkills =
                (await _bll.InRaidSkills.GetAllByGroupStatusIdAsync(groupStatus.Id)).ToList();

            foreach (var inRaidSkill in inRaidSkills)
            {
                await _bll.InRaidSkills.RemoveAsync(inRaidSkill.Id);
            }

            //Delete BattleCharacters
            List<BattleCharacter> battleCharacters = 
                (await _bll.BattleCharacters.GetAllByGroupStatusIdAsync(groupStatus.Id)).ToList();

            foreach (var bCharacter in battleCharacters)
            {
                await _bll.BattleCharacters.RemoveAsync(bCharacter.Id);
            }
            
            //Unassign group from character.
            character.LeaderGroup = null;
            _bll.Characters.Update(character);
            //delete groupstatus at end
            await _bll.GroupStatuses.RemoveAsync(groupStatus.Id);
            await _bll.SaveChangesAsync();
            
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Retreat()
        {
            return await EndOfAdventure(true);
        }
        
        // POST: Admin/Character/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCharacter([Bind("Id,Name,CreatedAt")] App.BLL.DTO.Character character)
        {
            if (ModelState.IsValid)
            {
                character.Id = User.GetUserId();
                character.AppUserId = User.GetUserId();
                
                _bll.Characters.Add(character);
                await _bll.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            return View(character);
        }
    }
}