﻿using App.BLL.DTO;
using App.Contracts.BLL;
using Base.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.User.Controllers;

[Area("User")]
[Authorize(Roles = "user")]
public class FriendsListController : Controller
{
    private readonly IAppBLL _bll;

    public FriendsListController(IAppBLL bll)
    {
        _bll = bll;
    }
    
    public async Task<IActionResult> Index()
    {
        var res =  await _bll.Friends.GetAllByIdAsync(User.GetUserId());
        return View(res);
    }
    
    // POST: 
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Index(string friendId)
    {

        var userId = User.GetUserId();
        
        // - Stop if can't parse Guid
        // - Stop if trying to add yourself
        // - Stop if friend already added
        // - Stop if character with such Guid does not exist
        if (!Guid.TryParse(friendId, out var id))
        {
            ViewData["ErrorMessage"] = "Invalid ID";
        }
        else if (id == userId)
        {
            ViewData["ErrorMessage"] = "Cannot add yourself as friend. Find some real friends >:(";
        }
        else if (_bll.Friends.Exists(userId, id))
        {
            ViewData["ErrorMessage"] = "User is already your friend";
        }
        else if (!await _bll.Characters.ExistsAsync(id))
        {
            ViewData["ErrorMessage"] = "Such user does not exist";
        }
        else
        {
            var friend = new Friend
            {
                CharacterId = User.GetUserId(),
                FriendId = id
            };
            _bll.Friends.Add(friend);
            await _bll.SaveChangesAsync();
        }

        var res =  await _bll.Friends.GetAllByIdAsync(User.GetUserId());
        return View(res);
    }
}