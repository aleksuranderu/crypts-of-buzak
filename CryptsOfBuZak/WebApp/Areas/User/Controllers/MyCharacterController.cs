﻿using App.Contracts.BLL;
using Base.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.User.Controllers;

[Area("User")]
[Authorize(Roles = "user")]
public class MyCharacterController : Controller
{
    private readonly IAppBLL _bll;
    
    public MyCharacterController(IAppBLL bll)
    {
        _bll = bll;
    }
    
    
    // GET
    public async Task<IActionResult> Index()
    {
        var res = await _bll.Characters.FirstOrDefaultSensitiveAsync(User.GetUserId());
        
        if (res == null)
        {
            return RedirectToAction("CreateCharacter", "Game");
        }
        
        return View(res);
    }
    
    // GET
    [HttpGet]
    public async Task<IActionResult> ChangeName()
    {
        var res = await _bll.Characters.FirstOrDefaultAsync(User.GetUserId());

        if (res == null)
        {
            return NotFound();
        }
        
        return View(res);
    }
    
    // POST
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> ChangeName(string name)
    {
        var character = (await _bll.Characters.FirstOrDefaultAsync(User.GetUserId()));

        if (character == null)
        {
            return NotFound();
        }
        
        character.Name = name;
        _bll.Characters.Update(character);
        await _bll.SaveChangesAsync();
        
        return RedirectToAction("Index");
    }
}