#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Contracts.BLL;
using App.DAL.EF;
using App.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App;
using Domain;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class EffectController : Controller
    {
        private readonly IAppBLL _bll;

        public EffectController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Admin/Effect
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Effects.GetAllAsync());
        }

        // GET: Admin/Effect/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var effect = await _bll.Effects
                .FirstOrDefaultAsync(id.Value);
            if (effect == null)
            {
                return NotFound();
            }

            return View(effect);
        }

        // GET: Admin/Effect/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Effect/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,PostEffectId")] App.BLL.DTO.Effect effect)
        {
            if (ModelState.IsValid)
            {
                _bll.Effects.Add(effect);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(effect);
        }

        // GET: Admin/Effect/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var effect = await _bll.Effects.FirstOrDefaultAsync(id.Value);
            if (effect == null)
            {
                return NotFound();
            }
            return View(effect);
        }

        // POST: Admin/Effect/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,PostEffectId")] App.BLL.DTO.Effect effect)
        {
            if (id != effect.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _bll.Effects.Update(effect);
                    await _bll.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await EffectExists(effect.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(effect);
        }

        // GET: Admin/Effect/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var effect = await _bll.Effects
                .FirstOrDefaultAsync(id.Value);
            if (effect == null)
            {
                return NotFound();
            }

            return View(effect);
        }

        // POST: Admin/Effect/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _bll.Effects.RemoveAsync(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> EffectExists(int id)
        {
            return await _bll.Effects.ExistsAsync(id);
        }
    }
}
