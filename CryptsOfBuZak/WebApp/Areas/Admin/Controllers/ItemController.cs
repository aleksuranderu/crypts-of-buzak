#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Contracts.BLL;
using App.DAL.EF;
using App.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App;
using Domain;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ItemController : Controller
    {
        private readonly IAppBLL _bll;

        public ItemController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Admin/Item
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Items.GetAllAsync());
        }

        // GET: Admin/Item/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _bll.Items.FirstOrDefaultAsync(id.Value);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // GET: Admin/Item/Create
        public IActionResult Create()
        {
            // ViewData["EffectId"] = new SelectList(_bll.Effects, "Id", "Description");
            // ViewData["SkillId"] = new SelectList(_bll.Skills, "Id", "Description");
            return View();
        }

        // POST: Admin/Item/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Category,SkillId,EffectId")] App.BLL.DTO.Item item)
        {
            if (ModelState.IsValid)
            {
                _bll.Items.Add(item);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            // ViewData["EffectId"] = new SelectList(_bll.Effects, "Id", "Description", item.EffectId);
            // ViewData["SkillId"] = new SelectList(_bll.Skills, "Id", "Description", item.SkillId);
            return View(item);
        }

        // GET: Admin/Item/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _bll.Items.FirstOrDefaultAsync(id.Value);
            if (item == null)
            {
                return NotFound();
            }
            // ViewData["EffectId"] = new SelectList(_bll.Effects, "Id", "Description", item.EffectId);
            // ViewData["SkillId"] = new SelectList(_bll.Skills, "Id", "Description", item.SkillId);
            return View(item);
        }

        // POST: Admin/Item/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Category,SkillId,EffectId")] App.BLL.DTO.Item item)
        {
            if (id != item.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _bll.Items.Update(item);
                    await _bll.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ItemExists(item.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            // ViewData["EffectId"] = new SelectList(_bll.Effects, "Id", "Description", item.EffectId);
            // ViewData["SkillId"] = new SelectList(_bll.Skills, "Id", "Description", item.SkillId);
            return View(item);
        }

        // GET: Admin/Item/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _bll.Items.FirstOrDefaultAsync(id.Value);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // POST: Admin/Item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _bll.Items.RemoveAsync(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> ItemExists(int id)
        {
            return await _bll.Items.ExistsAsync(id);
        }
    }
}
