#nullable disable
using App.Contracts.BLL;
using App.Contracts.DAL;
using App.BLL.DTO;
using App.Domain;
using Base.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "admin")]
    public class CharacterController : Controller
    {
        // private readonly ApplicationDbContext _context;
        private readonly IAppBLL _bll;

        public CharacterController(IAppBLL bll)
        {
            // _context = context;
            _bll = bll;
        }

        // GET: Admin/Character
        public async Task<IActionResult> Index()
        {
            var res =  await _bll.Characters.GetAllAsync();
            return View(res);
        }

        // GET: Admin/Character/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var character = await _bll.Characters.FirstOrDefaultAsync(id.Value);
            if (character == null)
            {
                return NotFound();
            }

            return View(character);
        }

        // GET: Admin/Character/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Character/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,CreatedAt")] App.BLL.DTO.Character character)
        {
            if (ModelState.IsValid)
            {
                character.AppUserId = User.GetUserId();
                
                character.Id = Guid.NewGuid();
                _bll.Characters.Add(character);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(character);
        }

        // GET: Admin/Character/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var character = await _bll.Characters.FirstOrDefaultAsync(id.Value);
            if (character == null)
            {
                return NotFound();
            }
            return View(character);
        }

        // POST: Admin/Character/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Name,CreatedAt")] App.BLL.DTO.Character character)
        {
            if (id != character.Id)
            {
                return NotFound();
            }


            // character.AppUserId = User.GetUserId();
            if (ModelState.IsValid)
            {
                try
                {
                    _bll.Characters.Update(character);
                    await _bll.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await CharacterExists(character.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            
            return View(character);
        }

        // GET: Admin/Character/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var character = await _bll.Characters.FirstOrDefaultAsync(id.Value);
            if (character == null)
            {
                return NotFound();
            }

            return View(character);
        }

        // POST: Admin/Character/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            await _bll.Characters.RemoveAsync(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> CharacterExists(Guid id)
        {
            return await _bll.Characters.ExistsAsync(id);
        }
    }
}
