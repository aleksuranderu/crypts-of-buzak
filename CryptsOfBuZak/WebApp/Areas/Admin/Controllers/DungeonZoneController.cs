#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.DAL.EF;
using App.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App;
using Domain;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class DungeonZoneController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DungeonZoneController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/DungeonZone
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.DungeonZones.Include(d => d.Dungeon);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Admin/DungeonZone/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dungeonZone = await _context.DungeonZones
                .Include(d => d.Dungeon)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dungeonZone == null)
            {
                return NotFound();
            }

            return View(dungeonZone);
        }

        // GET: Admin/DungeonZone/Create
        public IActionResult Create()
        {
            ViewData["DungeonId"] = new SelectList(_context.Dungeons, "Id", "Description");
            return View();
        }

        // POST: Admin/DungeonZone/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ZoneNumber,DungeonId")] DungeonZone dungeonZone)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dungeonZone);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            foreach (var modelStateValue in ModelState.Values)
            {
                foreach (var VARIABLE in modelStateValue.Errors)
                {
                    Console.WriteLine(VARIABLE.ErrorMessage);  
                }
            }
            ViewData["DungeonId"] = new SelectList(_context.Dungeons, "Id", "Description", dungeonZone.DungeonId);
            return View(dungeonZone);
        }

        // GET: Admin/DungeonZone/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dungeonZone = await _context.DungeonZones.FindAsync(id);
            if (dungeonZone == null)
            {
                return NotFound();
            }
            ViewData["DungeonId"] = new SelectList(_context.Dungeons, "Id", "Description", dungeonZone.DungeonId);
            return View(dungeonZone);
        }

        // POST: Admin/DungeonZone/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ZoneNumber,DungeonId")] DungeonZone dungeonZone)
        {
            if (id != dungeonZone.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dungeonZone);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DungeonZoneExists(dungeonZone.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DungeonId"] = new SelectList(_context.Dungeons, "Id", "Description", dungeonZone.DungeonId);
            return View(dungeonZone);
        }

        // GET: Admin/DungeonZone/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dungeonZone = await _context.DungeonZones
                .Include(d => d.Dungeon)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dungeonZone == null)
            {
                return NotFound();
            }

            return View(dungeonZone);
        }

        // POST: Admin/DungeonZone/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dungeonZone = await _context.DungeonZones.FindAsync(id);
            _context.DungeonZones.Remove(dungeonZone);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DungeonZoneExists(int id)
        {
            return _context.DungeonZones.Any(e => e.Id == id);
        }
    }
}
