#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Contracts.BLL;
using App.DAL.EF;
using App.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App;
using Domain;
using WebApp.DTO;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class DungeonController : Controller
    {
        private readonly IAppBLL _bll;

        public DungeonController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Admin/Dungeon
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Dungeons.GetAllAsync());
        }

        // GET: Admin/Dungeon/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dungeon = await _bll.Dungeons
                .FirstOrDefaultAsync(id.Value);
            if (dungeon == null)
            {
                return NotFound();
            }

            return View(dungeon);
        }

        // GET: Admin/Dungeon/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Dungeon/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,AverageLevel")] App.BLL.DTO.Dungeon dungeon)
        {
            if (ModelState.IsValid)
            {
                _bll.Dungeons.Add(dungeon);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dungeon);
        }

        // GET: Admin/Dungeon/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dungeon = await _bll.Dungeons.FirstOrDefaultAsync(id.Value);
            if (dungeon == null)
            {
                return NotFound();
            }
            return View(dungeon);
        }

        // POST: Admin/Dungeon/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,AverageLevel")] App.BLL.DTO.Dungeon dungeon)
        {
            if (id != dungeon.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _bll.Dungeons.Update(dungeon);
                    await _bll.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await DungeonExists(dungeon.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dungeon);
        }

        // GET: Admin/Dungeon/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dungeon = await _bll.Dungeons
                .FirstOrDefaultAsync(id.Value);
            if (dungeon == null)
            {
                return NotFound();
            }

            return View(dungeon);
        }

        // POST: Admin/Dungeon/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
            await _bll.Dungeons.RemoveAsync(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> DungeonExists(int id)
        {
            return await _bll.Dungeons.ExistsAsync(id);
        }
    }
}
