﻿using App.DAL.EF;
using App.Domain;
using App.Domain.Identity;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace WebApp;

public class AppDataHelper
{
    public static void SetupAppData(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration)
    {
        using var serviceScope = app.
            ApplicationServices.
            GetRequiredService<IServiceScopeFactory>().
            CreateScope();

        using var context = serviceScope
            .ServiceProvider.GetService<ApplicationDbContext>();

        if (context == null)
        {
            throw new ApplicationException("Problem in services. No db context.");
        }
        
        if (context.Database.ProviderName == "Microsoft.EntityFrameworkCore.InMemory") return;
        
        // TODO - Check database state
        // can't connect - wrong address
        // can't connect - wrong user/pass
        // can connect - but no database
        // can connect - there is database

        if (configuration.GetValue<bool>("DataInitialization:DropDatabase"))
        {
            context.Database.EnsureDeleted();
        }
        if (configuration.GetValue<bool>("DataInitialization:MigrateDatabase"))
        {
            context.Database.Migrate();
        }
        if (configuration.GetValue<bool>("DataInitialization:SeedIdentity"))
        {
            using var userManager = serviceScope.ServiceProvider.GetService<UserManager<AppUser>>();
            using var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<AppRole>>();

            if (userManager == null || roleManager == null)
            {
                throw new NullReferenceException("userManager or roleManager cannot be null!");
            }

            var roles = new string[]
            {
                "admin",
                "user",
            };

            foreach (var roleInfo in roles)
            {
                var role = roleManager.FindByNameAsync(roleInfo).Result;
                if (role == null)
                {
                    var identityResult = roleManager.CreateAsync(new AppRole()
                    {
                        Name = roleInfo
                    }).Result;
                    if (!identityResult.Succeeded)
                    {
                        throw new ApplicationException("Role creation failed");
                    }
                }
            }

                Guid g1 = Guid.Parse("11111111-1111-1111-1111-111111111111");
            Guid g2 = Guid.Parse("22222222-2222-2222-2222-222222222222");
            Guid g3 = Guid.Parse("33333333-3333-3333-3333-333333333333");
            Guid g4 = Guid.Parse("44444444-4444-4444-4444-444444444444");
            Guid g5 = Guid.Parse("55555555-5555-5555-5555-555555555555");

            var users = new (string username, string password, string roles, Guid guid)[]
            {
                ("admin@admin.in", "Kala.maja1", "user,admin", g1),
                ("notakaver@itcollege.ee", "Kala.maja1", "user,admin", g2),
                ("user1@test.ee", "Kala.maja1", "user", g3),
                ("user2@test.ee", "Kala.maja1", "user", g4),
                ("user3@test.ee", "Kala.maja1", "user", g5)
            };

            foreach (var userInfo in users)
            {
                var user = userManager.FindByEmailAsync(userInfo.username).Result;
                if (user == null)
                {
                    user = new AppUser()
                    {
                        Id = userInfo.guid,
                        Email = userInfo.username,
                        UserName = userInfo.username,
                        EmailConfirmed = true,
                    };
                    var identityResult = userManager.CreateAsync(user, userInfo.password).Result;
                    if (!identityResult.Succeeded)
                    {
                        
                        
                        List<String> errorsList = new List<string>();
                        foreach (var error in identityResult.Errors)
                        {
                            errorsList.Add($"{error.Code}: {error.Description}");
                        }
                        
                        throw new ApplicationException($"Cannot create user! \n" +
                                                       "---start of report--- \n " +
                                                       $"{String.Join('\n',errorsList)}" +
                                                       "\n---end of report---");
                    }
                }

                if (!string.IsNullOrWhiteSpace(userInfo.roles))
                {
                    var identityResultRole = userManager.AddToRolesAsync(user,
                        userInfo.roles.Split(",").Select(r => r.Trim())
                    ).Result;
                }
            }

        }
        if (configuration.GetValue<bool>("DataInitialization:SeedData"))
        {
            // TODO Add default data to database.

            // -----------------------EFFECTS (3)---------------------------------

            var e1 = new Effect
            {
                Id = 1,
                Name =
                {
                    ["en"] = "Heal I",
                    ["ru"] = "Исцеление I"
                },
                Description =
                {
                    ["en"] = "Heals characters by 5 HP.",
                    ["ru"] = "Исцеляет персонажей на 5 ХП"
                },
                Duration = 1,
                HpModifier = 5,
                MpModifier = 0,
                AtkModifier = 0,
                DefModifier = 0,
            };
            
            var e2 = new Effect
            {
                Id = 2,
                Name =
                {
                    ["en"] = "Attack I",
                    ["ru"] = "Атака I"
                },
                Description =
                {
                    ["en"] = "Increase characters' attack by 3 points.",
                    ["ru"] = "Увеличивает атаку персонажей на 3 очка."
                },
                Duration = 2,
                HpModifier = 0,
                MpModifier = 0,
                AtkModifier = 3,
                DefModifier = 0,
            };
            
            var e3 = new Effect
            {
                Id = 3,
                Name =
                {
                    ["en"] = "Attack II",
                    ["ru"] = "Атака II"
                },
                Description =
                {
                    ["en"] = "Increase characters' attack by 12 points.",
                    ["ru"] = "Увеличивает атаку персонажей на 12 очка."
                },
                Duration = 2,
                HpModifier = 0,
                MpModifier = 0,
                AtkModifier = 12,
                DefModifier = 0,
            };
            
            context.Effects.Add(e1);
            context.Effects.Add(e2);
            context.Effects.Add(e3);
            
            // -----------------------SKILLS (2)---------------------------------

            var s1 = new Skill
            {
                Id = 1,
                Name =
                {
                    ["en"] = "Help from God",
                    ["ru"] = "Помощь от Бога",
                },
                Description = 
                {
                    ["en"] = "Call for the help of God to heal your wounds.",
                    ["ru"] = "Взов к Богу с просьбой об исцелении ваших ран.",
                },
                EffectId = 1,
                Cooldown = 2,
                ManaCost = 1,
            };
            
            var s2 = new Skill
            {
                Id = 2,
                Name =
                {
                    ["en"] = "Rage",
                    ["ru"] = "Ярость",
                },
                Description = 
                {
                    ["en"] = "Enter into rage and increase your attack power.",
                    ["ru"] = "Войдите в ярость и увеличте свою силу.",
                },
                EffectId = 2,
                Cooldown = 3,
                ManaCost = 0,
            };

            context.Skills.Add(s1);
            context.Skills.Add(s2);
            
            // -----------------------ITEMS (3)---------------------------------

            var i1 = new Item
            {
                Id = 1,
                Name = 
                {
                    ["en"] = "Healing Powder (green)",
                    ["ru"] = "Исцеляющих порошок (зеленый)",
                },
                Description =
                {
                    ["en"] = "Green powder that has healing properties.",
                    ["ru"] = "Зеленый порошок с исцеляющими свойствами.",
                },
                Category = EItemCategory.Consumable,
                SkillId = null,
                EffectId = 1,
                Price = 20,
                MinLevelToBuy = 1
            };
            
            var i2 = new Item
            {
                Id = 2,
                Name = 
                {
                    ["en"] = "Goblin Ear",
                    ["ru"] = "Ухо Гоблина",
                },
                Description =
                {
                    ["en"] = "All that's left of a goblin...",
                    ["ru"] = "Все что осталось от гоблина...",
                },
                Category = EItemCategory.Miscellaneous,
                SkillId = null,
                EffectId = null,
                Price = 3
            };
            
            var i3 = new Item
            {
                Id = 3,
                Name = 
                {
                    ["en"] = "Rage SkillBook",
                    ["ru"] = "Книга навыков: Ярость",
                },
                Description =
                {
                    ["en"] = "Learn Rage skill using this book.",
                    ["ru"] = "Выучите навык Ярость",
                },
                Category = EItemCategory.SkillBook,
                SkillId = 2,
                EffectId = null,
                Price = 200,
                MinLevelToBuy = 10
            };
            
            var i4 = new Item
            {
                Id = 4,
                Name = 
                {
                    ["en"] = "Gnome's brick",
                    ["ru"] = "Кирпич гнома",
                },
                Description =
                {
                    ["en"] = "Looks like a normal brick but being in possession of this artifact gives you an increase in physical power",
                    ["ru"] = "На вид обычный кирпич, но в нем скрыты великие силы, которые передаются носителю.",
                },
                Category = EItemCategory.Artifact,
                SkillId = null,
                EffectId = 3,
                Price = 1000,
                MinLevelToBuy = 100
            };

            context.Items.Add(i1);
            context.Items.Add(i2);
            context.Items.Add(i3);
            context.Items.Add(i4);

            // -----------------------MONSTERS (2)---------------------------------
            
            var m1 = new Monster
            {
                Id = 1,
                Name =
                {
                    ["en"] = "Goblin"
                },
                HealthPoints = 5,
                AttackStat = 2,
                DefenceStat = 1,
                DropTable = null
            };
            
            var m2 = new Monster
            {
                Id = 2,
                Name =
                {
                    ["en"] = "Goblin But Stronger"
                },
                HealthPoints = 8,
                AttackStat = 4,
                DefenceStat = 2,
                DropTable = null
            };

            context.Monsters.Add(m1);
            context.Monsters.Add(m2);
            
            // -----------------------DROP TABLE (3)---------------------------------

            var dt1 = new DropTableItem
            {
                Id = Guid.NewGuid(),
                ItemId = 2,
                MonsterId = 1,
                DropMinimumAmount = 1,
                DropMaximumAmount = 2,
                DropChance = 0.8f
            };
            
            var dt2 = new DropTableItem
            {
                Id = Guid.NewGuid(),
                ItemId = 2,
                MonsterId = 2,
                DropMinimumAmount = 2,
                DropMaximumAmount = 2,
                DropChance = 1
            };
            
            var dt3 = new DropTableItem
            {
                Id = Guid.NewGuid(),
                ItemId = 3,
                MonsterId = 2,
                DropMinimumAmount = 1,
                DropMaximumAmount = 1,
                DropChance = 1
            };

            context.DropTableItems.Add(dt1);
            context.DropTableItems.Add(dt2);
            context.DropTableItems.Add(dt3);
            
            // -----------------------DUNGEONS (1)---------------------------------
            
            var d = new Dungeon
            {
                Name =
                {
                    ["en"] = "First Dungeon",
                    ["et"] = "Esimene vangikoobas",
                    ["ru"] = "Первое подземелье"
                },
                Description =
                {
                    ["en"] = "Your first dungeon. Time to kill those innocent rats",
                    ["en-GB"] = "Your first dungeon. Time to kill those bloody rats and steal their tea",
                    ["et"] = "Sinu esimene vangikoobas.",
                    ["ru"] = "Первое подземелье. Ничего примечательного о нем нет."
                },
                AverageLevel = 1
            };
            context.Dungeons.Add(d);
            context.SaveChanges();
            
            // -----------------------DUNGEON ZONES (2)---------------------------------

            var dz1 = new DungeonZone
            {
                Id = 1,
                DungeonId = 1,
                ZoneNumber = 1
            };
            
            var dz2 = new DungeonZone
            {
                Id = 2,
                DungeonId = 1,
                ZoneNumber = 2
            };
            
            context.DungeonZones.Add(dz1);
            context.DungeonZones.Add(dz2);
            
            // -----------------------MONSTER LISTS (3)---------------------------------
            
            var ml1 = new MonsterList
            {
                MonsterId = 1,
                MonsterAmount = 2,
                DungeonZoneId = 1
            };
            
            var ml2 = new MonsterList
            {
                MonsterId = 2,
                MonsterAmount = 1,
                DungeonZoneId = 1
            };
            
            var ml3 = new MonsterList
            {
                MonsterId = 2,
                MonsterAmount = 2,
                DungeonZoneId = 2
            };

            context.MonsterLists.Add(ml1);
            context.MonsterLists.Add(ml2);
            context.MonsterLists.Add(ml3);

            // -----------------------CHARACTERS (5)---------------------------------

            var c1 = new Character
            {
                Id = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                AppUserId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                Name = "Admin Character",
                Level = 1,
                Money = 510,
                ManaPoints = 5
            };
            
            var c2 = new Character
            {
                Id = Guid.Parse("22222222-2222-2222-2222-222222222222"),
                AppUserId = Guid.Parse("22222222-2222-2222-2222-222222222222"),
                Name = "NotAkaver's Character",
                Money = 10
            };
            
            var c3 = new Character
            {
                Id = Guid.Parse("33333333-3333-3333-3333-333333333333"),
                AppUserId = Guid.Parse("33333333-3333-3333-3333-333333333333"),
                Name = "User1 Character"
            };
            
            var c4 = new Character
            {
                Id = Guid.Parse("44444444-4444-4444-4444-444444444444"),
                AppUserId = Guid.Parse("44444444-4444-4444-4444-444444444444"),
                Name = "User2 Character"
            };
            
            var c5 = new Character
            {
                Id = Guid.Parse("55555555-5555-5555-5555-555555555555"),
                AppUserId = Guid.Parse("55555555-5555-5555-5555-555555555555"),
                Name = "User3 Character",
                Level = 50,
            };

            context.Characters.Add(c1);
            context.Characters.Add(c2);
            context.Characters.Add(c3);
            context.Characters.Add(c4);
            context.Characters.Add(c5);

            // -----------------------INVENTORY ITEM (3)---------------------------------

            var ii1 = new InventoryItem
            {
                ItemId = 1,
                Amount = 5,
                CharacterId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
            };
            
            var ii2 = new InventoryItem
            {
                ItemId = 2,
                Amount = 3,
                CharacterId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
            };
            
            var ii3 = new InventoryItem
            {
                ItemId = 3,
                Amount = 2,
                CharacterId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
            };
            
            var ii4 = new InventoryItem
            {
                ItemId = 4,
                Amount = 1,
                CharacterId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
            };

            context.InventoryItems.Add(ii1);
            context.InventoryItems.Add(ii2);
            context.InventoryItems.Add(ii3);
            context.InventoryItems.Add(ii4);
            
            // -----------------------SKILLBOOK (1)---------------------------------

            var sb = new SkillBook
            {
                CharacterId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                SkillId = 1,
            };

            context.SkillBook.Add(sb);

            // -----------------------FRIENDS (4)---------------------------------

            var f1 = new Friend
            {
                CharacterId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                FriendId = Guid.Parse("22222222-2222-2222-2222-222222222222")
            };
            
            var f2 = new Friend
            {
                CharacterId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                FriendId = Guid.Parse("33333333-3333-3333-3333-333333333333")
            };
            
            var f3 = new Friend
            {
                CharacterId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                FriendId = Guid.Parse("44444444-4444-4444-4444-444444444444")
            };

            var f4 = new Friend
            {
                CharacterId = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                FriendId = Guid.Parse("55555555-5555-5555-5555-555555555555")
            };
            
            context.Friends.Add(f1);
            context.Friends.Add(f2);
            context.Friends.Add(f3);
            context.Friends.Add(f4);
            
            //TODO Add more seed data.
            
            context.SaveChanges();
        }

    }
}