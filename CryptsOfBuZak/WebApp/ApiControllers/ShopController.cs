﻿#nullable disable
using App.BLL.CryptsOfBuzak;
using App.Contracts.BLL;
using App.Public.DTO.v1;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Base.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Character = App.BLL.DTO.Character;
using InventoryItem = App.BLL.DTO.InventoryItem;
using Item = App.BLL.DTO.Item;

namespace WebApp.ApiControllers;


[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiController]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
public class ShopController : ControllerBase
{

    private readonly IAppBLL _bll;
    private AutoMapper.IMapper _mapper;
    
    public ShopController(IAppBLL bll, IMapper mapper)
    {
        _bll = bll;
        _mapper = mapper;
    }
    
    /// <summary>
    /// Get all available to buy items on specific level.
    /// </summary>
    /// <param name="level">Conditional character level</param>
    /// <returns></returns>
    [Produces("application/json")]
    [Consumes("application/json")]
    [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.Monster>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [HttpGet("{level}")]
    [AllowAnonymous]
    public async Task<IEnumerable<App.Public.DTO.v1.Item>> GetShopList(int level)
    {
        return (await _bll.Items.GetAllAvailableByLevel(level)).Select(x => _mapper.Map<App.Public.DTO.v1.Item>(x));
    }
    
    /// <summary>
    /// Buy specified amount of specified items.
    /// </summary>
    /// <param name="id">Item's id</param>
    /// <param name="amount">Amount you want to buy</param>
    /// <returns>String with current character's balance</returns>
    [Produces("application/json")]
    [Consumes("application/json")]
    [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.Monster>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("Buy/{id}&{amount}")]
    public async Task<IActionResult> BuyItem(int id, int amount)
    {
        Character character =  await _bll.Characters.FirstOrDefaultAsync(User.GetUserId());
        Item item = await _bll.Items.FirstOrDefaultAsync(id);
        
        if (character == null || item == null)
        {
            return BadRequest("Item or character is null");
        }

        if (amount <= 0)
        {
            return BadRequest("Can't sell negative or null items");
        }

        if (amount * item.Price > character.Money)
        {
            return BadRequest("Character doesn't have enough money to buy this.");
        }

        if (item.MinLevelToBuy == null || character.Level < item.MinLevelToBuy)
        {
            return BadRequest("You can't buy this!");
        }
        
        character.Money -= item.Price * amount;
        _bll.Characters.Update(character);
            
        InventoryItem inventoryItem = 
            await _bll.InventoryItems.FirstOrDefaultAsync(User.GetUserId(), id);

        if (inventoryItem == null)
        {
            inventoryItem = Core.CreateInventoryItem(User.GetUserId(), id, amount);
        }
        else
        {
            inventoryItem.Amount += amount;
            inventoryItem.Item = null;
        }
            
        _bll.InventoryItems.Update(inventoryItem);
        await _bll.SaveChangesAsync();
        
        return Ok($"{character.Money}");
    }

    
    
    /// <summary>
    /// Sell specified amount of items from inventory.
    /// </summary>
    /// <param name="id">InventoryItem's id</param>
    /// <param name="amount">Amount you want to sell</param>
    /// <returns>String with current character's balance</returns>
    [Produces("application/json")]
    [Consumes("application/json")]
    [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.Monster>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("Sell/{id}&{amount}")]
    public async Task<IActionResult> SellItem(int id, int amount)
    {
        Character character =  await _bll.Characters.FirstOrDefaultAsync(User.GetUserId());
        InventoryItem item = await _bll.InventoryItems.FirstOrDefaultAsync(User.GetUserId(), id);

        if (character == null)
        {
            return BadRequest("Character not found!");
        }
        if (item == null)
        {
            return BadRequest("Item not found!");
        }
        if (item.Item == null)
        {
            return BadRequest("Item references null, not found!");
        }
        if (item.CharacterId != User.GetUserId())
        {
            return BadRequest("You don't own this item!");
        }
        if (amount < 1)
        {
            return BadRequest("You can't sell negative or zero items! Seriously, how you gonna do this?");
        }
        if (item.Amount < amount)
        {
            return BadRequest("You trying to sell more than you have!");
        }
        
        if (item.Amount - amount > 0)
        {
            item.Amount -= amount;
            character.Money += item.Item.Price * amount;
                
            item.Item = null;
            _bll.InventoryItems.Update(item);

        }
        else
        {
            await _bll.InventoryItems.RemoveAsync(User.GetUserId(), id);
            character.Money += item.Item.Price * amount;
        }

        _bll.Characters.Update(character);
        await _bll.SaveChangesAsync();
        
        return Ok($"{character.Money}");
    }
}