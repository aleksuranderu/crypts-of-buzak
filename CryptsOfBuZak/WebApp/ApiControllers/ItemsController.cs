#nullable disable
using App.Contracts.BLL;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ItemsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public ItemsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/Items
        /// <summary>
        /// Gets a list of all Items.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Item>), StatusCodes.Status200OK)]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<App.Public.DTO.v1.Item>> GetItems()
        {
            return (await _bll.Items.GetAllAsync()).Select(x => _mapper.Map<App.Public.DTO.v1.Item>(x));
        }

        // GET: api/Items/5
        /// <summary>
        /// Get Item specified by id.
        /// </summary>
        /// <param name="id">Item's id</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Item>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<App.Public.DTO.v1.Item>> GetItem(int id)
        {
            var item = await _bll.Items.FirstOrDefaultAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            var itemPublic = _mapper.Map<App.Public.DTO.v1.Item>(item);
            
            return itemPublic;
        }

        // PUT: api/Items/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates Item specified by id
        /// </summary>
        /// <param name="id">Item's id</param>
        /// <param name="item">Item's parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Item>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutItem(int id, App.Public.DTO.v1.Item item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            if (item.SkillId != null)
            {
                if (!await _bll.Skills.ExistsAsync(item.SkillId.GetValueOrDefault()))
                {
                    return NotFound($"Skill with id {item.SkillId} was not found");
                }
            }


            if (item.EffectId != null)
            {
                if (!await _bll.Effects.ExistsAsync(item.EffectId.GetValueOrDefault()))
                {
                    return NotFound($"Effect with id {item.EffectId} was not found");
                }   
            }

            var itemOriginal = await _bll.Effects.FirstOrDefaultAsync(id);

            var itemBll = _mapper.Map<App.BLL.DTO.Item>(item);
            
            if (itemOriginal != null)
            {
                itemBll.Name = itemOriginal.Name;
                itemBll.Name[Thread.CurrentThread.CurrentUICulture.Name] = item.Name;
                itemBll.Description = itemOriginal.Description;
                itemBll.Description[Thread.CurrentThread.CurrentUICulture.Name] = item.Description;
            }
            
            _bll.Items.Update(itemBll);

            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Items
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new Item.
        /// </summary>
        /// <param name="item">Item's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Item), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<App.Public.DTO.v1.Item>> PostItem(App.Public.DTO.v1.Item item)
        {
            if (item.SkillId != null)
            {
                if (!await _bll.Skills.ExistsAsync(item.SkillId.GetValueOrDefault()))
                {
                    return NotFound($"Skill with id {item.SkillId} was not found");
                }
            }


            if (item.EffectId != null)
            {
                if (!await _bll.Effects.ExistsAsync(item.EffectId.GetValueOrDefault()))
                {
                    return NotFound($"Effect with id {item.EffectId} was not found");
                }   
            }
            
            var itemBll = _mapper.Map<App.BLL.DTO.Item>(item);
            
            _bll.Items.Add(itemBll);
            await _bll.SaveChangesAsync();

            return CreatedAtAction("GetItem", new { id = item.Id }, item);
        }

        // DELETE: api/Items/5
        /// <summary>
        /// Deletes Item specified by id.
        /// </summary>
        /// <param name="id">Item's id</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItem(int id)
        {
            var item = await _bll.Items.FirstOrDefaultAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            _bll.Items.Remove(item);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool ItemExists(int id)
        {
            return _bll.Items.Exists(id);
        }
    }
}
