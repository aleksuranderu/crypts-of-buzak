#nullable disable
using System.Net;
using App.Contracts.BLL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(Roles="admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DropTableItemsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public DropTableItemsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/DropTableItems
        /// <summary>
        /// Gets a list of all DropTableItem objects.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.DropTableItem>), StatusCodes.Status200OK)]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<App.Public.DTO.v1.Monster.DropTableItem>> GetDropTableItems()
        {
            return (await _bll.DropTableItems.GetAllAsync())
                .Select(x => _mapper.Map<App.Public.DTO.v1.Monster.DropTableItem>(x));
        }

        // GET: api/DropTableItems/5
        /// <summary>
        /// Gets a DropTableItem object specified by id.
        /// </summary>
        /// <param name="id">DropTableItem's Guid</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.DropTableItem>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<App.Public.DTO.v1.Monster.DropTableItem>> GetDropTableItem(Guid id)
        {
            var dropTableItem = await _bll.DropTableItems.FirstOrDefaultAsync(id);

            if (dropTableItem == null)
            {
                return NotFound();
            }

            return _mapper.Map<App.Public.DTO.v1.Monster.DropTableItem>(dropTableItem);
        }

        // PUT: api/DropTableItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates DropTableItem specified by id.
        /// </summary>
        /// <param name="id">DropTableItem's Guid</param>
        /// <param name="dropTableItem">DropTableItem parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Effect>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDropTableItem(Guid id, App.Public.DTO.v1.Monster.DropTableItem dropTableItem)
        {
            if (id != dropTableItem.Id)
            {
                return BadRequest();
            }
            
            if (!await _bll.Items.ExistsAsync(dropTableItem.ItemId))
            {
                return NotFound($"Item with id {dropTableItem.ItemId} was not found");
            }
            
            if (!await _bll.Monsters.ExistsAsync(dropTableItem.MonsterId))
            {
                return NotFound($"Monster with id {dropTableItem.MonsterId} was not found");
            }

            var dropTableItemBll = _mapper.Map<App.BLL.DTO.DropTableItem>(dropTableItem);

            _bll.DropTableItems.Update(dropTableItemBll);

            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DropTableItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DropTableItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new DropTableItem with specified parameters.
        /// </summary>
        /// <param name="dropTableItem">DropTableItem's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Monster.DropTableItem), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        public async Task<ActionResult<App.Public.DTO.v1.Monster.DropTableItem>> PostDropTableItem(App.Public.DTO.v1.Monster.DropTableItem dropTableItem)
        {
            if (!await _bll.Items.ExistsAsync(dropTableItem.ItemId))
            {
                return NotFound($"Item with id {dropTableItem.ItemId} was not found");
            }
            
            if (!await _bll.Monsters.ExistsAsync(dropTableItem.MonsterId))
            {
                return NotFound($"Monster with id {dropTableItem.MonsterId} was not found");
            }
         
            var dropTableItemBll = _mapper.Map<App.BLL.DTO.DropTableItem>(dropTableItem);
            
            if (dropTableItem.Id == null)
            {
                dropTableItemBll.Id = Guid.NewGuid();    
            }

            _bll.DropTableItems.Add(dropTableItemBll);
            await _bll.SaveChangesAsync();

            return CreatedAtAction("GetDropTableItem", new { id = dropTableItem.Id }, dropTableItem);
        }

        // DELETE: api/DropTableItems/5
        /// <summary>
        /// Deletes DropTableItem specified by id.
        /// </summary>
        /// <param name="id">DropTableItem's Guid</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDropTableItem(Guid id)
        {
            var dropTableItem = await _bll.DropTableItems.FirstOrDefaultAsync(id);
            if (dropTableItem == null)
            {
                return NotFound();
            }

            _bll.DropTableItems.Remove(dropTableItem);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool DropTableItemExists(Guid id)
        {
            return _bll.DropTableItems.Exists(id);
        }
    }
}
