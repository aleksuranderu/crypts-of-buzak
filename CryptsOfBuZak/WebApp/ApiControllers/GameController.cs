﻿using System.Text.Json;
using App.BLL.CryptsOfBuzak;
using App.Contracts.BLL;
using AutoMapper;
using Base.Extensions;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace WebApp.ApiControllers;

[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
public class GameController : ControllerBase
{
    private readonly IAppBLL _bll;
    private AutoMapper.IMapper _mapper;

    public GameController(IAppBLL bll, IMapper mapper)
    {
        _bll = bll;
        _mapper = mapper;
    }
    
    // GET: api/Dungeons
    /// <summary>
    /// Gets a current group status object.
    /// </summary>
    /// <returns></returns>
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.GroupStatus.GroupStatus>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpGet]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<ActionResult<App.Public.DTO.v1.GroupStatus.GroupStatus>> GetGroupStatus()
    {
        var userId = User.GetUserId();
        if (!await _bll.GroupStatuses.ExistsAsync(userId))
        {
            return NotFound("Game was not started.");
        }
        
        return _mapper.Map<App.Public.DTO.v1.GroupStatus.GroupStatus>(await _bll.GroupStatuses.GetByLeaderIdSensitive(userId));
    }
    
    /// <summary>
    /// Creates a new group status object.
    /// </summary>
    /// <returns></returns>
    [Produces("application/json")]
    [ProducesResponseType(typeof(App.Public.DTO.v1.GroupStatus.GroupStatus), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [HttpPost("Create/{dungeonId}")]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<ActionResult<App.Public.DTO.v1.GroupStatus.GroupStatus>> CreateGroupStatus(int dungeonId)
    {
        if (!_bll.Dungeons.Exists(dungeonId))
        {
            return BadRequest("No such dungeon");
        }

        var userId = User.GetUserId();

        if (!_bll.Characters.Exists(userId))
        {
            return BadRequest("Character was not created");
        }

        var character = await _bll.Characters.FirstOrDefaultAsync(userId);
        
        App.BLL.DTO.GroupStatus.GroupStatus? res = await _bll.GroupStatuses.FirstOrDefaultAsync(User.GetUserId());
        bool createNew = false;
            
        if(res == null)
        {
            // Group Doesn't exist yet, create one.
            res = Core.CreateGroupStatus(character!);
            createNew = true;
        }
        
        // Check if Dungeon actually exists and if group already exists
        if (await _bll.Dungeons.FirstOrDefaultAsync(dungeonId) == null || res.DungeonZoneNumber != null)
        {
            return BadRequest("Game in progress");
        }
            
        res.DungeonId = dungeonId;

        if (createNew)
        {
            _bll.GroupStatuses.Add(res);
        }
        else
        {
            _bll.GroupStatuses.Update(res);
        }
            
        await _bll.SaveChangesAsync();

        return Ok(res);
    }

    /// <summary>
    /// Adds a new party member to group.
    /// </summary>
    /// <param name="friendId">Friend's Guid</param>
    /// <returns></returns>
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpPost("AddToParty/{friendId}")]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<ActionResult<string>> AddFriendToParty(Guid friendId)
    {
        var gs = await _bll.GroupStatuses.FirstOrDefaultAsync(User.GetUserId());
        if (gs == null)
        {
            return NotFound("No group yet.");
        }
        var userId = User.GetUserId();
        // if DungeonZoneNumber is not null - adventure already in progress.
        if (gs?.DungeonZoneNumber != null)
        {
            return BadRequest("Game already started");
        }

        List<Guid> guidList = new List<Guid>();
            
        if (gs!.GameHistory != "")
        {
            guidList = JsonSerializer.Deserialize<List<Guid>>(gs.GameHistory)!;
        }
        if (guidList.Contains(friendId))
        {
            //Such friend already exists in party! || Party is already full, maximum 3 friends!
            return BadRequest("Friend already in party.");
        }
        if (guidList.Count >= 3)
        {
            //Such friend already exists in party! || Party is already full, maximum 3 friends!
            return BadRequest("Party is already full.");
        }

        // Checks whether a character with such guid in your friendList.
        if (!_bll.Friends.Exists(userId, friendId))
        {
            return NotFound($"You don't have friend with id {friendId}");
        }

        guidList.Add(friendId);
        gs.GameHistory = JsonSerializer.Serialize(guidList);
        _bll.GroupStatuses.Update(gs);
        await _bll.SaveChangesAsync();

        return gs.GameHistory;
    }

    /// <summary>
    /// Removes party member from group
    /// </summary>
    /// <param name="friendId">Friend's Guid</param>
    /// <returns></returns>
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpPost("RemoveFromParty/{friendId}")]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<ActionResult<string>> RemoveFriendFromParty(Guid friendId)
    {
        var gs = await _bll.GroupStatuses.FirstOrDefaultAsync(User.GetUserId());
        if (gs == null)
        {
            return NotFound("No group yet.");
        }
        var userId = User.GetUserId();
        // if DungeonZoneNumber is not null - adventure already in progress.
        if (gs?.DungeonZoneNumber != null)
        {
            return BadRequest("Game already started");
        }

        if (string.IsNullOrEmpty(gs!.GameHistory))
        {
            return BadRequest("No party members in group");
        }
        
        List<Guid> guidList = JsonSerializer.Deserialize<List<Guid>>(gs.GameHistory)!;
        
        if (guidList.All(x => x != friendId))
        {
            //Character with such id doesn't exist in your party!
            return NotFound($"There is no member with id {friendId}");
        }

        guidList.Remove(friendId);
        gs.GameHistory = JsonSerializer.Serialize(guidList);
        _bll.GroupStatuses.Update(gs);
        await _bll.SaveChangesAsync();

        return gs.GameHistory;
    }

    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpGet("Retreat")]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<ActionResult> RetreatFromDungeon()
    {
        var groupStatus = await _bll.GroupStatuses.FirstOrDefaultAsync(User.GetUserId());
        if (groupStatus == null)
        {
            return NotFound("No group yet.");
        }

        if (groupStatus.DungeonId != null && !(await _bll.Dungeons.ExistsAsync(groupStatus.DungeonId.Value)))
        {
            return NotFound("Group status with bad dungeon id");
        }

        await EndOfAdventureAsync(true);
        return NoContent();
    }

    [Produces("application/json")]
    [ProducesResponseType(typeof(App.Public.DTO.v1.GroupStatus.GroupStatus), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpGet("Play")]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<ActionResult<App.Public.DTO.v1.GroupStatus.GroupStatus>> PlayDungeon()
    {
        var groupStatus = await _bll.GroupStatuses.FirstOrDefaultAsync(User.GetUserId());
        if (groupStatus == null)
        {
            return NotFound("No group yet.");
        }

        if (groupStatus.DungeonId != null && !(await _bll.Dungeons.ExistsAsync(groupStatus.DungeonId.Value)))
        {
            return NotFound("Group status with bad dungeon id");
        }

        if (groupStatus.DungeonZoneNumber == null)
        {
            return await StartDungeonAsync(groupStatus);
        }

        return await FightAsync();
    }

    private async Task<ActionResult<App.Public.DTO.v1.GroupStatus.GroupStatus>> StartDungeonAsync(App.BLL.DTO.GroupStatus.GroupStatus groupStatus)
    {
        var dungeon = await _bll.Dungeons.FirstOrDefaultByIdAsync(groupStatus.DungeonId!.Value);

        if (dungeon?.DungeonZones == null)
        {
            return NotFound("Dungeon has no zones to explore.");
        }
            
        List<Guid> guidList = new List<Guid>();
            
        if (!groupStatus.GameHistory.IsNullOrEmpty())
        {
            guidList = JsonSerializer.Deserialize<List<Guid>>(groupStatus.GameHistory) ?? new List<Guid>();
        }
            
        groupStatus.BattleCharacters = new List<App.BLL.DTO.GroupStatus.BattleCharacter>();
            
        // Create leader's battleCharacter.
        var character = (await _bll.Characters.FirstOrDefaultAsync(User.GetUserId()))!;
        var leader =
            Core.CreateBattleCharacter(character);
        leader.GroupStatusId = groupStatus.Id;
            
        _bll.BattleCharacters.Add(leader);
            
        groupStatus.BattleCharacters.Add(leader);
        groupStatus.BattleCharacterLeaderId = leader.Id;

        // Create leader's teammates battleCharacters .
        foreach (var friend in (await _bll.Friends.GetAllByIdAsync(User.GetUserId())).ToList())
        {
            var friendCharacter = friend.FriendCharacter;
            if (friendCharacter != null && guidList.Exists(x => x == friend.FriendId))
            {
                // pay friends for hiring. tax is 50%.
                friendCharacter.Money += (friendCharacter.Level * 5);
                character.Money -= (friendCharacter.Level * 10);
                    
                var bc = Core.CreateBattleCharacter(friendCharacter);
                bc.GroupStatusId = groupStatus.Id;
                _bll.BattleCharacters.Add(bc);
                _bll.Characters.Update(friendCharacter);
                groupStatus.BattleCharacters!.Add(bc);
            }
        }

        _bll.Characters.Update(character);

        // Assign start Dungeon level. Picks lowest zoneNumber as first
        if (dungeon.DungeonZones.Count > 0)
        {
            int startLevel = dungeon.DungeonZones.Min(x => x.ZoneNumber);
            groupStatus.DungeonZoneNumber = startLevel;
        }
        else
        {
            //Can't Start an Empty Dungeon!
            return RedirectToAction(nameof(Index));
        }

        //Make all items unused
        List<App.BLL.DTO.InventoryItem> inventoryItems = 
            (await _bll.InventoryItems.GetAllByCharacterIdAsync(User.GetUserId())).ToList();

        foreach (var inventoryItem in inventoryItems)
        {
            if (inventoryItem.Used)
            {
                inventoryItem.Used = false;
                inventoryItem.Item = null;
                _bll.InventoryItems.Update(inventoryItem);
            }
        }
            
        groupStatus.GameHistory = "";
        groupStatus.BattleCharacters = null;
        _bll.GroupStatuses.Update(groupStatus);
        await _bll.SaveChangesAsync();
            
        // Adventure has started! Good luck!
        await CampAsync();
        
        return Ok(groupStatus);
    }
    
    private async Task<ActionResult<App.Public.DTO.v1.GroupStatus.GroupStatus>> FightAsync()
    {
        var groupStatus = await _bll.GroupStatuses.GetByLeaderIdFightPrepared(User.GetUserId());
        if (groupStatus == null) return NotFound();
        if (groupStatus.Dungeon == null) return BadRequest();
        
        if (groupStatus.DungeonZoneNumber == -1)
        {
            await EndOfAdventureAsync();
            return NoContent();
        }
            
        if (groupStatus.Dungeon.DungeonZones == null)
        {
            return BadRequest("dungeon zones are null");
        }

        // Check if dungeon doesn't contains dungeonZone with such dungeonZoneNumber
        if (groupStatus.Dungeon.DungeonZones.All(x => x.ZoneNumber != groupStatus.DungeonZoneNumber))
        {
            return BadRequest($"no dungeon with {groupStatus.DungeonZoneNumber}");
        }

        GameSession gameSession = new GameSession(groupStatus);
        gameSession.Fight();
            
        var afterFightGroupStatus = gameSession.GetGroupStatus();
            
        // Update status of battleCharacters.
        if (afterFightGroupStatus.BattleCharacters != null)
        {
            foreach (var battleCharacter in afterFightGroupStatus.BattleCharacters)
            {
                _bll.BattleCharacters.Update(battleCharacter);
            }
        }

        // Assign next dungeon Zone. If End of dungeon or all members can't continue fight - assign zoneNumber -1.
        if (afterFightGroupStatus.Dungeon != null)
        {
                
            //TODO! put this in GameSession.cs
            if (afterFightGroupStatus.BattleCharacters.All(x => x.CurrentHp <= 0))
            {
                afterFightGroupStatus.DungeonZoneNumber = -1;
                afterFightGroupStatus.GameHistory += "Nobody can continue adventure.\nYou lost!";
            }
            else
            {
                afterFightGroupStatus.DungeonZoneNumber = afterFightGroupStatus.Dungeon.DungeonZones!
                    .OrderBy(x => x.ZoneNumber)
                    .FirstOrDefault(x => x.ZoneNumber > afterFightGroupStatus.DungeonZoneNumber)
                    ?.ZoneNumber ?? -1;
                if (afterFightGroupStatus.DungeonZoneNumber == -1)
                {
                    afterFightGroupStatus.GameHistory += "Your group has passed all levels!\nCongratulations!";
                }
            }
        }
            
        // Decrement Effect Duration. Delete if Duration is 0. Record Changes.
        if (afterFightGroupStatus.InRaidEffects != null)
        {
            foreach (var inRaidEffect in afterFightGroupStatus.InRaidEffects)
            {
                inRaidEffect.Duration--;
                inRaidEffect.Effect = null;
                _bll.InRaidEffects.Update(inRaidEffect);
            }
        }

        // Decrement Skill Cooldown. Delete if Cooldown is 0. Record Changes.
        if (afterFightGroupStatus.InRaidSkills != null)
        {
            foreach (var inRaidSkill in afterFightGroupStatus.InRaidSkills)
            {
                inRaidSkill.CurrentCooldown--;

                if (inRaidSkill.CurrentCooldown < 0)
                {
                    await _bll.InRaidSkills.RemoveAsync(inRaidSkill.Id);
                }
                else
                {
                    inRaidSkill.Skill = null;
                    _bll.InRaidSkills.Update(inRaidSkill);
                }
            }
        }

        // Add new drops in db. If drop already exist - update.
        List<App.BLL.DTO.GroupStatus.InRaidItem> inRaidItems = (await _bll.InRaidItems.GetAllByGroupStatusIdAsync(User.GetUserId())).ToList();
        if (afterFightGroupStatus.InRaidItems != null)
        {

            foreach (var inRaidItem in afterFightGroupStatus.InRaidItems)
            {
                inRaidItem.Item = null;

                if (inRaidItems.Any(i => i.ItemId == inRaidItem.ItemId))
                {
                    _bll.InRaidItems.Update(inRaidItem);
                }
                else
                {
                    _bll.InRaidItems.Add(inRaidItem);
                }
            }
        }

        // Make items unused. Record Changes.
        List<App.BLL.DTO.InventoryItem> invItems = (await _bll.InventoryItems.GetAllByCharacterIdAsync(User.GetUserId())).ToList();
        foreach (var invItem in invItems)
        {
            invItem.Used = false;
            invItem.Item = null;
            _bll.InventoryItems.Update(invItem);
        }
            
        // Change NavigationProperties to null to prevent further conflicts and errors.
        afterFightGroupStatus.Dungeon = null;
        afterFightGroupStatus.BattleCharacters = null;
        afterFightGroupStatus.InRaidEffects = null;
        afterFightGroupStatus.InRaidItems = null;
        afterFightGroupStatus.InRaidSkills = null;
        afterFightGroupStatus.GroupLeader = null;
        _bll.GroupStatuses.Update(afterFightGroupStatus);
        await _bll.SaveChangesAsync();
            
        // Return to Camp,
        await CampAsync();
        
        return Ok(_mapper.Map<App.Public.DTO.v1.GroupStatus.GroupStatus>(afterFightGroupStatus));
    }

    private async Task CampAsync()
    {
        var groupStatus = await _bll.GroupStatuses.GetByLeaderIdSensitive(User.GetUserId());

        if (groupStatus == null)
        {
            return;
        }
            
        // Auto-use artifacts. If effect from artifact already exists or artifact is used - do nothing.
        List<App.BLL.DTO.InventoryItem> artifacts =
            groupStatus.GroupLeader!.InventoryItems!
                .Where(x => x.Item is {Category: EItemCategory.Artifact}).ToList();

        foreach (var artifact in artifacts)
        {
            if (artifact.Item != null && artifact.Used == false)
            {
                var inRaidEffect =
                    groupStatus.InRaidEffects!
                        .FirstOrDefault(x => x.EffectId == artifact.Item.EffectId);

                var effect = await _bll.Effects.FirstOrDefaultAsync(artifact.Item.EffectId!.Value);

                if (effect != null)
                {
                    if (inRaidEffect == null)
                    {
                        inRaidEffect = Core.CreateInRaidEffect(groupStatus.Id, effect);
                        _bll.InRaidEffects.Add(inRaidEffect);
                        inRaidEffect.Effect = effect;
                        groupStatus.InRaidEffects?.Add(inRaidEffect);
                        artifact.Used = true;
                    }
                }
            }
        }

        foreach (var effect in groupStatus.InRaidEffects!)
        {
            foreach (var battleChar in groupStatus.BattleCharacters!)
            {
                battleChar.AtkStat += effect.Effect!.AtkModifier;
                battleChar.DefStat += effect.Effect!.DefModifier;
                battleChar.CurrentHp += effect.Effect!.HpModifier;
                battleChar.CurrentMp += effect.Effect!.MpModifier;
            }
        }
        await _bll.SaveChangesAsync();
    }

    private async Task EndOfAdventureAsync(bool retreat=false)
    {
        var character = await _bll.Characters.FirstOrDefaultAsync(User.GetUserId());
        var groupStatus = await _bll.GroupStatuses.GetByLeaderIdSensitive(User.GetUserId());

        if (character == null || groupStatus?.BattleCharacters == null)
        {
            return;
        }

        if (retreat)
        {
            foreach (var battleChar in groupStatus.BattleCharacters)
            {
                battleChar.CurrentHp = 0;
            }
        }
            
        bool lost = groupStatus.BattleCharacters.All(x => x.CurrentHp <= 0);
            
        //Give all exp and stats back to original character.
        var battleCharacter = groupStatus.BattleCharacters
            .FirstOrDefault(x => x.Id == groupStatus.BattleCharacterLeaderId);

        if (battleCharacter == null)
        {
            return;
        }
            
        character = Core.BattleCharacterToCharacterStats(battleCharacter, character);
            
        // Transfer loot and progress only if won. Otherwise delete group only.
        if (!lost)
        {

            //Convert all InRaidItems to InventoryItems, then delete InRaidItems.
            if (groupStatus.InRaidItems != null)
            {
                List<App.BLL.DTO.InventoryItem> inventoryItems =
                    (await _bll.InventoryItems.GetAllByCharacterIdAsync(User.GetUserId())).ToList();
                    
                // Convert InRaidItem to InventoryItem, then delete InRaidItem
                foreach (var inRaidItem in groupStatus.InRaidItems)
                {
                    var inventoryItem = 
                        inventoryItems.FirstOrDefault(x => x.ItemId == inRaidItem.ItemId);

                    if (inventoryItem == null)
                    {
                        inventoryItem = Core.CreateInventoryItem(User.GetUserId(), inRaidItem.ItemId, inRaidItem.Amount);
                        inventoryItem.Used = false;
                        _bll.InventoryItems.Add(inventoryItem);
                    }
                    else
                    {
                        inventoryItem.Amount += inRaidItem.Amount;
                        inventoryItem.Used = false;
                        inventoryItem.Item = null;
                        _bll.InventoryItems.Update(inventoryItem);
                    }
                    await _bll.InRaidItems.RemoveAsync(inRaidItem.Id);
                }
                    
            }
        }
            
        //Delete all belonging InRaidSkills
        List<App.BLL.DTO.GroupStatus.InRaidEffect> inRaidEffects = 
            (await _bll.InRaidEffects.GetAllByGroupStatusIdAsync(groupStatus.Id)).ToList();

        foreach (var inRaidEffect in inRaidEffects)
        {
            await _bll.InRaidEffects.RemoveAsync(inRaidEffect.Id);
        }
            
        //Delete all belonging InRaidEffects
        List<App.BLL.DTO.GroupStatus.InRaidSkill> inRaidSkills =
            (await _bll.InRaidSkills.GetAllByGroupStatusIdAsync(groupStatus.Id)).ToList();

        foreach (var inRaidSkill in inRaidSkills)
        {
            await _bll.InRaidSkills.RemoveAsync(inRaidSkill.Id);
        }

        //Delete BattleCharacters
        List<App.BLL.DTO.GroupStatus.BattleCharacter> battleCharacters = 
            (await _bll.BattleCharacters.GetAllByGroupStatusIdAsync(groupStatus.Id)).ToList();

        foreach (var bCharacter in battleCharacters)
        {
            await _bll.BattleCharacters.RemoveAsync(bCharacter.Id);
        }
            
        //Unassign group from character.
        character.LeaderGroup = null;
        _bll.Characters.Update(character);
        //delete groupstatus at end
        await _bll.GroupStatuses.RemoveAsync(groupStatus.Id);
        await _bll.SaveChangesAsync();
    }
    
    /// <summary>
    /// Use specific skill in camp
    /// </summary>
    /// <param name="id">Id of used Skill (Not SkillBook!)</param>
    /// <returns>Object of used skill</returns>
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpPost("UseSkill/{id}")]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<ActionResult<string>> UseSkill(int id)
    {
        var  groupStatus = (await _bll.GroupStatuses.GetByLeaderIdSensitive(User.GetUserId()))!;

        if (groupStatus == null || _bll.Characters.FirstOrDefault(User.GetUserId()) == null)
        {
            return BadRequest("Adventure not started yet!");
        }
        
        var battleCharacter = (await _bll.BattleCharacters.FirstOrDefaultAsync(groupStatus.BattleCharacterLeaderId));
        
        if (groupStatus.GroupLeader == null || battleCharacter == null || _bll.Characters.FirstOrDefault(User.GetUserId()) == null)
        {
            return BadRequest();
        }
        
        if (groupStatus.InRaidSkills == null || groupStatus.GroupLeader!.SkillBook == null)
        {
            return BadRequest();
        }
        
        var inRaidSkills = groupStatus.InRaidSkills;
        var skillBook = groupStatus.GroupLeader.SkillBook.FirstOrDefault(x => x.SkillId == id);

        if (skillBook?.Skill == null)
        {
            return BadRequest();
        }

        if (inRaidSkills.Any(x => x.SkillId == skillBook.SkillId))
        {
            return BadRequest("Skill is on cooldown!");
        }

        if (skillBook.Skill.EffectId == default || skillBook.Skill.ManaCost > battleCharacter.CurrentMp)
        {
            return BadRequest();
        }

        var effect = _bll.Effects.FirstOrDefault(skillBook.Skill.EffectId);

        if (effect == null)
        {
            return RedirectToAction(nameof(Index));
        }

        var inRaidEffect = groupStatus.InRaidEffects!.FirstOrDefault(x => x.EffectId == skillBook.Skill.EffectId);

        if (inRaidEffect == null)
        {
            // There is no similar active effects, create one.
            inRaidEffect = Core.CreateInRaidEffect(groupStatus.Id, effect);
            _bll.InRaidEffects.Add(inRaidEffect);
        }
        else
        {
            // similar active effect exists, prolong.
            inRaidEffect.Duration += effect.Duration;
            inRaidEffect.Effect = null;
            _bll.InRaidEffects.Update(inRaidEffect);
        }

        
        battleCharacter.CurrentMp -= skillBook.Skill.ManaCost;
        _bll.BattleCharacters.Update(battleCharacter);
        _bll.InRaidSkills.Add(Core.CreateInRaidSkill(groupStatus.Id, skillBook.Skill));
        await _bll.SaveChangesAsync();

        return Ok(_mapper.Map<App.Public.DTO.v1.Skill>(skillBook.Skill));
    }
        
    
    /// <summary>
    /// Use specific item in camp
    /// </summary>
    /// <param name="id">Id of used Item (Not InventoryItem!)</param>
    /// <returns>Object of used InventoryItem</returns>
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpPost("UseItem/{id}")]
    [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<ActionResult<string>> UseItem(int id)
    {
        var groupStatus = (await _bll.GroupStatuses.GetByLeaderIdSensitive(User.GetUserId()))!;

        if (groupStatus == null || _bll.Characters.FirstOrDefault(User.GetUserId()) == null)
        {
            return BadRequest("Game not started yet!");
        }
        
        if (groupStatus.GroupLeader?.InventoryItems == null || groupStatus.InRaidEffects == null)
        {
            return BadRequest("InRaidEffects or InventoryItems are null");
        }

        var inventoryItem = (await _bll.InventoryItems
                .GetAllByCharacterIdAsync(User.GetUserId()))
            .FirstOrDefault(x => x.ItemId == id) ;

        if (inventoryItem?.Item == null)
        {
            return BadRequest("InventoryItem is null");
        }

        if (inventoryItem.Used)
        {
            return BadRequest("item is already used!");
        }

        if (inventoryItem.Item.Category == EItemCategory.SkillBook)
        {
            if (inventoryItem.Item.SkillId == null || groupStatus.GroupLeader.SkillBook == null)
            {
                return BadRequest("Skill is null.");
            }
            
            if (groupStatus.GroupLeader.SkillBook.Any(x => x.SkillId ==  inventoryItem.Item.SkillId))
            {
                return BadRequest("Skill is already learned!");
            }
            
            // Skill is not learned yet, add to skillbook.
            var skillBook = Core.CreateSkillBook(groupStatus.GroupLeaderId, inventoryItem.Item.SkillId.Value);
            _bll.SkillBooks.Add(skillBook);

        } else if (inventoryItem.Item.Category == EItemCategory.Consumable)
        {
            if (inventoryItem.Item.EffectId != null)
            {
                var inRaidEffect =
                    groupStatus.InRaidEffects.FirstOrDefault(x => x.EffectId == inventoryItem.Item.EffectId);

                if (inRaidEffect != null)
                {
                    // Effect already exist. prolong.
                    inRaidEffect.Duration += inRaidEffect.Effect!.Duration;
                    inRaidEffect.Effect = null;
                    _bll.InRaidEffects.Update(inRaidEffect);
                }
                else
                {
                    // Effect doesn't exist yet. Create
                    if (inventoryItem.Item.EffectId != null)
                    {
                        var effect = (await _bll.Effects.FirstOrDefaultAsync(inventoryItem.Item.EffectId.Value));
                        if (effect != null)
                        {
                            inRaidEffect = Core.CreateInRaidEffect(groupStatus.Id, effect);
                            _bll.InRaidEffects.Add(inRaidEffect);
                        }
                    }
                }
            }
            else
            {
                return BadRequest("Item is null");
            }
        }
        else
        {
            return BadRequest("You can't use anything except skillBooks and consumables Here!");
        }
        // Good Ending
        inventoryItem.Used = true;
        inventoryItem.Amount--;
        
        var inventoryItemResult = inventoryItem;

        inventoryItem.Item = null;
        _bll.InventoryItems.Update(inventoryItem);
        await _bll.SaveChangesAsync();
        return Ok(_mapper.Map<App.Public.DTO.v1.InventoryItem>(inventoryItemResult));
    }
}