#nullable disable
using App.Contracts.BLL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MonstersController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public MonstersController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/Monsters
        /// <summary>
        /// Gets a list of all Monsters.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.Monster>), StatusCodes.Status200OK)]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<App.Public.DTO.v1.Monster.Monster>> GetMonsters()
        {
            return (await _bll.Monsters.GetAllAsync())
                .Select(x => _mapper.Map<App.Public.DTO.v1.Monster.Monster>(x));
        }

        // GET: api/Monsters/5
        /// <summary>
        /// Gets a Monster specified by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.Monster>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<App.Public.DTO.v1.Monster.Monster>> GetMonster(int id)
        {
            var monster = await _bll.Monsters.FirstOrDefaultAsync(id);

            if (monster == null)
            {
                return NotFound();
            }

            return _mapper.Map<App.Public.DTO.v1.Monster.Monster>(monster);
        }

        // PUT: api/Monsters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates Monster specified by id.
        /// </summary>
        /// <param name="id">Monster's id</param>
        /// <param name="monster">Monster's parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.Monster>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMonster(int id, App.Public.DTO.v1.Monster.Monster monster)
        {
            if (id != monster.Id)
            {
                return BadRequest();
            }

            var monsterOriginal = await _bll.Monsters.FirstOrDefaultAsync(id);

            var monsterBll = _mapper.Map<App.BLL.DTO.Monster>(monster);
            
            if (monsterOriginal != null)
            {
                monsterBll.Name = monsterOriginal.Name;
                monsterBll.Name[Thread.CurrentThread.CurrentUICulture.Name] = monster.Name;
            }
            
            _bll.Monsters.Update(monsterBll);

            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MonsterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Monsters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new Monster.
        /// </summary>
        /// <param name="monster">Monster's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Monster.Monster), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        public async Task<ActionResult<App.Public.DTO.v1.Monster.Monster>> PostMonster(App.Public.DTO.v1.Monster.Monster monster)
        {
            _bll.Monsters.Add(_mapper.Map<App.BLL.DTO.Monster>(monster));
            await _bll.SaveChangesAsync();

            return CreatedAtAction("GetMonster", new { id = monster.Id }, monster);
        }

        // DELETE: api/Monsters/5
        /// <summary>
        /// Deletes Monster spevified by id.
        /// </summary>
        /// <param name="id">Monster's id</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMonster(int id)
        {
            var monster = await _bll.Monsters.FirstOrDefaultAsync(id);
            if (monster == null)
            {
                return NotFound();
            }

            _bll.Monsters.Remove(monster);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool MonsterExists(int id)
        {
            return _bll.Monsters.Exists(id);
        }
    }
}
