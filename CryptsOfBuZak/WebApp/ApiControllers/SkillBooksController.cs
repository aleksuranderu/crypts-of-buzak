using App.Contracts.BLL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Base.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class SkillBooksController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public SkillBooksController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/SkillBooks
        /// <summary>
        /// Gets a list of all learned skills by characters.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.SkillBook>), StatusCodes.Status200OK)]
        [HttpGet]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<App.Public.DTO.v1.SkillBook>> GetSkillBooks()
        {
            return (await _bll.SkillBooks.GetAllAsync())
                .Select(x => _mapper.Map<App.Public.DTO.v1.SkillBook>(x));
        }

        // GET: api/SkillBooks/Character
        /// <summary>
        /// Gets a list of all skills learned by currently signed in user.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.SkillBook>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("Character")]
        [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<IEnumerable<App.Public.DTO.v1.SkillBook>>> GetSkillBookByCharacter()
        {
            var id = User.GetUserId();

            return await GetByIdAsync(id);
        }
        
        // GET: api/SkillBooks/Character/5
        /// <summary>
        /// Gets a list of all learned skills by specified character
        /// </summary>
        /// <param name="id">Character's id</param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.SkillBook>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("Character/{id}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<IEnumerable<App.Public.DTO.v1.SkillBook>>> GetSkillBookById(Guid id)
        {
            return await GetByIdAsync(id);
        }

        // POST: api/SkillBooks
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new learned skill for specified user.
        /// </summary>
        /// <param name="skillBook">User and Skill id</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.SkillBook), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<App.Public.DTO.v1.SkillBook>> PostSkillBook(App.Public.DTO.v1.SkillBook skillBook)
        {
            if (_bll.SkillBooks.Exists(skillBook.CharacterId, skillBook.SkillId))
            {
                return BadRequest("Already exists");
            }
            
            if (!await _bll.Characters.ExistsAsync(skillBook.CharacterId))
            {
                return NotFound($"Character with id {skillBook.CharacterId} was not found");
            }
            
            if (!await _bll.Skills.ExistsAsync(skillBook.SkillId))
            {
                return NotFound($"Skill with id {skillBook.SkillId} was not found");
            }
            
            _bll.SkillBooks.Add(_mapper.Map<App.BLL.DTO.SkillBook>(skillBook));
            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SkillBookExists(skillBook.CharacterId, skillBook.SkillId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSkillBookById", new { id = skillBook.CharacterId }, skillBook);
        }

        // DELETE: api/SkillBooks/{Guid}/5
        /// <summary>
        /// Deletes learned skill from specified character.
        /// </summary>
        /// <param name="characterId">Character's id</param>
        /// <param name="skillId">Skill's id</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{characterId}/{skillId}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteSkillBook(Guid characterId, int skillId)
        {
            var skillBook = await _bll.SkillBooks.FirstOrDefaultAsync(characterId, skillId);
            if (skillBook == null)
            {
                return NotFound();
            }

            _bll.SkillBooks.Remove(skillBook);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool SkillBookExists(Guid characterId, int skillId)
        {
            return _bll.SkillBooks.Exists(characterId, skillId);
        }

        private async Task<ActionResult<IEnumerable<App.Public.DTO.v1.SkillBook>>> GetByIdAsync(Guid id)
        {
            if (!await _bll.Characters.ExistsAsync(id))
            {
                return NotFound();
            }
            
            var skillBook = (await _bll.SkillBooks.GetAllByCharacterIdAsync(id))
                .Select(x => _mapper.Map<App.Public.DTO.v1.SkillBook>(x)).ToList();

            if (!skillBook.Any())
            {
                return NotFound();
            }

            return skillBook;
        }
    }
}
