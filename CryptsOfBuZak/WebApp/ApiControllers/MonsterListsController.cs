#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Contracts.BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using App.DAL.EF;
using App.Domain;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(Roles="admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MonsterListsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public MonsterListsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/MonsterLists
        /// <summary>
        /// Gets a list of all MonsterLists.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.DropTableItem>), StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<IEnumerable<App.Public.DTO.v1.Monster.MonsterList>> GetMonsterLists()
        {
            return (await _bll.MonsterLists.GetAllAsync()).Select(x => _mapper.Map<App.Public.DTO.v1.Monster.MonsterList>(x));
        }

        // GET: api/MonsterLists/5
        /// <summary>
        /// Gets a MonsterList specified by id.
        /// </summary>
        /// <param name="id">MonsterList's id</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Monster.DropTableItem>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<App.Public.DTO.v1.Monster.MonsterList>> GetMonsterList(int id)
        {
            var monsterList = await _bll.MonsterLists.FirstOrDefaultAsync(id);

            if (monsterList == null)
            {
                return NotFound();
            }

            return _mapper.Map<App.Public.DTO.v1.Monster.MonsterList>(monsterList);
        }

        // PUT: api/MonsterLists/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates MonsterList specified by id.
        /// </summary>
        /// <param name="id">MonsterList's id</param>
        /// <param name="monsterList">MonsterList's parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Effect>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMonsterList(int id, App.Public.DTO.v1.Monster.MonsterList monsterList)
        {
            if (id != monsterList.Id)
            {
                return BadRequest();
            }

            //

            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MonsterListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MonsterLists
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new MonsterList.
        /// </summary>
        /// <param name="monsterList">MonsterList's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Monster.DropTableItem), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        public async Task<ActionResult<App.Public.DTO.v1.Monster.MonsterList>> PostMonsterList(App.Public.DTO.v1.Monster.MonsterList monsterList)
        {
            _bll.MonsterLists.Add(_mapper.Map<App.BLL.DTO.MonsterList>(monsterList));
            await _bll.SaveChangesAsync();

            return CreatedAtAction("GetMonsterList", new { id = monsterList.Id }, monsterList);
        }

        // DELETE: api/MonsterLists/5
        /// <summary>
        /// Deletes MonsterList specified by id.
        /// </summary>
        /// <param name="id">MonsterList's id</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMonsterList(int id)
        {
            var monsterList = await _bll.MonsterLists.FirstOrDefaultAsync(id);
            if (monsterList == null)
            {
                return NotFound();
            }

            _bll.MonsterLists.Remove(monsterList);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool MonsterListExists(int id)
        {
            return _bll.MonsterLists.Exists(id);
        }
    }
}
