﻿using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using App.Domain.Identity;
using Base.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApp.DTO;
using App.DAL.EF;
using App.Public.DTO.v1.Identity;
using Microsoft.EntityFrameworkCore;

namespace WebApp.ApiControllers.Identity;

[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/identity/[controller]/[action]")]
[ApiController]
public class AccountController : ControllerBase
{

    // Api for sign in
    private readonly SignInManager<AppUser> _signInManager;
    // Api for managing user
    private readonly UserManager<AppUser> _userManager;
    private readonly ILogger<AccountController> _logger;
    private readonly Random _rnd = new();
    private readonly IConfiguration _configuration;
    private readonly ApplicationDbContext _context;


    public AccountController(SignInManager<AppUser> signInManager, UserManager<AppUser> userManager, ILogger<AccountController> logger, IConfiguration configuration, ApplicationDbContext context)
    {
        _signInManager = signInManager;
        _userManager = userManager;
        _logger = logger;
        _configuration = configuration;
        _context = context;
    }
    
    /// <summary>
    /// Logs into rest backend with your user and generates JWT.
    /// </summary>
    /// <param name="loginData">Username and password</param>
    /// <returns>JWT and refresh token</returns>
    [Produces("application/json")]
    [Consumes("application/json")]
    [ProducesResponseType(typeof(JwtResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpPost]
    public async Task<ActionResult<JwtResponse>> LogIn([FromBody] Login loginData)
    {
       // verify username
       var appUser = await _userManager.FindByEmailAsync(loginData.Email);
       if (appUser == null)
       {
           _logger.LogWarning("WebApi login failed: email {} not found", loginData.Email);
           await Task.Delay(_rnd.Next(50,800));
           return NotFound("User/Password error");
       }
       
       // verify username and password
       var result = await _signInManager.CheckPasswordSignInAsync(appUser, loginData.Password, false);
       if (!result.Succeeded)
       {
           _logger.LogWarning("WebApi login failed: password mismatch for user {}", loginData.Email);
           await Task.Delay(_rnd.Next(50,1000));
           return NotFound("User/Password error");
       }
       
       // get claims base user
       var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(appUser);
       if (claimsPrincipal == null)
       {
           _logger.LogWarning("Could not get claimsPrincipal for user {}", loginData.Email);
           await Task.Delay(_rnd.Next(50,1000));
           return NotFound("User/Password error");
       }
       
       appUser.RefreshTokens = await _context
           .Entry(appUser)
           .Collection(a => a.RefreshTokens!)
           .Query()
           .Where(t => t.AppUserId == appUser.Id)
           .ToListAsync();

       foreach (var userRefreshToken in appUser.RefreshTokens)
       {
           if (userRefreshToken.TokenExpirationDateTime < DateTime.UtcNow &&
               userRefreshToken.PreviousTokenExpirationDateTime < DateTime.UtcNow)
           {
               _context.RefreshTokens.Remove(userRefreshToken);
           }
       }
        
       var refreshToken = new RefreshToken();
       refreshToken.AppUserId = appUser.Id;
       _context.RefreshTokens.Add(refreshToken);
       await _context.SaveChangesAsync();
       
       // generate jwt
       var jwt = IdentityExtensions.GenerateJwt(
           claimsPrincipal.Claims, 
           _configuration["JWT:Key"], 
           _configuration["JWT:Issuer"], 
           _configuration["JWT:Issuer"],
           DateTime.Now.AddMinutes(_configuration.GetValue<int>("JWT:ExpireInMinutes")));

       var res = new JwtResponse()
       {
           Token = jwt,
           RefreshToken = refreshToken.Token,
           Email = appUser.Email,
           UserName = appUser.UserName
       };

       return Ok(res);
    }

    /// <summary>
    /// Registers new user.
    /// </summary>
    /// <param name="registrationData">Email, password and username</param>
    /// <returns>JWT and refresh token</returns>
    [Produces("application/json")]
    [Consumes("application/json")]
    [ProducesResponseType(typeof(JwtResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpPost]
    public async Task<ActionResult<JwtResponse>> Register(Register registrationData)
    {
        // verify user

        var appUser = await _userManager.FindByEmailAsync(registrationData.Email);
        if (appUser != null)
        {
            _logger.LogWarning("User with email {} is already exists", registrationData.Email);
            
            //TODO!! Implement given error system to other errors!
            // Create error payload
            var errorResponse = new RestApiErrorResponse()
            {
                Type = "https://datatracker.ietf.org/doc/html/rfc7231#section-6.5.1",
                Title = "App error",
                Status = HttpStatusCode.BadRequest,
                TraceId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
            };

            // Add error description
            errorResponse.Errors["email"] = new List<string>()
            {
                "Email Already registered"
            };
            
            return BadRequest(errorResponse);
        }

        var refreshToken = new App.Domain.Identity.RefreshToken();
        
        appUser = new AppUser()
        {
            //TODO! IF SOMETHING WENT WRONG WITH CHARACTERID, IMPLEMENT HERE, APRIL 1 9.00-11.59/12.00-2.59 ECHO
            UserName = registrationData.UserName,
            Email = registrationData.Email,
            RefreshTokens = new List<App.Domain.Identity.RefreshToken>()
            {
                refreshToken
            }
        };

        // create user (system will do it)

        var result = await _userManager.CreateAsync(appUser, registrationData.Password);
        if (!result.Succeeded)
        {
            return BadRequest(result);
        }
        
        // add user to "user" role
        await _userManager.AddToRoleAsync(appUser, "user");

        // result = await _userManager.AddClaimAsync(user, new Claim("aspnet.firstname", user.FirstName); 
        
        // get full user from system with fixed data
        
        // this may cause problem later, too bad !
        appUser = await _userManager.FindByEmailAsync(appUser.Email);
        if (appUser == null)
        {
            _logger.LogWarning("User with email {} is not found after registration", registrationData.Email);
            return BadRequest("User with such name or email is not found after registration");
        }

        // get claims base user
        var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(appUser);
        if (claimsPrincipal == null)
        {
            _logger.LogWarning("Could not get claimsPrincipal for user {}", registrationData.Email);
            return NotFound("User/Password error");
        }
        // generate jwt
        var jwt = IdentityExtensions.GenerateJwt(
            claimsPrincipal.Claims, 
            _configuration["JWT:Key"], 
            _configuration["JWT:Issuer"], 
            _configuration["JWT:Issuer"],
            DateTime.Now.AddMinutes(_configuration.GetValue<int>("JWT:ExpireInMinutes"))
            );

        var res = new JwtResponse()
        {
            Token = jwt,
            RefreshToken = refreshToken.Token,
            Email = appUser.Email,
            UserName = appUser.UserName
        };

        return Ok(res);
    }

    /// <summary>
    /// Refreshes token.
    /// </summary>
    /// <param name="refreshTokenModel">JWT and refresh token</param>
    /// <returns>JWT and refresh token</returns>
    [Produces("application/json")]
    [Consumes("application/json")]
    [ProducesResponseType(typeof(JwtResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpPost]
    public async Task<ActionResult> RefreshToken([FromBody] RefreshTokenModel refreshTokenModel)
    {

        // get info from jwt
        JwtSecurityToken jwtToken;

        try
        {
            jwtToken = new JwtSecurityTokenHandler().ReadJwtToken(refreshTokenModel.Jwt);
            if (jwtToken == null)
            {
                return BadRequest("No token");
            }
        }
        catch (Exception e)
        {
            return BadRequest($"Can't parse the token, {e.Message}");
        }

        //TODO! validate token signature


        var userEmail = jwtToken.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value;
        if (userEmail == null)
        {
            return BadRequest("No email in jwt");
        }

        // get user and tokens
        var appUser = await _userManager.FindByEmailAsync(userEmail);
        if (appUser == null)
        {
            return NotFound($"User with email {userEmail} not found");
        }

        // load and compare refresh tokens

        await _context.Entry(appUser).Collection(u => u.RefreshTokens!)
            .Query()
            .Where(x =>
                (x.Token == refreshTokenModel.RefreshToken && x.TokenExpirationDateTime > DateTime.UtcNow) ||
                (x.PreviousToken == refreshTokenModel.RefreshToken &&
                 x.PreviousTokenExpirationDateTime > DateTime.UtcNow)
            )
            .ToListAsync();

        if (appUser.RefreshTokens == null)
        {
            return Problem("RefreshTokens collection is null");
        }

        if (appUser.RefreshTokens.Count == 0)
        {
            return Problem("RefreshTokens collection is empty, no valid refresh tokens found");
        }

        if (appUser.RefreshTokens.Count != 1)
        {
            return Problem("More than one valid refresh token found!");
        }

        // get claims base user
        var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(appUser);
        if (claimsPrincipal == null)
        {
            _logger.LogWarning("Could not get claimsPrincipal for user {}", userEmail);
            return NotFound("User/Password error");
        }

        // generate jwt
        var jwt = IdentityExtensions.GenerateJwt(
            claimsPrincipal.Claims,
            _configuration["JWT:Key"],
            _configuration["JWT:Issuer"],
            _configuration["JWT:Issuer"],
            DateTime.Now.AddMinutes(_configuration.GetValue<int>("JWT:ExpireInMinutes"))
        );
        // make new refresh token, obsolete old ones
        var refreshToken = appUser.RefreshTokens.First();
        if (refreshToken.Token == refreshTokenModel.RefreshToken)
        {
            refreshToken.PreviousToken = refreshToken.Token;
            refreshToken.PreviousTokenExpirationDateTime =
                DateTime.UtcNow.AddMinutes(_configuration.GetValue<int>("JWT:ExpireInMinutes"));

            refreshToken.Token = Guid.NewGuid().ToString();

            //TODO! get AddDays() from Appsettings.json instead of hardcode 
            refreshToken.TokenExpirationDateTime = DateTime.UtcNow.AddDays(7);

            await _context.SaveChangesAsync();
        }

        var res = new JwtResponse()
        {
            Token = jwt,
            RefreshToken = refreshToken.Token,
            Email = appUser.Email,
            UserName = appUser.UserName
        };

        return Ok(res);
    }
}