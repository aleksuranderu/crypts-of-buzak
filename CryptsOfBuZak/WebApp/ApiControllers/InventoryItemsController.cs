﻿#nullable disable
using App.Contracts.BLL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Base.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class InventoryItemsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public InventoryItemsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/InventoryItems
        /// <summary>
        /// Gets a list of all InventoryItems.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.InventoryItem>), StatusCodes.Status200OK)]
        [HttpGet]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<App.Public.DTO.v1.InventoryItem>> GetInventoryItems()
        {
            return (await _bll.InventoryItems.GetAllAsync())
                .Select(x => _mapper.Map<App.Public.DTO.v1.InventoryItem>(x));
        }

        // GET: api/InventoryItems/Character
        /// <summary>
        /// Gets a list of all InventoryItems for the signed in user.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.InventoryItem>), StatusCodes.Status200OK)]
        [HttpGet("Character")]
        [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<IEnumerable<App.Public.DTO.v1.InventoryItem>>> GetInventoryItemByCharacter()
        {
            var id = User.GetUserId();

            return await GetByIdAsync(id);
        }
        
        // GET: api/InventoryItems/5
        /// <summary>
        /// Gets a list of all InventoryItems for user with specified id.
        /// </summary>
        /// <param name="id">User's Guid</param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.InventoryItem>), StatusCodes.Status200OK)]
        [HttpGet("{id}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<IEnumerable<App.Public.DTO.v1.InventoryItem>>> GetInventoryItemById(Guid id)
        {
            return await GetByIdAsync(id);
        }

        // PUT: api/InventoryItems/{guid}/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates InventoryItem for the specified character
        /// </summary>
        /// <param name="characterId">Character's Guid</param>
        /// <param name="itemId">Item's id</param>
        /// <param name="inventoryItem">InventoryItem's parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.InventoryItem>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{characterId}/{itemId}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> PutInventoryItem(Guid characterId, int itemId, App.Public.DTO.v1.InventoryItem inventoryItem)
        {
            if (characterId != inventoryItem.CharacterId && itemId != inventoryItem.ItemId)
            {
                return BadRequest();
            }

            var inventoryItemBll = _mapper.Map<App.BLL.DTO.InventoryItem>(inventoryItem);

            if (_bll.InventoryItems.Exists(inventoryItem.CharacterId, inventoryItem.ItemId))
            {
                return BadRequest("Already exists");
            }
            
            if (!await _bll.Characters.ExistsAsync(inventoryItem.CharacterId))
            {
                return NotFound($"Character with id {inventoryItem.CharacterId} was not found");
            }
            
            if (!await _bll.Items.ExistsAsync(inventoryItem.ItemId))
            {
                return NotFound($"Item with id {inventoryItem.ItemId} was not found");
            }
            
            _bll.InventoryItems.Update(inventoryItemBll);
            
            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InventoryItemExists(characterId, itemId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/InventoryItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new InventoryItem.
        /// </summary>
        /// <param name="inventoryItem">InventoryItem's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.InventoryItem), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<App.Public.DTO.v1.InventoryItem>> PostInventoryItem(App.Public.DTO.v1.InventoryItem inventoryItem)
        {
            if (_bll.InventoryItems.Exists(inventoryItem.CharacterId, inventoryItem.ItemId))
            {
                return BadRequest("Already exists");
            }
            
            if (!await _bll.Characters.ExistsAsync(inventoryItem.CharacterId))
            {
                return NotFound($"Character with id {inventoryItem.CharacterId} was not found");
            }
            
            if (!await _bll.Items.ExistsAsync(inventoryItem.ItemId))
            {
                return NotFound($"Item with id {inventoryItem.ItemId} was not found");
            }
            
            _bll.InventoryItems.Add(_mapper.Map<App.BLL.DTO.InventoryItem>(inventoryItem));
            await _bll.SaveChangesAsync();

            return CreatedAtAction("GetInventoryItemById", new { id = inventoryItem.CharacterId }, inventoryItem);
        }

        // DELETE: api/InventoryItems/{characterId}/5
        /// <summary>
        /// Deletes InventoryItem from character specified by id.
        /// </summary>
        /// <param name="characterId">Character's Guid</param>
        /// <param name="itemId">InventoryItem's parameters</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{characterId}/{itemId}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteInventoryItem(Guid characterId, int itemId)
        {
            var inventoryItem = await _bll.InventoryItems.FirstOrDefaultAsync(characterId, itemId);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            await _bll.InventoryItems.RemoveAsync(characterId, itemId);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool InventoryItemExists(Guid characterId, int itemId)
        {
            return _bll.InventoryItems.Exists(characterId, itemId);
        }
        
        private async Task<ActionResult<IEnumerable<App.Public.DTO.v1.InventoryItem>>> GetByIdAsync(Guid characterId)
        {
            if (!await _bll.Characters.ExistsAsync(characterId))
            {
                return NotFound();
            }
            
            var inventoryItems = (await _bll.InventoryItems.GetAllByCharacterIdAsync(characterId))
                .Select(x => _mapper.Map<App.Public.DTO.v1.InventoryItem>(x)).ToList();

            if (!inventoryItems.Any())
            {
                return NotFound();
            }

            return inventoryItems;
        }
    }
}
