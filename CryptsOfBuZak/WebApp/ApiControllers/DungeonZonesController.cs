﻿using App.Contracts.BLL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class DungeonZonesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public DungeonZonesController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/DungeonZones
        /// <summary>
        /// Gets a list of all DungeonZones.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.DungeonZone>), StatusCodes.Status200OK)]
        [AllowAnonymous]
        [HttpGet]
        public async Task<IEnumerable<App.Public.DTO.v1.DungeonZone>> GetDungeonZones()
        {
            return (await _bll.DungeonZones.GetAllAsync()).Select(x => _mapper.Map<App.Public.DTO.v1.DungeonZone>(x));
        }

        // GET: api/DungeonZones/5
        /// <summary>
        /// Gets a DungeonZone specified by id.
        /// </summary>
        /// <param name="id">DungeonZone's id</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.DungeonZone>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<App.Public.DTO.v1.DungeonZone>> GetDungeonZone(int id)
        {
            var dungeonZone = await _bll.DungeonZones.FirstOrDefaultAsync(id);

            if (dungeonZone == null)
            {
                return NotFound();
            }

            return _mapper.Map<App.Public.DTO.v1.DungeonZone>(dungeonZone);
        }
        
        // GET: api/DungeonZones/5
        /// <summary>
        /// Gets a list of DungeonZones that belong to specified Dungeon.
        /// </summary>
        /// <param name="dungeonId">Dungeon's id</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.DungeonZone>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [AllowAnonymous]
        [HttpGet("Dungeon/{dungeonId}")]
        public async Task<ActionResult<IEnumerable<App.Public.DTO.v1.DungeonZone>>> GetDungeonZoneByDungeon(int dungeonId)
        {
            if (!_bll.Dungeons.Exists(dungeonId))
            {
                return NotFound();
            }
            
            return new ActionResult<IEnumerable<App.Public.DTO.v1.DungeonZone>>
                ((await _bll.DungeonZones.GetAllByDungeonIdAsync(dungeonId))
                    .Select(x => _mapper.Map<App.Public.DTO.v1.DungeonZone>(x)));
        }

        // PUT: api/DungeonZones/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates DungeonZone specified by id.
        /// </summary>
        /// <param name="id">DungeonZone's id</param>
        /// <param name="dungeonZone">DungeonZone's parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.DungeonZone>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDungeonZone(int id, App.Public.DTO.v1.DungeonZone dungeonZone)
        {
            if (id != dungeonZone.Id)
            {
                return BadRequest();
            }

            if (!await _bll.Dungeons.ExistsAsync(dungeonZone.DungeonId))
            {
                return NotFound($"Dungeon with id {dungeonZone.DungeonId} was not found");
            }

            _bll.DungeonZones.Add(_mapper.Map<App.BLL.DTO.DungeonZone>(dungeonZone));

            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DungeonZoneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DungeonZones
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new DungeonZone.
        /// </summary>
        /// <param name="dungeonZone">DungeonZone's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.DungeonZone), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        public async Task<ActionResult<App.Public.DTO.v1.DungeonZone>> PostDungeonZone(App.Public.DTO.v1.DungeonZone dungeonZone)
        {
            if (!await _bll.Dungeons.ExistsAsync(dungeonZone.DungeonId))
            {
                return NotFound($"Dungeon with id {dungeonZone.DungeonId} was not found");
            }
            
            _bll.DungeonZones.Add(_mapper.Map<App.BLL.DTO.DungeonZone>(dungeonZone));
            await _bll.SaveChangesAsync();

            return CreatedAtAction("GetDungeonZone", new { id = dungeonZone.Id }, dungeonZone);
        }

        // DELETE: api/DungeonZones/5
        /// <summary>
        /// Deletes DungeonZone specified by id.
        /// </summary>
        /// <param name="id">DungeonZone's id</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDungeonZone(int id)
        {
            var dungeonZone = await _bll.DungeonZones.FirstOrDefaultAsync(id);
            if (dungeonZone == null)
            {
                return NotFound();
            }

            _bll.DungeonZones.Remove(dungeonZone);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool DungeonZoneExists(int id)
        {
            return _bll.DungeonZones.Exists(id);
        }
    }
}
