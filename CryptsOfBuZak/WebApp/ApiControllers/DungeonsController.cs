using App.Contracts.BLL;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DungeonsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public DungeonsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/Dungeons
        /// <summary>
        /// Gets a list of all Dungeons.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Dungeon>), StatusCodes.Status200OK)]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<App.Public.DTO.v1.Dungeon>> GetDungeons()
        {
            return (await _bll.Dungeons.GetAllAsync()).Select(x => _mapper.Map<App.Public.DTO.v1.Dungeon>(x));
        }

        // GET: api/Dungeons/5
        /// <summary>
        /// Gets a Dungeon specified by id.
        /// </summary>
        /// <param name="id">Dungeon's id</param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Dungeon>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<App.Public.DTO.v1.Dungeon>> GetDungeon(int id)
        {
            var dungeon = await _bll.Dungeons.FirstOrDefaultAsync(id);

            if (dungeon == null)
            {
                return NotFound();
            }

            var dungeonPublic = _mapper.Map<App.Public.DTO.v1.Dungeon>(dungeon);

            return dungeonPublic;
        }

        // PUT: api/Dungeons/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates Dungeon specified by id.
        /// </summary>
        /// <param name="id">Dungeon's id</param>
        /// <param name="dungeon">Dungeon's parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Dungeon>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDungeon(int id, App.Public.DTO.v1.Dungeon dungeon)
        {
            if (id != dungeon.Id)
            {
                return BadRequest();
            }

            var dungeonOriginal = await _bll.Dungeons.FirstOrDefaultByIdAsync(id);
            
            var dungeonBll = _mapper.Map<App.BLL.DTO.Dungeon>(dungeon);
            
            if (dungeonOriginal != null)
            {
                dungeonBll.Name = dungeonOriginal.Name;
                dungeonBll.Name[Thread.CurrentThread.CurrentUICulture.Name] = dungeon.Name;
                dungeonBll.Description = dungeonOriginal.Description;
                dungeonBll.Description[Thread.CurrentThread.CurrentUICulture.Name] = dungeon.Description;
            }


            _bll.Dungeons.Update(dungeonBll);
            
            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DungeonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Dungeons
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new Dungeon.
        /// </summary>
        /// <param name="dungeon">Dungeon's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Dungeon), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<ActionResult<App.Public.DTO.v1.Dungeon>> PostDungeon(App.Public.DTO.v1.Dungeon dungeon)
        {
            App.BLL.DTO.Dungeon dungeonBll = _mapper.Map<App.BLL.DTO.Dungeon>(dungeon);
            
            _bll.Dungeons.Add(dungeonBll);
            await _bll.SaveChangesAsync();

            return CreatedAtAction("GetDungeon", new { id = dungeon.Id }, dungeon);
        }

        // DELETE: api/Dungeons/5
        /// <summary>
        /// Deletes Dungeon specified by id.
        /// </summary>
        /// <param name="id">Dungeon's id</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDungeon(int id)
        {
            var dungeon = await _bll.Dungeons.FirstOrDefaultAsync(id);
            if (dungeon == null)
            {
                return NotFound();
            }

            _bll.Dungeons.Remove(dungeon);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
        
        private bool DungeonExists(int id)
        {
            return _bll.Dungeons.Exists(id);
        }
    }
}
