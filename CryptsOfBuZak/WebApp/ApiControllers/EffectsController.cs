#nullable disable
using App.Contracts.BLL;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EffectsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public EffectsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/Effects
        /// <summary>
        /// Gets a list of effects.
        /// </summary>
        /// <returns>List of effects</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Effect>), StatusCodes.Status200OK)]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<App.Public.DTO.v1.Effect>> GetEffects()
        {
            return (await _bll.Effects.GetAllAsync()).Select(x => _mapper.Map<App.Public.DTO.v1.Effect>(x));
        }

        // GET: api/Effects/5
        /// <summary>
        /// Gets Effect specified by id.
        /// </summary>
        /// <param name="id">Effect's id</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Effect>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<App.Public.DTO.v1.Effect>> GetEffect(int id)
        {
            var effect = await _bll.Effects.FirstOrDefaultAsync(id);

            if (effect == null)
            {
                return NotFound();
            }

            var effectPublic = _mapper.Map<App.Public.DTO.v1.Effect>(effect);
            
            return effectPublic;
        }

        // PUT: api/Effects/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates Effect specified by id.
        /// </summary>
        /// <param name="id">Effect's id</param>
        /// <param name="effect">Effect's parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Effect>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEffect(int id, App.Public.DTO.v1.Effect effect)
        {
            if (id != effect.Id)
            {
                return BadRequest();
            }

            var effectOriginal = await _bll.Effects.FirstOrDefaultAsync(id);

            var effectBll = _mapper.Map<App.BLL.DTO.Effect>(effect);
            
            if (effectOriginal != null)
            {
                effectBll.Name = effectOriginal.Name;
                effectBll.Name[Thread.CurrentThread.CurrentUICulture.Name] = effect.Name;
                effectBll.Description = effectOriginal.Description;
                effectBll.Description[Thread.CurrentThread.CurrentUICulture.Name] = effect.Description;
            }
            
            _bll.Effects.Update(effectBll);

            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EffectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Effects
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new Effect.
        /// </summary>
        /// <param name="effect">Effect's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Effect), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        public async Task<ActionResult<App.Public.DTO.v1.Effect>> PostEffect(App.Public.DTO.v1.Effect effect)
        {
            if (_bll.Effects.Exists(effect.Id))
            {
                return Conflict();
            }
            
            var effectBll = _mapper.Map<App.BLL.DTO.Effect>(effect);
            
            
            _bll.Effects.Add(effectBll);
            await _bll.SaveChangesAsync();

            return CreatedAtAction("GetEffect", new { id = effect.Id }, effect);
        }

        // DELETE: api/Effects/5
        /// <summary>
        /// Deletes Effect specified by id.
        /// </summary>
        /// <param name="id">Effect's id</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEffect(int id)
        {
            var effect = await _bll.Effects.FirstOrDefaultAsync(id);
            if (effect == null)
            {
                return NotFound();
            }

            _bll.Effects.Remove(effect);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool EffectExists(int id)
        {
            return _bll.Effects.Exists(id);
        }
    }
}
