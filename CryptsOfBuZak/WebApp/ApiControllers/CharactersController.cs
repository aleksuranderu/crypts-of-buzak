#nullable disable
using App.BLL.DTO;
using App.Contracts.BLL;
using AutoMapper;
using Base.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApp.ApiControllers
{
    /// <summary>
    /// Used for login and registration.
    /// </summary>
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CharactersController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;
        
        public CharactersController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/Characters
        /// <summary>
        /// Gets a list of all characters. 
        /// </summary>
        /// <returns>List of all characters in database.</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Character>), StatusCodes.Status200OK)]
        [HttpGet]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<App.Public.DTO.v1.Character>> GetCharacters()
        {
            return (await _bll.Characters.GetAllAsync()).Select(x => _mapper.Map<App.Public.DTO.v1.Character>(x));
        }

        // GET: api/Characters/5
        /// <summary>
        /// Gets character specified by id.
        /// </summary>
        /// <param name="id">Character's guid</param>
        /// <returns>Character with specified id</returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Character), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("User")]
        [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<App.Public.DTO.v1.Character>> GetCharacterByUser()
        {
            var character = await _bll.Characters.FirstOrDefaultAsync(User.GetUserId());

            if (character == null)
            {
                return NotFound();
            }

            var characterPublic = _mapper.Map<App.Public.DTO.v1.Character>(character);

            return characterPublic;
        }

        /// <summary>
        /// Creates a new character for signed in user
        /// </summary>
        /// <param name="name">Character's name (Max 25 characters)</param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Character), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("User/{name}")]
        [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<App.Public.DTO.v1.Character>> CreateCharacterForUser(string name)
        {
            if (_bll.Characters.Exists(User.GetUserId()))
            {
                return BadRequest("Already exists");
            }

            if (name.Length < 1 || name.Length > 25)
            {
                return BadRequest("Incorrect name");
            } 

            var character = new Character()
            {
                Id = User.GetUserId(),
                AppUserId = User.GetUserId(),
                Name = name
            };
            
            _bll.Characters.Add(character);
            await _bll.SaveChangesAsync();
            return Ok(_mapper.Map<App.Public.DTO.v1.Character>(character));
        }

        // GET: api/Characters/5
        /// <summary>
        /// Gets character of currently signed in user.
        /// </summary>
        /// <returns>Character with specified id</returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Character>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<App.Public.DTO.v1.Character>> GetCharacter(Guid id)
        {
            var character = await _bll.Characters.FirstOrDefaultAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            var characterPublic = _mapper.Map<App.Public.DTO.v1.Character>(character);

            return characterPublic;
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates character specified by id.
        /// </summary>
        /// <param name="id">Character's guid</param>
        /// <param name="character">Character's parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Character>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> PutCharacter(Guid id, App.Public.DTO.v1.Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            var characterBLL = _mapper.Map<App.BLL.DTO.Character>(character);

            _bll.Characters.Update(characterBLL);
            
            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new character with specified parameters.
        /// </summary>
        /// <param name="character">Character's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Character), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<App.Public.DTO.v1.Character>> PostCharacter(App.Public.DTO.v1.Character character)
        {
            if (CharacterExists(character.Id))
            {
                return Conflict();
            }

            App.BLL.DTO.Character characterBll = _mapper.Map<App.BLL.DTO.Character>(character);
            
            _bll.Characters.Add(characterBll);
            await _bll.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Deletes a character with specified id.
        /// </summary>
        /// <param name="id">Character's guid</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteCharacter(Guid id)
        {
            var character = await _bll.Characters.FirstOrDefaultAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _bll.Characters.Remove(character);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(Guid id)
        {
            return _bll.Characters.Exists(id);
        }
    }
}
