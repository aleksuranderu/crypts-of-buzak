#nullable disable
using App.Contracts.BLL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using App.DAL.EF;
using Base.Contracts.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class SkillsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public SkillsController(IAppBLL bll, AutoMapper.IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/Skills
        /// <summary>
        /// Gets a list of all Skills.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Skill>), StatusCodes.Status200OK)]
        [AllowAnonymous]
        [HttpGet]
        public async Task<IEnumerable<App.Public.DTO.v1.Skill>> GetSkills()
        {
            return (await _bll.Skills.GetAllAsync()).Select(x => _mapper.Map<App.Public.DTO.v1.Skill>(x));
        }

        // GET: api/Skills/5
        /// <summary>
        /// Gets a Skill specified by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Skill>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<App.Public.DTO.v1.Skill>> GetSkill(int id)
        {
            var skill = await _bll.Skills.FirstOrDefaultAsync(id);

            if (skill == null)
            {
                return NotFound();
            }

            var skillPublic = _mapper.Map<App.Public.DTO.v1.Skill>(skill);

            return skillPublic;
        }

        // PUT: api/Skills/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates Skill specified by id.
        /// </summary>
        /// <param name="id">Skill's id</param>
        /// <param name="skill">Skill's parameters</param>
        /// <returns></returns>
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Skill>), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSkill(int id, App.Public.DTO.v1.Skill skill)
        {
            if (id != skill.Id)
            {
                return BadRequest();
            }
            
            if (_bll.Skills.Exists(skill.Id))
            {
                return BadRequest("Already exists");
            }
            
            if (!await _bll.Effects.ExistsAsync(skill.EffectId))
            {
                return NotFound($"Effect with id {skill.EffectId} was not found");
            }

            var skillOriginal = await _bll.Effects.FirstOrDefaultAsync(id);

            var skillBll = _mapper.Map<App.BLL.DTO.Skill>(skill);
            
            if (skillOriginal != null)
            {
                skillBll.Name = skillOriginal.Name;
                skillBll.Name[Thread.CurrentThread.CurrentUICulture.Name] = skill.Name;
                skillBll.Description = skillOriginal.Description;
                skillBll.Description[Thread.CurrentThread.CurrentUICulture.Name] = skill.Description;
            }
            
            _bll.Skills.Update(skillBll);
            
            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SkillExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Skills
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new Skill.
        /// </summary>
        /// <param name="skill">Skill's parameters</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Skill), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        public async Task<ActionResult<App.Public.DTO.v1.Skill>> PostSkill(App.Public.DTO.v1.Skill skill)
        {
            if (_bll.Skills.Exists(skill.Id))
            {
                return BadRequest("Already exists");
            }
            
            if (!await _bll.Effects.ExistsAsync(skill.EffectId))
            {
                return NotFound($"Effect with id {skill.EffectId} was not found");
            }

            _bll.Skills.Add(_mapper.Map<App.BLL.DTO.Skill>(skill));
            await _bll.SaveChangesAsync();
            
            return CreatedAtAction("GetSkill", new { id = skill.Id }, skill);
        }

        // DELETE: api/Skills/5
        /// <summary>
        /// Deletes Skill specified by id.
        /// </summary>
        /// <param name="id">Skill's id</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSkill(int id)
        {
            var skill = await _bll.Skills.FirstOrDefaultAsync(id);
            if (skill == null)
            {
                return NotFound();
            }
            
            _bll.Skills.Remove(skill);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool SkillExists(int id)
        {
            return _bll.Skills.Exists(id);
        }
    }
}
