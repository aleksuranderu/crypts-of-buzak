#nullable disable
using App.BLL.DTO;
using App.Contracts.BLL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Base.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class FriendsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private AutoMapper.IMapper _mapper;

        public FriendsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            _mapper = mapper;
        }

        // GET: api/Friends
        /// <summary>
        /// Gets a list of friend relations.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Friend>), StatusCodes.Status200OK)]
        [HttpGet]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<App.Public.DTO.v1.Friend>> GetFriends()
        {
            return (await _bll.Friends.GetAllAsync()).Select(x => _mapper.Map<App.Public.DTO.v1.Friend>(x));
        }

        // GET: api/Friends/5
        /// <summary>
        /// Gets a list of friends for signed in user.
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Friend>), StatusCodes.Status200OK)]
        [HttpGet("User")]
        [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<App.Public.DTO.v1.Friend>> GetFriendsByUser()
        {
            
            return (await _bll.Friends.GetAllByIdAsync(User.GetUserId())).Select(x => _mapper.Map<App.Public.DTO.v1.Friend>(x));
        }
        
        // GET: api/Friends/5
        /// <summary>
        /// Gets a list of friends for user specified by id.
        /// </summary>
        /// <param name="id">User's id</param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<App.Public.DTO.v1.Friend>), StatusCodes.Status200OK)]
        [HttpGet("{id}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<App.Public.DTO.v1.Friend>> GetFriendsById(Guid id)
        {
            
            return (await _bll.Friends.GetAllByIdAsync(id)).Select(x => _mapper.Map<App.Public.DTO.v1.Friend>(x));
        }

        // POST: api/Friends
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a new relation for user character.
        /// </summary>
        /// <param name="friend">User and Friend Guids</param>
        /// <returns></returns>
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(App.Public.DTO.v1.Friend), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [HttpPost]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<App.Public.DTO.v1.Friend>> PostFriend(App.Public.DTO.v1.Friend friend)
        {
            if (FriendExists(friend.CharacterId, friend.FriendId))
            {
                return Conflict();
            }
            
            if (!await _bll.Characters.ExistsAsync(friend.CharacterId))
            {
                return NotFound($"Character with id {friend.CharacterId} was not found");
            }
            
            if (!await _bll.Characters.ExistsAsync(friend.FriendId))
            {
                return NotFound($"Friend with id {friend.FriendId} was not found");
            }
            
            _bll.Friends.Add(_mapper.Map<App.BLL.DTO.Friend>(friend));
            try
            {
                await _bll.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FriendExists(friend.CharacterId, friend.FriendId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFriendsById", new { id = friend.CharacterId }, friend);
        }

        // DELETE: api/Friends/5
        /// <summary>
        /// Deletes friend relation from specified user
        /// </summary>
        /// <param name="friend">User and Friend Guids</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        [Authorize(Roles = "admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteFriend(App.Public.DTO.v1.Friend friend)
        {
            var friendBll = await _bll.Friends.FirstOrDefaultAsync(friend.CharacterId, friend.FriendId);
            if (friendBll == null)
            {
                return NotFound();
            }

            _bll.Friends.Remove(friendBll);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        private bool FriendExists(Guid characterId, Guid friendId)
        {
            return _bll.Friends.Exists(characterId, friendId);
        }
        
        /// <summary>
        /// Adds friend in friend list.
        /// </summary>
        /// <param name="friendId">Friend guid</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPost("AddFriend/{friendId}")]
        [Authorize(Roles = "admin,user", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> AddFriend(Guid friendId)
        {
            if (!_bll.Characters.Exists(User.GetUserId()))
            {
                return NotFound("Character doesn't exist!");
            }

            if (!_bll.Characters.Exists(friendId))
            {
                return NotFound("Friend with such id doesn't exist");
            }

            var friendBll = new Friend()
            {
                CharacterId = User.GetUserId(),
                FriendId = friendId
            };

            _bll.Friends.Add(friendBll);
            
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}
