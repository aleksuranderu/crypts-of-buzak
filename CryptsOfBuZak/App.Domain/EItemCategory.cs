﻿namespace Domain;
public enum EItemCategory
{
    
    Consumable,
    
    SkillBook,
    
    Artifact,
    
    Miscellaneous
}