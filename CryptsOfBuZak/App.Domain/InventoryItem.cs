﻿using App.Domain.AbstractThings;

namespace App.Domain;

public class InventoryItem: AbstractItem
{
    public Guid CharacterId { get; set; }
    public Character? Character { get; set; }

    public bool Used { get; set; }
    // Entity for character inventory
}