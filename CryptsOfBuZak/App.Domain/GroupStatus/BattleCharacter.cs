﻿using Base.Domain;

namespace App.Domain.GroupStatus;

public class BattleCharacter: DomainEntityMetaId<Guid>
{
    public Guid GroupStatusId { get; set; }
    public GroupStatus? GroupStatus { get; set; }
    
    public string? Name { get; set; }
    public int Level { get; set; }
    public long Experience { get; set; }
    public int MaxHp { get; set; }
    public int CurrentHp { get; set; }
    public int MaxMp { get; set; }
    public int CurrentMp { get; set; }
    public int AtkStat { get; set; }
    public int DefStat { get; set; }
}