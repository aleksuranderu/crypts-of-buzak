﻿using System.ComponentModel.DataAnnotations.Schema;
using Base.Domain;

namespace App.Domain.GroupStatus;

public class InRaidEffect: DomainEntityMetaId<Guid>
{
    public Guid GroupStatusId { get; set; }
    public GroupStatus? GroupStatus { get; set; }
    public int EffectId { get; set; }  // Use Id instead of object so changes may be done during playtime.
    public Effect? Effect { get; set; }
    public int Duration { get; set; }  // remove effect if 0
}