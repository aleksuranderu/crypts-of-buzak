﻿using System.ComponentModel.DataAnnotations;
using Base.Domain;

namespace App.Domain.GroupStatus;

public class GroupStatus: DomainEntityMetaId<Guid>
{
    public Guid GroupLeaderId { get; set; }
    public Character? GroupLeader { get; set; }
    
    public Guid BattleCharacterLeaderid { get; set; }

    [MaxLength(4)]
    public ICollection<BattleCharacter>? BattleCharacters { get; set; }
    public int? DungeonZoneNumber { get; set; }
    public ICollection<InRaidSkill>? InRaidSkills { get; set; }
    public ICollection<InRaidItem>? InRaidItems { get; set; }
    public ICollection<InRaidEffect>? InRaidEffects { get; set; }
    public int DungeonId { get; set; }
    public Dungeon? Dungeon { get; set; }
    public string? GameHistory { get; set; }
}