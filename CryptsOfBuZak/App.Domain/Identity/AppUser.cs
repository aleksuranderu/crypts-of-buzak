﻿using Base.Domain.Identity;

namespace App.Domain.Identity;

public class AppUser : BaseUser<Guid>
{
    public ICollection<RefreshToken>? RefreshTokens { get; set; }
    
}