﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Base.Domain;

namespace App.Domain;

public class Skill : DomainEntityMetaId<int>
{
    [Column(TypeName = "jsonb")]
    public LangStr Name { get; set; } = new LangStr();
    [Column(TypeName = "jsonb")]
    public LangStr Description { get; set; } = new LangStr();
    public int EffectId { get; set; }
    public int Cooldown { get; set; }
    public int ManaCost { get; set; }
    public Effect? Effect { get; set; }
}