﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.Domain.GroupStatus;
using App.Domain.Identity;
using Base.Domain;
using Domain;

namespace App.Domain;

public class Character : DomainEntityMetaId<Guid>
{
    [Display(ResourceType = typeof(App.Resources.App.Domain.Character), Name = nameof(Name))]
    [MaxLength(64)] public string Name { get; set; } = default!;

    [ForeignKey("AppUser")]
    public Guid AppUserId { get; set; }
    [Display(ResourceType = typeof(App.Resources.App.Domain.Character), Name = nameof(AppUser))]
    public AppUser? AppUser { get; set; }
    
    public GroupStatus.GroupStatus? LeaderGroup { get; set; }

    public int Level { get; set; } = 1;
    public int ExperiencePoints { get; set; } = 0;
    public int HealthPoints { get; set; } = 10;
    public int ManaPoints { get; set; } = 1;
    public int AttackStat { get; set; } = 4;
    public int DefenceStat { get; set; } = 1;
    public int Money { get; set; }

    public ICollection<InventoryItem>? InventoryItems { get; set; }
    public ICollection<Friend>? FriendList { get; set; }
    public ICollection<SkillBook>? SkillBook { get; set; }
}