﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Base.Domain;

namespace App.Domain;

public class Monster : DomainEntityMetaId<int>
{
    [Display(ResourceType = typeof(App.Resources.App.Domain.Character), Name = nameof(Name))]
    [Column(TypeName = "jsonb")]
    public LangStr Name { get; set; } = new LangStr();
    [Display(ResourceType = typeof(App.Resources.App.Domain.Monster), Name = nameof(HealthPoints))]

    public int HealthPoints { get; set; }
    public int AttackStat { get; set; }
    public int DefenceStat { get; set; }
    
    public ICollection<DropTableItem>? DropTable { get; set; }
}