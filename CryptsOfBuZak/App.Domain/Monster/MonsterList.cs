﻿using Base.Domain;

namespace App.Domain;

public class MonsterList : DomainEntityMetaId<int>
{
    public int MonsterAmount { get; set; }
    public int MonsterId { get; set; }
    public int DungeonZoneId { get; set; }
    
    public Monster? Monster { get; set; }
    public DungeonZone? DungeonZone { get; set; }
}