﻿using Base.Domain;
using Domain;

namespace App.Domain;

public class DungeonZone : DomainEntityMetaId<int>
{
    public int ZoneNumber { get; set; }
    public ICollection<MonsterList>? MonsterLists { get; set; }
    public int DungeonId { get; set; }
    public Dungeon? Dungeon { get; set; }
}