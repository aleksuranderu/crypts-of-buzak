﻿using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface ISkillService : IEntityService<App.BLL.DTO.Skill, int>
{
    
}