﻿using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IMonsterService : IEntityService<App.BLL.DTO.Monster, int>
{
    
}