﻿using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IMonsterListService : IEntityService<App.BLL.DTO.MonsterList, int>
{
    
}