﻿using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IDungeonZoneService : IEntityService<App.BLL.DTO.DungeonZone, int>
{
    Task<IEnumerable<App.BLL.DTO.DungeonZone>> GetAllByDungeonIdAsync(int dungeonId, bool noTracking = true);
}