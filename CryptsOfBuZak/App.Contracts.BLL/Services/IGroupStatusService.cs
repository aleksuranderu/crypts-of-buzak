﻿using App.BLL.DTO.GroupStatus;
using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IGroupStatusService : IEntityService<App.BLL.DTO.GroupStatus.GroupStatus, Guid>
{
        Task<GroupStatus?> GetByLeaderIdAsync(Guid userid, bool noTracking = true);
        Task<GroupStatus?> GetByLeaderIdSensitive(Guid userid, bool noTracking = true);
        Task<GroupStatus?> GetByLeaderIdFightPrepared(Guid userid, bool noTracking = true);
}