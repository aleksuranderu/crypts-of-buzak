﻿using App.BLL.DTO.GroupStatus;
using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IBattleCharacterService : IEntityService<App.BLL.DTO.GroupStatus.BattleCharacter, Guid>
{
    Task<IEnumerable<BattleCharacter>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true);
}