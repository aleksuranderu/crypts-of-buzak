﻿using App.BLL.DTO;
using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IDungeonService : IEntityService<App.BLL.DTO.Dungeon, int>
{
    Task<Dungeon?> FirstOrDefaultByIdAsync(int dungeonId, bool noTracking = true);
}