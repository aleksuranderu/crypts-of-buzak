﻿using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IDropTableItemService : IEntityService<App.BLL.DTO.DropTableItem, Guid>
{
    
}