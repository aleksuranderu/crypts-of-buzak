﻿using App.BLL.DTO;
using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IFriendService : IEntityService<App.BLL.DTO.Friend, int>
{
    bool Exists(Guid characterId, Guid friendId);
    
    Task<IEnumerable<App.BLL.DTO.Friend>> GetAllByIdAsync(Guid characterId, bool noTracking = true);
    
    Task<Friend?> FirstOrDefaultAsync(Guid characterId, Guid friendId, bool noTracking = true);
}