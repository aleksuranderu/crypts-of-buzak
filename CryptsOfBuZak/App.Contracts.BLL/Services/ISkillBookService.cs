﻿using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface ISkillBookService : IEntityService<App.BLL.DTO.SkillBook, int>
{
    Task<IEnumerable<App.BLL.DTO.SkillBook>> GetAllByCharacterIdAsync(Guid characterId, bool noTracking = true);

    bool Exists(Guid characterId, int skillId);
    
    Task<App.BLL.DTO.SkillBook?> FirstOrDefaultAsync(Guid characterId, int skillId, bool noTracking = true);
}