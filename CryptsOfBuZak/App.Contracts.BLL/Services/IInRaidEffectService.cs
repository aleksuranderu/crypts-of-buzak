﻿using App.BLL.DTO.GroupStatus;
using Base.Contracts.DAL;

namespace App.Contracts.BLL.Services;

public interface IInRaidEffectService : IEntityRepository<App.BLL.DTO.GroupStatus.InRaidEffect, Guid>
{
    Task<IEnumerable<InRaidEffect>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true);
}