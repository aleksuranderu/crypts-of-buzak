﻿using App.BLL.DTO;
using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IItemService : IEntityService<App.BLL.DTO.Item, int>
{
    Task<IEnumerable<Item>> GetAllAvailableByLevel(int level, bool noTracking = true);
}