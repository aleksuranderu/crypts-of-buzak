﻿using App.BLL.DTO.GroupStatus;
using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IInRaidItemService : IEntityService<App.BLL.DTO.GroupStatus.InRaidItem, Guid>
{
    Task<IEnumerable<InRaidItem>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true);
}