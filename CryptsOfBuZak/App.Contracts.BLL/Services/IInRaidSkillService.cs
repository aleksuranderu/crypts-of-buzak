﻿using App.BLL.DTO.GroupStatus;
using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IInRaidSkillService : IEntityService<App.BLL.DTO.GroupStatus.InRaidSkill, Guid>
{
    Task<IEnumerable<InRaidSkill>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true);
}