﻿using App.BLL.DTO;
using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface IInventoryItemService : IEntityService<App.BLL.DTO.InventoryItem, int>
{
    Task<InventoryItem?> FirstOrDefaultAsync(Guid characterId, int itemId, bool noTracking = true);
    Task<IEnumerable<InventoryItem>> GetAllByCharacterIdAsync(Guid characterId, bool noTracking = true);
    bool Exists(Guid itemOwnerId, int itemId);
    Task<InventoryItem?> RemoveAsync(Guid characterId, int itemId);
}