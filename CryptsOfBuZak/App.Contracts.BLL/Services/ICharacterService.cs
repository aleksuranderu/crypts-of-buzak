﻿using App.BLL.DTO;
using App.Contracts.DAL;
using Base.Contracts.BLL;

namespace App.Contracts.BLL.Services;

public interface ICharacterService : IEntityService<App.BLL.DTO.Character, Guid>
{
    Task<Character?> FirstOrDefaultSensitiveAsync(Guid userId, bool noTracking = true);
}