﻿using Base.Contracts.BLL;
using App.Contracts.DAL;

namespace App.Contracts.BLL.Services;

public interface IEffectService : IEntityService<App.BLL.DTO.Effect, int>
{
    
}