﻿using App.BLL.DTO.GroupStatus;
using App.Contracts.BLL.Services;
using App.Contracts.BLL.Services.Identity;
using Base.Contracts.BLL;

namespace App.Contracts.BLL;

public interface IAppBLL : IBLL
{
    IAppUserService AppUsers { get; }
    IDungeonService Dungeons { get; }
    ICharacterService Characters { get; }
    IEffectService Effects { get; }
    IItemService Items { get; }
    ISkillService Skills { get; }
    IMonsterService Monsters { get; }
    IMonsterListService MonsterLists { get; }
    IInventoryItemService InventoryItems { get; }
    IInRaidItemService InRaidItems { get; }
    IInRaidSkillService InRaidSkills { get; }
    IInRaidEffectService InRaidEffects { get; }
    IGroupStatusService GroupStatuses { get; }
    IFriendService Friends { get; }
    IDungeonZoneService DungeonZones { get; }
    IDropTableItemService DropTableItems { get; }
    IBattleCharacterService BattleCharacters { get; }
    ISkillBookService SkillBooks { get; }
}