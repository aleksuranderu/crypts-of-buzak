﻿using App.BLL.Mappers;
using App.BLL.Mappers.GroupStatus;
using App.BLL.Mappers.Identity;
using App.BLL.Services;
using App.BLL.Services.Identity;
using App.Contracts.BLL;
using App.Contracts.BLL.Services;
using App.Contracts.BLL.Services.Identity;
using App.Contracts.DAL;
using App.Contracts.DAL.Identity;
using App.DAL.EF.Mappers;
using AutoMapper;
using Base.BLL;
using Base.Contracts.DAL;
using CharacterMapper = App.BLL.Mappers.CharacterMapper;
using DropTableItemMapper = App.BLL.Mappers.DropTableItemMapper;
using DungeonMapper = App.BLL.Mappers.DungeonMapper;
using DungeonZoneMapper = App.BLL.Mappers.DungeonZoneMapper;
using EffectMapper = App.BLL.Mappers.EffectMapper;
using FriendMapper = App.BLL.Mappers.FriendMapper;
using InventoryItemMapper = App.BLL.Mappers.InventoryItemMapper;
using ItemMapper = App.BLL.Mappers.ItemMapper;
using MonsterMapper = App.BLL.Mappers.MonsterMapper;
using SkillBookMapper = App.BLL.Mappers.SkillBookMapper;
using SkillMapper = App.BLL.Mappers.SkillMapper;

namespace App.BLL;

public class AppBLL : BaseBLL<IAppUnitOfWork>, IAppBLL
{
    protected IAppUnitOfWork UnitOfWork;
    private readonly AutoMapper.IMapper _mapper;
    
    public AppBLL(IAppUnitOfWork unitOfWork, IMapper mapper)
    {
        UnitOfWork = unitOfWork;
        _mapper = mapper;
    }
    
    public override async Task<int> SaveChangesAsync()
    {
        return await UnitOfWork.SaveChangesAsync();
    }

    public override int SaveChanges()
    {
        return UnitOfWork.SaveChanges();
    }

    private IAppUserService? _appUsers;

    public IAppUserService AppUsers =>
        _appUsers ??= new AppUserService(UnitOfWork.AppUsers, new AppUserMapper(_mapper));

    private IDungeonService? _dungeons;
    public IDungeonService Dungeons =>
        _dungeons ??= new DungeonService(UnitOfWork.Dungeons, new DungeonMapper(_mapper));
    
    private ICharacterService? _characters;
    public ICharacterService Characters =>
        _characters ??= new CharacterService(UnitOfWork.Characters, new CharacterMapper(_mapper));

    private IEffectService? _effects;
    public IEffectService Effects =>
        _effects ??= new EffectService(UnitOfWork.Effects, new EffectMapper(_mapper));

    private IItemService? _items;
    public IItemService Items =>
        _items ??= new ItemService(UnitOfWork.Items, new ItemMapper(_mapper));

    private ISkillService? _skills;
    public ISkillService Skills =>
        _skills ??= new SkillService(UnitOfWork.Skills, new SkillMapper(_mapper));

    private IMonsterService? _monsters;
    public IMonsterService Monsters =>
        _monsters ??= new MonsterService(UnitOfWork.Monsters, new MonsterMapper(_mapper));

    private IMonsterListService? _monsterLists;
    public IMonsterListService MonsterLists =>
        _monsterLists ??= new MonsterListService(UnitOfWork.MonsterLists, new MonsterLIstMapper(_mapper));
    
    private IInRaidItemService? _inRaidItems;
    public IInRaidItemService InRaidItems =>
        _inRaidItems ??= new InRaidItemService(UnitOfWork.InRaidItems, new InRaidItemMapper(_mapper));

    private IInRaidSkillService? _inRaidSkills;
    public IInRaidSkillService InRaidSkills =>
        _inRaidSkills ??= new InRaidSkillService(UnitOfWork.InRaidSkills, new InRaidSkillMapper(_mapper));
    
    private IInRaidEffectService? _inRaidEffects;
    public IInRaidEffectService InRaidEffects =>
        _inRaidEffects ??= new InRaidEffectService(UnitOfWork.InRaidEffects, new InRaidEffectMapper(_mapper));

    private IGroupStatusService? _groupStatuses;
    public IGroupStatusService GroupStatuses =>
        _groupStatuses ??= new GroupStatusService(UnitOfWork.GroupStatuses, new GroupStatusMapper(_mapper));

    private IFriendService? _friends;
    public IFriendService Friends =>
        _friends ??= new FriendService(UnitOfWork.Friends, new FriendMapper(_mapper));

    private IDungeonZoneService? _dungeonZones;
    public IDungeonZoneService DungeonZones =>
        _dungeonZones ??= new DungeonZoneService(UnitOfWork.DungeonZones, new DungeonZoneMapper(_mapper));

    private IDropTableItemService? _dropTableItems;
    public IDropTableItemService DropTableItems =>
        _dropTableItems ??= new DropTableItemService(UnitOfWork.DropTableItems, new DropTableItemMapper(_mapper));

    private IBattleCharacterService? _battleCharacters;
    public IBattleCharacterService BattleCharacters =>
        _battleCharacters ??=
            new BattleCharacterService(UnitOfWork.BattleCharacters, new BattleCharacterMapper(_mapper));

    private IInventoryItemService? _inventoryItems;
    public IInventoryItemService InventoryItems =>
        _inventoryItems ??= new InventoryItemService(UnitOfWork.InventoryItems, new InventoryItemMapper(_mapper));

    private ISkillBookService? _skillBooks;
    public ISkillBookService SkillBooks =>
        _skillBooks ??= new SkillBookService(UnitOfWork.SkillBooks, new SkillBookMapper(_mapper));
}