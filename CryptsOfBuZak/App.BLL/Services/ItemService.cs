﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class ItemService : BaseEntityService<App.BLL.DTO.Item, App.DAL.DTO.Item, IItemRepository, int>, IItemService
{
    public ItemService(IItemRepository repository, IMapper<App.BLL.DTO.Item, App.DAL.DTO.Item> mapper) : base(repository, mapper)
    {
    }

    public async Task<IEnumerable<Item>> GetAllAvailableByLevel(int level, bool noTracking = true)
    {
        return (await Repository.GetAllAvailableByLevel(level, noTracking)).Select(x => Mapper.Map(x)!);
    }
}