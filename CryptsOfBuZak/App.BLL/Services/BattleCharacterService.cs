﻿using App.BLL.DTO.GroupStatus;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class BattleCharacterService : BaseEntityService<App.BLL.DTO.GroupStatus.BattleCharacter,App.DAL.DTO.GroupStatus.BattleCharacter,IBattleCharacterRepository, Guid>, IBattleCharacterService
{
    public BattleCharacterService(IBattleCharacterRepository repository, IMapper<App.BLL.DTO.GroupStatus.BattleCharacter, App.DAL.DTO.GroupStatus.BattleCharacter> mapper) : base(repository, mapper)
    {
    }
    
    public async Task<IEnumerable<BattleCharacter>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true)
    {
        return (await Repository.GetAllByGroupStatusIdAsync(groupStatusId, noTracking)).Select(x => Mapper.Map(x)!);
    }
}