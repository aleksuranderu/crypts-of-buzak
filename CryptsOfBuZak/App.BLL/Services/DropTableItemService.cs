﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class DropTableItemService : BaseEntityService<App.BLL.DTO.DropTableItem, App.DAL.DTO.DropTableItem, IDropTableItemRepository, Guid>, IDropTableItemService
{
    public DropTableItemService(IDropTableItemRepository repository, IMapper<App.BLL.DTO.DropTableItem, App.DAL.DTO.DropTableItem> mapper) : base(repository, mapper)
    {
    }
}