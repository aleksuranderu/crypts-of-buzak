﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class MonsterService : BaseEntityService<App.BLL.DTO.Monster, App.DAL.DTO.Monster, IMonsterRepository, int>, IMonsterService
{
    public MonsterService(IMonsterRepository repository, IMapper<App.BLL.DTO.Monster, App.DAL.DTO.Monster> mapper) : base(repository, mapper)
    {
    }
}