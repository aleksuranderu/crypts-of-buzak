﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class EffectService : BaseEntityService<App.BLL.DTO.Effect, App.DAL.DTO.Effect, IEffectRepository, int>, IEffectService
{
    public EffectService(IEffectRepository repository, IMapper<App.BLL.DTO.Effect, App.DAL.DTO.Effect> mapper) : base(repository, mapper)
    {
    }
}