﻿using App.BLL.DTO.GroupStatus;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class InRaidItemService : BaseEntityService<App.BLL.DTO.GroupStatus.InRaidItem, App.DAL.DTO.GroupStatus.InRaidItem, IInRaidItemRepository, Guid>, IInRaidItemService
{
    public InRaidItemService(IInRaidItemRepository repository, IMapper<App.BLL.DTO.GroupStatus.InRaidItem, App.DAL.DTO.GroupStatus.InRaidItem> mapper) : base(repository, mapper)
    {
    }
    
    public async Task<IEnumerable<InRaidItem>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true)
    {
        return (await Repository.GetAllByGroupStatusIdAsync(groupStatusId, noTracking)).Select(x => Mapper.Map(x)!);
    }
}