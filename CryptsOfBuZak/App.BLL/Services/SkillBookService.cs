﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;
using Base.DAL.EF;
using SkillBook = App.DAL.DTO.SkillBook;

namespace App.BLL.Services;

public class SkillBookService : BaseEntityService<App.BLL.DTO.SkillBook, App.DAL.DTO.SkillBook, ISkillBookRepository, int>, ISkillBookService
{
    public SkillBookService(ISkillBookRepository repository, IMapper<App.BLL.DTO.SkillBook, App.DAL.DTO.SkillBook> mapper) : base(repository, mapper)
    {
    }

    public async Task<IEnumerable<App.BLL.DTO.SkillBook>> GetAllByCharacterIdAsync(Guid characterId, bool noTracking = true)
    {
        return (await Repository.GetAllByCharacterIdAsync(characterId, noTracking)).Select(x => Mapper.Map(x)!);
    }

    public bool Exists(Guid characterId, int skillId)
    {
        return Repository.Exists(characterId, skillId);
    }

    public async Task<App.BLL.DTO.SkillBook?> FirstOrDefaultAsync(Guid characterId, int skillId, bool noTracking = true)
    {
        return Mapper.Map(await Repository.FirstOrDefaultAsync(characterId, skillId, noTracking));
    }
}