﻿using App.BLL.DTO.GroupStatus;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class InRaidEffectService : BaseEntityService<App.BLL.DTO.GroupStatus.InRaidEffect, App.DAL.DTO.GroupStatus.InRaidEffect, IInRaidEffectRepository, Guid>, IInRaidEffectService
{
    public InRaidEffectService(IInRaidEffectRepository repository, IMapper<App.BLL.DTO.GroupStatus.InRaidEffect, App.DAL.DTO.GroupStatus.InRaidEffect> mapper) : base(repository, mapper)
    {
        
    }
    
    public async Task<IEnumerable<InRaidEffect>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true)
    {
        return (await Repository.GetAllByGroupStatusIdAsync(groupStatusId, noTracking)).Select(x => Mapper.Map(x)!);
    }
    
    public InRaidEffect Update(InRaidEffect effect)
    {
        if (effect.Duration <= 0)
        {
            return Mapper.Map(Repository.Remove(Mapper.Map(effect)!))!;
        }
        
        return Mapper.Map(Repository.Update(Mapper.Map(effect)!))!;
    }
}