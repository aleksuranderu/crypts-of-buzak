﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class FriendService : BaseEntityService<App.BLL.DTO.Friend, App.DAL.DTO.Friend, IFriendRepository, int>, IFriendService
{
    public FriendService(IFriendRepository repository, IMapper<App.BLL.DTO.Friend, App.DAL.DTO.Friend> mapper) : base(repository, mapper)
    {
    }

    public bool Exists(Guid characterId, Guid friendId)
    {
        return Repository.Exists(characterId, friendId);
    }

    public async Task<IEnumerable<App.BLL.DTO.Friend>> GetAllByIdAsync(Guid characterId, bool noTracking = true)
    {
        return (await Repository.GetAllByIdAsync(characterId, noTracking)).Select(x => Mapper.Map(x)!);
    }

    public async Task<App.BLL.DTO.Friend?> FirstOrDefaultAsync(Guid characterId, Guid friendId, bool noTracking = true)
    {
        return Mapper.Map(await Repository.FirstOrDefaultAsync(characterId, friendId, noTracking));
    }
}