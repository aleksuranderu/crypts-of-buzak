﻿using App.BLL.DTO.GroupStatus;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class GroupStatusService : BaseEntityService<App.BLL.DTO.GroupStatus.GroupStatus, App.DAL.DTO.GroupStatus.GroupStatus, IGroupStatusRepository, Guid>, IGroupStatusService
{
    public GroupStatusService(IGroupStatusRepository repository, IMapper<App.BLL.DTO.GroupStatus.GroupStatus, App.DAL.DTO.GroupStatus.GroupStatus> mapper) : base(repository, mapper)
    {
    }

    public async Task<GroupStatus?> GetByLeaderIdAsync(Guid userid, bool noTracking = true)
    {
        return Mapper.Map(await Repository.GetByLeaderIdAsync(userid, noTracking));
    }
    
    public async Task<GroupStatus?> GetByLeaderIdSensitive(Guid userid, bool noTracking = true)
    {
        return Mapper.Map(await Repository.GetByLeaderIdSensitive(userid, noTracking));
    }

    public async Task<GroupStatus?> GetByLeaderIdFightPrepared(Guid userid, bool noTracking = true)
    {
        return Mapper.Map(await Repository.GetByLeaderIdFightPrepared(userid, noTracking));
    }
}