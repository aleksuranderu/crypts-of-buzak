﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using App.DAL.EF.Repositories;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class DungeonService : BaseEntityService<App.BLL.DTO.Dungeon, App.DAL.DTO.Dungeon, IDungeonRepository, int>, IDungeonService
{
    public DungeonService(IDungeonRepository repository, IMapper<BLL.DTO.Dungeon, DAL.DTO.Dungeon> mapper) : base(repository, mapper)
    {
    }
    
    public async Task<Dungeon?> FirstOrDefaultByIdAsync(int dungeonId, bool noTracking = true)
    {
        return Mapper.Map(await Repository.FirstOrDefaultByIdAsync(dungeonId));
    }
}