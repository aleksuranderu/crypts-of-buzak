﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class DungeonZoneService : BaseEntityService<App.BLL.DTO.DungeonZone, App.DAL.DTO.DungeonZone, IDungeonZoneRepository, int>, IDungeonZoneService
{
    public DungeonZoneService(IDungeonZoneRepository repository, IMapper<App.BLL.DTO.DungeonZone, App.DAL.DTO.DungeonZone> mapper) : base(repository, mapper)
    {
    }

    public async Task<IEnumerable<App.BLL.DTO.DungeonZone>> GetAllByDungeonIdAsync(int dungeonId, bool noTracking = true)
    {
        return (await Repository.GetAllByDungeonIdAsync(dungeonId)).Select(x => Mapper.Map(x)!);
    }
}