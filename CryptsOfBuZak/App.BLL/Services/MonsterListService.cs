﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;
using Base.DAL;

namespace App.BLL.Services;

public class MonsterListService : BaseEntityService<App.BLL.DTO.MonsterList, App.DAL.DTO.MonsterList, IMonsterListRepository, int>, IMonsterListService
{
    public MonsterListService(IMonsterListRepository repository, IMapper<App.BLL.DTO.MonsterList, App.DAL.DTO.MonsterList> mapper) : base(repository, mapper)
    {
    }
}