﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class SkillService : BaseEntityService<App.BLL.DTO.Skill, App.DAL.DTO.Skill, ISkillRepository, int>, ISkillService
{
    public SkillService(ISkillRepository repository, IMapper<App.BLL.DTO.Skill, App.DAL.DTO.Skill> mapper) : base(repository, mapper)
    {
    }
}