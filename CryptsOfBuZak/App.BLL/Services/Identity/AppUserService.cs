﻿using App.Contracts.BLL.Services;
using App.Contracts.BLL.Services.Identity;
using App.Contracts.DAL;
using App.Contracts.DAL.Identity;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services.Identity;

public class AppUserService : BaseEntityService<App.BLL.DTO.Identity.AppUser,App.DAL.DTO.Identity.AppUser,IAppUserRepository, Guid>, IAppUserService
{
    public AppUserService(IAppUserRepository repository, IMapper<App.BLL.DTO.Identity.AppUser, App.DAL.DTO.Identity.AppUser> mapper) : base(repository, mapper)
    {
    }
}