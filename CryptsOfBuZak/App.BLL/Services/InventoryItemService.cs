﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class InventoryItemService : BaseEntityService<App.BLL.DTO.InventoryItem, App.DAL.DTO.InventoryItem, IInventoryItemRepository, int>, IInventoryItemService
{
    public InventoryItemService(IInventoryItemRepository repository, IMapper<App.BLL.DTO.InventoryItem, App.DAL.DTO.InventoryItem> mapper) : base(repository, mapper)
    {
    }

    public async Task<InventoryItem?> FirstOrDefaultAsync(Guid characterId, int itemId, bool noTracking = true)
    {
        return Mapper.Map(await Repository.FirstOrDefaultAsync(characterId, itemId, noTracking));
    }

    public async Task<IEnumerable<InventoryItem>> GetAllByCharacterIdAsync(Guid characterId, bool noTracking = true)
    {
        return (await Repository.GetAllByCharacterIdAsync(characterId, noTracking)).Select(x => Mapper.Map(x)!);
    }

    // Check if item(InventoryItem) belongs to specific owner(Character).
    public bool Exists(Guid itemOwnerId, int itemId)
    {
        return Repository.Exists(itemOwnerId, itemId);
    }

    public async Task<InventoryItem?> RemoveAsync(Guid characterId, int itemId)
    {
        return Mapper.Map(await Repository.RemoveAsync(characterId, itemId));
    }

    public InventoryItem Update(InventoryItem inventoryItem)
    {
        if (!Exists(inventoryItem.CharacterId, inventoryItem.ItemId))
        {
            return Mapper.Map(Repository.Add(Mapper.Map(inventoryItem)!))!;
        }
        if (inventoryItem.Amount <= 0)
        {
            return Mapper.Map(Repository.Remove(Mapper.Map(inventoryItem)!))!;
        }
        
        return Mapper.Map(Repository.Update(Mapper.Map(inventoryItem)!))!;
    }
}