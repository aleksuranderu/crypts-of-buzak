﻿using App.BLL.DTO.GroupStatus;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class InRaidSkillService : BaseEntityService<App.BLL.DTO.GroupStatus.InRaidSkill, App.DAL.DTO.GroupStatus.InRaidSkill, IInRaidSkillRepository, Guid>, IInRaidSkillService
{
    public InRaidSkillService(IInRaidSkillRepository repository, IMapper<App.BLL.DTO.GroupStatus.InRaidSkill, App.DAL.DTO.GroupStatus.InRaidSkill> mapper) : base(repository, mapper)
    {
    }
    
    public async Task<IEnumerable<InRaidSkill>> GetAllByGroupStatusIdAsync(Guid groupStatusId, bool noTracking = true)
    {
        return (await Repository.GetAllByGroupStatusIdAsync(groupStatusId, noTracking)).Select(x => Mapper.Map(x)!);
    }
    
    public InRaidSkill Update(InRaidSkill skill)
    {
        if (skill.CurrentCooldown <= 0)
        {
            return Mapper.Map(Repository.Remove(Mapper.Map(skill)!))!;
        }
        
        return Mapper.Map(Repository.Update(Mapper.Map(skill)!))!;
    }
}