﻿using App.BLL.DTO;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using Base.BLL;
using Base.Contracts.Base;

namespace App.BLL.Services;

public class CharacterService : BaseEntityService<App.BLL.DTO.Character, App.DAL.DTO.Character, ICharacterRepository, Guid>, ICharacterService
{
    public CharacterService(ICharacterRepository repository, IMapper<Character, DAL.DTO.Character> mapper) : base(repository, mapper)
    {
    }

    public async Task<Character?> FirstOrDefaultSensitiveAsync(Guid userId, bool noTracking = true)
    {
        return Mapper.Map(await Repository.FirstOrDefaultSensitiveAsync(userId, noTracking));
    }
}