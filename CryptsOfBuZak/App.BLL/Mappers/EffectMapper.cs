﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class EffectMapper : BaseMapper<App.BLL.DTO.Effect, App.DAL.DTO.Effect>
{
    public EffectMapper(IMapper mapper) : base(mapper)
    {
    }
}