﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class CharacterMapper : BaseMapper<App.BLL.DTO.Character, App.DAL.DTO.Character>
{
    public CharacterMapper(IMapper mapper) : base(mapper)
    {
    }
}