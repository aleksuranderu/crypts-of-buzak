﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class FriendMapper : BaseMapper<App.BLL.DTO.Friend, App.DAL.DTO.Friend>
{
    public FriendMapper(IMapper mapper) : base(mapper)
    {
    }
}