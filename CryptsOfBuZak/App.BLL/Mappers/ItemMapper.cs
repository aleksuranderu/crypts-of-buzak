﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class ItemMapper : BaseMapper<App.BLL.DTO.Item, App.DAL.DTO.Item>
{
    public ItemMapper(IMapper mapper) : base(mapper)
    {
    }
}