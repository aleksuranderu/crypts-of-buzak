﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class MonsterMapper : BaseMapper<App.BLL.DTO.Monster, App.DAL.DTO.Monster>
{
    public MonsterMapper(IMapper mapper) : base(mapper)
    {
    }
}