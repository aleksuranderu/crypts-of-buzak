﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class DungeonZoneMapper : BaseMapper<App.BLL.DTO.DungeonZone, App.DAL.DTO.DungeonZone>
{
    public DungeonZoneMapper(IMapper mapper) : base(mapper)
    {
    }
}