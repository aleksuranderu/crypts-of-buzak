﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class DropTableItemMapper : BaseMapper<App.BLL.DTO.DropTableItem, App.DAL.DTO.DropTableItem>
{
    public DropTableItemMapper(IMapper mapper) : base(mapper)
    {
    }
}