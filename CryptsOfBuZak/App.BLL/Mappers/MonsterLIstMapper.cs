﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class MonsterLIstMapper : BaseMapper<App.BLL.DTO.MonsterList, App.DAL.DTO.MonsterList>
{
    public MonsterLIstMapper(IMapper mapper) : base(mapper)
    {
    }
}