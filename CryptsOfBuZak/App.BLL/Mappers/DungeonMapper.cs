﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class DungeonMapper : BaseMapper<App.BLL.DTO.Dungeon, App.DAL.DTO.Dungeon>
{
    public DungeonMapper(IMapper mapper) : base(mapper)
    {
    }
}