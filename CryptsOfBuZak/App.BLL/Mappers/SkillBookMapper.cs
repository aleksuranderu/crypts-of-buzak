﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class SkillBookMapper: BaseMapper<App.BLL.DTO.SkillBook, App.DAL.DTO.SkillBook>
{
    public SkillBookMapper(IMapper mapper) : base(mapper)
    {
    }
}