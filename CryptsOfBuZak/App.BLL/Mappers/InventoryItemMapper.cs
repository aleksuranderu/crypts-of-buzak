﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class InventoryItemMapper : BaseMapper<App.BLL.DTO.InventoryItem, App.DAL.DTO.InventoryItem>
{
    public InventoryItemMapper(IMapper mapper) : base(mapper)
    {
    }
}