﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers;

public class SkillMapper : BaseMapper<App.BLL.DTO.Skill, App.DAL.DTO.Skill>
{
    public SkillMapper(IMapper mapper) : base(mapper)
    {
    }
}