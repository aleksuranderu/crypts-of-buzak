﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers.GroupStatus;

public class GroupStatusMapper : BaseMapper<App.BLL.DTO.GroupStatus.GroupStatus, App.DAL.DTO.GroupStatus.GroupStatus>
{
    public GroupStatusMapper(IMapper mapper) : base(mapper)
    {
    }
}