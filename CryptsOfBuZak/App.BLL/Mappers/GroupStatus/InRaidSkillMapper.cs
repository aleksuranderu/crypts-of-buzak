﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers.GroupStatus;

public class InRaidSkillMapper : BaseMapper<App.BLL.DTO.GroupStatus.InRaidSkill, App.DAL.DTO.GroupStatus.InRaidSkill>
{
    public InRaidSkillMapper(IMapper mapper) : base(mapper)
    {
    }
}