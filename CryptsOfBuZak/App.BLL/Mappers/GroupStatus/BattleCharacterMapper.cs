﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers.GroupStatus;

public class BattleCharacterMapper : BaseMapper<App.BLL.DTO.GroupStatus.BattleCharacter, App.DAL.DTO.GroupStatus.BattleCharacter>
{
    public BattleCharacterMapper(IMapper mapper) : base(mapper)
    {
    }
}