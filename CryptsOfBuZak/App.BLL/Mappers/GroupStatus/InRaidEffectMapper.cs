﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers.GroupStatus;

public class InRaidEffectMapper : BaseMapper<App.BLL.DTO.GroupStatus.InRaidEffect, App.DAL.DTO.GroupStatus.InRaidEffect>
{
    public InRaidEffectMapper(IMapper mapper) : base(mapper)
    {
    }
}