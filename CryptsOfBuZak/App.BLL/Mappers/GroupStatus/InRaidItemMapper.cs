﻿using AutoMapper;
using Base.DAL;

namespace App.BLL.Mappers.GroupStatus;

public class InRaidItemMapper : BaseMapper<App.BLL.DTO.GroupStatus.InRaidItem, App.DAL.DTO.GroupStatus.InRaidItem>
{
    public InRaidItemMapper(IMapper mapper) : base(mapper)
    {
    }
}