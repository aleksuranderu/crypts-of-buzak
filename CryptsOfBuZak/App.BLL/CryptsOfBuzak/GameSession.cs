﻿using System.Diagnostics.CodeAnalysis;
using App.BLL.DTO;
using App.BLL.DTO.GroupStatus;

namespace App.BLL.CryptsOfBuzak;

public class GameSession
{
    private List<BattleCharacter> _battleCharacters = new List<BattleCharacter>();
    private List<BattleCharacter> _battleMonsters = new List<BattleCharacter>();
    private List<Effect> _effects = new List<Effect>();
    private List<InRaidItem> _inRaidItems = new List<InRaidItem>();
    private GroupStatus _groupStatus; 

    public GameSession(GroupStatus groupStatus)
    {
        _groupStatus = groupStatus;
        _groupStatus.GameHistory = "";
        
        _battleCharacters.AddRange(
            groupStatus.BattleCharacters ?? throw new InvalidOperationException("No Battle Characters?"));
        
        var monsterLists = groupStatus
            .Dungeon?
            .DungeonZones?
            .FirstOrDefault(x => x.ZoneNumber == groupStatus.DungeonZoneNumber)?
            .MonsterLists?
            .ToList();
            
        // Convert Monster entities to BattleCharacter Monsters
        _battleMonsters.AddRange(SpawnMonsters(
                monsterLists
                , groupStatus.Dungeon!.AverageLevel));

        // Apply All effects
        if (groupStatus.InRaidEffects != null)
        {
            _effects.AddRange(groupStatus.InRaidEffects.Select(e => e.Effect).ToList()!);
            ApplyEffectsToCharacters();
        }

        if (groupStatus.InRaidItems != null)
        {
            _inRaidItems.AddRange(groupStatus.InRaidItems.ToList());
        }

        // Generate reward for zone clear.
        _inRaidItems = GenerateDrops(monsterLists!);
    }
    
    public GroupStatus GetGroupStatus()
    {
        GroupStatus groupStatus = _groupStatus;

        groupStatus.BattleCharacters = _battleCharacters;
        groupStatus.InRaidItems = _inRaidItems;
        groupStatus.GameHistory = _groupStatus.GameHistory;

        return _groupStatus;
    }
    
    // Fight until Monsters or Characters loose. After Game over, initialize loot drop process.
    public void Fight()
    {
        bool gameOver = (_battleMonsters.Count == 0 || _battleCharacters.All(x => x.CurrentHp <= 0));
        // Skip fight if there are no monsters.
        // Skip fight if all battleCharacters are dead.

        int turnNumber = 0;
        int fightMaxLength = 150;
        
        while (!gameOver && turnNumber < fightMaxLength)
        {
            var random = new Random();
            
            foreach (var vCharacter in _battleCharacters)
            {
                if (vCharacter.CurrentHp <= 0 || _battleMonsters.Count == 0)
                {
                    continue;
                }
                
                // Choose who to attack.

                int index;
                if (_battleMonsters.Count > 1)
                {
                    index = random.Next(maxValue:_battleMonsters.Count, minValue: 0);
                }
                else
                {
                    index = 0;
                }

                BattleCharacter vMonster = _battleMonsters[index];
                // Attack
                Attack(vCharacter, vMonster);
                
                if (vMonster.CurrentHp <= 0)
                {
                    Kill(vMonster);

                    // Give each character in group experience for slain monster.
                    foreach (var vChar in _battleCharacters)
                    {
                        int xp = ExperienceFromMonster(vChar, vMonster);
                        GiveExperienceForCharacter(vChar, xp);
                    }

                    _battleMonsters.Remove(vMonster);
                    
                    if (_battleMonsters.All(m => m.CurrentHp < 0) || _battleMonsters.Count == 0)
                    {
                        gameOver = true;
                    }
                }
                else
                {
                    // If monster still alive, it will attack back!
                    Attack(vMonster, vCharacter);
                }
                
                if (vCharacter.CurrentHp <= 0)
                {
                    Kill(vCharacter);
                    
                    if (_battleCharacters.All( c => c.CurrentHp <= 0))
                    {
                        gameOver = true;
                    }
                }
            }
            turnNumber++;
        }

        if (turnNumber >= fightMaxLength)
        {
            _groupStatus.GameHistory += "After too long fight, Warlock Ka'zub found out where you are" +
                                        " and sent an earthquake that collapsed the entire dungeon!\n";
            
            foreach (var battleCharacter in _battleCharacters)
            {
                Kill(battleCharacter);
            }
        }
        RemoveEffectsFromCharacters();
    }

    //  Roll items based on list of MonsterList objects, then return dropped items as list of InRaidItem.
    private List<InRaidItem> GenerateDrops(List<MonsterList> listOfMonsterList)
    {
        List<InRaidItem> localDrops = _inRaidItems;

        var random = new Random();
        
        foreach (var vMonsterList in listOfMonsterList)
        {
            if (vMonsterList.Monster != null)
            {
                if (vMonsterList.Monster.DropTable != null)
                {
                    foreach (var dropTableItem in vMonsterList.Monster.DropTable.ToList())
                    {
                        InRaidItem iri = RollInRaidItemFromDropList(dropTableItem, vMonsterList.MonsterAmount);

                        if (iri.Amount != 0)
                        {
                            InRaidItem? dropsInRaidItem = localDrops.FirstOrDefault(x => x.ItemId == iri.ItemId);

                            if (dropsInRaidItem != null)
                            {
                                dropsInRaidItem.Amount += iri.Amount;
                            }
                            else
                            {
                                localDrops.Add(iri);
                            }
                        }
                    }
                }
            }
        }
        return localDrops;
    }

    private InRaidItem RollInRaidItemFromDropList(DropTableItem dti, int rolls)
    {
        InRaidItem iri = new InRaidItem();

        Random random = new Random();

        iri.GroupStatusId = _groupStatus.Id;
        iri.ItemId = dti.ItemId;
        iri.Item = dti.Item;
        
        //Make an extra roll for each monster.
        for (int i = 0; i < rolls; i++)
        {
            if (dti.DropChance > random.NextDouble())
            {
                iri.Amount += random.Next(minValue: dti.DropMinimumAmount, maxValue: dti.DropMaximumAmount);
            }
        }

        return iri;
    }

    // Record death of character
    private void Kill(BattleCharacter battleCharacter)
    {
        battleCharacter.CurrentHp = 0;
        _groupStatus.GameHistory += $"{battleCharacter.Name} died!\n";
    }
    
    // battleCharacterA attacks battleCharacterB, decreasing HP of battleCharacterB, record this attack in GroupStatus.GameHistory.
    [SuppressMessage("ReSharper.DPA", "DPA0003: Excessive memory allocations in LOH", MessageId = "type: System.String")]
    private void Attack(BattleCharacter battleCharacterA, BattleCharacter battleCharacterB)
    {
        if (battleCharacterA.CurrentHp <= 0)
        {
            // Already dead.
            return;
        }

        float damage;
        
        if (battleCharacterB.DefStat != -105)
        {
            damage = (float) (battleCharacterA.AtkStat * (105.0 / (105.0 + battleCharacterB.DefStat)));
        }
        else
        {
            damage = (float) ( battleCharacterA.AtkStat * (210.0 / (210.0 + (battleCharacterB.DefStat - 1.0) * 2.0)));
        }
        
        
        _groupStatus.GameHistory += 
            $"{battleCharacterA.Name}({battleCharacterA.CurrentHp}/{battleCharacterA.MaxHp})" +
            " attacks " +
            $"{battleCharacterB.Name}({battleCharacterB.CurrentHp}/{battleCharacterB.MaxHp})" +
            $", and deals {(int) damage} damage!\n";
        
        
        battleCharacterB.CurrentHp -= (int) damage;
    }
    
    // Return List of BattleCharacter Monsters objects, Which are made from MonsterList objects from list.
    private List<BattleCharacter> SpawnMonsters(List<MonsterList>? listOfMonsterList, int level)
    {
        List<BattleCharacter> battleMonsters = new List<BattleCharacter>();
        if (listOfMonsterList != null)
        {
            foreach (var monsterList in listOfMonsterList)
            {
                try
                {
                    for (int i = 0; i < monsterList.MonsterAmount; i++)
                    {
                        BattleCharacter battleMonster = MonsterToBattleCharacter(monsterList.Monster, level);
                        battleMonsters.Add(battleMonster);
                    }
                }
                catch (Exception e)
                {
                    //Skip inappropriate entities.
                }
            }
        }
        return battleMonsters;
    }

    // Convert Monster to BattleCharacter object.
    private BattleCharacter MonsterToBattleCharacter(Monster? monster, int level)
    {
        BattleCharacter bc = new BattleCharacter();
        if (monster != null)
        {
            bc.Id = Guid.NewGuid();
            bc.Name = monster.Name;
            bc.Level = level;
            bc.MaxHp = monster.HealthPoints + level;
            bc.CurrentHp = bc.MaxHp;
            bc.AtkStat = monster.AttackStat + level;
            bc.DefStat = monster.DefenceStat + level;
        }
        else
        {
            throw new Exception("Can't convert null!");
        }
        return bc;
    }

    // Calculate experience points gained from defeated monster. 
    private int ExperienceFromMonster(BattleCharacter battleCharacter, BattleCharacter monster)
    {
        // DIF = MonLevel / CharLevel
        // (ATK + DEF) * (DIF + 1)

        float dif = (float)monster.Level / battleCharacter.Level;
        return (int) ((monster.AtkStat + monster.DefStat) * (dif + 0.5));
    }

    // Add experience to character and check for level up.
    // Recursively calls itself if leveled up to check for multiple level ups.
    private void GiveExperienceForCharacter(BattleCharacter battleCharacter, int xp)
    {
        while (true)
        {
            int xpMax = battleCharacter.Level * 100;
            battleCharacter.Experience += xp;

            if (battleCharacter.Experience >= xpMax)
            {
                LevelUpCharacter(battleCharacter);
                battleCharacter.Experience -= xpMax;
                xp -= xpMax;
                if (xp > 0)
                {
                    continue;
                }
            }
            break;
        }
    }

    private void LevelUpCharacter(BattleCharacter battleCharacter)
    {
        battleCharacter.Level += 1;
        battleCharacter.MaxHp += 5; 
        battleCharacter.CurrentHp = battleCharacter.MaxHp; // Level up replenish HP
        battleCharacter.MaxMp += 1;
        battleCharacter.CurrentMp = battleCharacter.MaxMp; // Level up replenish MP
        battleCharacter.AtkStat += 1;
        battleCharacter.DefStat += 1;
    }

    private void ApplyEffectsToCharacters()
    {
        foreach (var effect in _effects)
        {
            foreach (var vCharacter in _battleCharacters)
            {
                vCharacter.CurrentHp += effect.HpModifier;
                vCharacter.CurrentMp += effect.MpModifier;
                vCharacter.AtkStat += effect.AtkModifier;
                vCharacter.DefStat += effect.DefModifier;
            }
        }
        
    }

    private void RemoveEffectsFromCharacters()
    {
        foreach (var effect in _effects)
        {
            foreach (var vCharacter in _battleCharacters)
            {
                if (vCharacter.CurrentHp > vCharacter.MaxHp)
                {
                    vCharacter.CurrentHp = vCharacter.MaxHp;
                }
                if (vCharacter.CurrentMp > vCharacter.MaxMp)
                {
                    vCharacter.CurrentMp = vCharacter.MaxMp;
                }
                vCharacter.AtkStat -= effect.AtkModifier;
                vCharacter.DefStat -= effect.DefModifier;
            }
        }
    }
}