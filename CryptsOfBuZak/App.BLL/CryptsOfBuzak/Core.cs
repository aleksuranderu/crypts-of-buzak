﻿using App.BLL.DTO;
using App.BLL.DTO.GroupStatus;
using App.Contracts.BLL;

namespace App.BLL.CryptsOfBuzak;

public class Core
{
    // Create GroupStatus
    public static GroupStatus CreateGroupStatus(Character character)
    {
        GroupStatus groupStatus = new GroupStatus();
        
        // Character can have only one concurrent GroupStatus, so this is ok. 
        groupStatus.Id = character.Id;
        groupStatus.GroupLeaderId = character.Id;
        groupStatus.DungeonZoneNumber = null;

        return groupStatus;
    }

    public static InventoryItem CreateInventoryItem(Guid characterId, int itemId, int amount)
    {
        InventoryItem inventoryItem = new InventoryItem();
        inventoryItem.CharacterId = characterId;
        inventoryItem.ItemId = itemId;
        inventoryItem.Amount = amount;
        return inventoryItem;
    }
    
    public static SkillBook CreateSkillBook(Guid characterId, int skillId)
    {
        SkillBook skillBook = new SkillBook();
        skillBook.SkillId = skillId;
        skillBook.CharacterId = characterId;

        return skillBook;
    }
    
    public static InRaidEffect CreateInRaidEffect(Guid groupStatusId, Effect effect)
    {
        InRaidEffect inRaidEffect = new InRaidEffect();
        inRaidEffect.Duration = effect.Duration;
        inRaidEffect.EffectId = effect.Id;
        inRaidEffect.GroupStatusId = groupStatusId;

        return inRaidEffect;
    }
    
    public static InRaidSkill CreateInRaidSkill(Guid groupStatusId, Skill skill)
    {
        InRaidSkill inRaidSkill = new InRaidSkill();
        inRaidSkill.SkillId = skill.Id;
        inRaidSkill.GroupStatusId = groupStatusId;
        inRaidSkill.CurrentCooldown = skill.Cooldown;

        return inRaidSkill;
    }
    
    // Create BattleCharacter, according to its stats.
    public static BattleCharacter CreateBattleCharacter(Character character)
    {
        BattleCharacter battleCharacter = new BattleCharacter();

        battleCharacter.Id = Guid.NewGuid();
        battleCharacter.Name = character.Name;
        battleCharacter.Experience = character.ExperiencePoints;
        battleCharacter.Level = character.Level;
        battleCharacter.AtkStat = character.AttackStat;
        battleCharacter.DefStat = character.DefenceStat;
        battleCharacter.MaxHp = character.HealthPoints;
        battleCharacter.CurrentHp = battleCharacter.MaxHp;
        battleCharacter.MaxMp = character.ManaPoints;
        battleCharacter.CurrentMp = battleCharacter.MaxMp;

        return battleCharacter;
    }

    public static Character BattleCharacterToCharacterStats(BattleCharacter battleCharacter, Character character)
    {
        Character localCharacter = character;
        localCharacter.Level = battleCharacter.Level;
        localCharacter.ExperiencePoints = (int) battleCharacter.Experience;
        localCharacter.HealthPoints = battleCharacter.MaxHp;
        localCharacter.ManaPoints = battleCharacter.MaxMp;
        localCharacter.AttackStat = battleCharacter.AtkStat;
        localCharacter.DefenceStat = battleCharacter.DefStat;

        return localCharacter;
    }
}