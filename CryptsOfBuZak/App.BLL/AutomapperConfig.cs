﻿using AutoMapper;

namespace App.BLL;

public class AutomapperConfig : Profile
{
    public AutomapperConfig()
    {
        CreateMap<App.BLL.DTO.Character, App.DAL.DTO.Character>().ReverseMap();
        CreateMap<App.BLL.DTO.Dungeon, App.DAL.DTO.Dungeon>().ReverseMap();
        CreateMap<App.BLL.DTO.Effect, App.DAL.DTO.Effect>().ReverseMap();
        CreateMap<App.BLL.DTO.Item, App.DAL.DTO.Item>().ReverseMap();
        CreateMap<App.BLL.DTO.Identity.AppUser, App.DAL.DTO.Identity.AppUser>().ReverseMap();
        CreateMap<App.BLL.DTO.Skill, App.DAL.DTO.Skill>().ReverseMap();
        CreateMap<App.BLL.DTO.Monster, App.DAL.DTO.Monster>().ReverseMap();
        CreateMap<App.BLL.DTO.MonsterList, App.DAL.DTO.MonsterList>().ReverseMap();
        CreateMap<App.BLL.DTO.InventoryItem, App.DAL.DTO.InventoryItem>().ReverseMap();
        CreateMap<App.BLL.DTO.Friend, App.DAL.DTO.Friend>().ReverseMap();
        CreateMap<App.BLL.DTO.DungeonZone, App.DAL.DTO.DungeonZone>().ReverseMap();
        CreateMap<App.BLL.DTO.DropTableItem, App.DAL.DTO.DropTableItem>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.BattleCharacter, App.DAL.DTO.GroupStatus.BattleCharacter>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.GroupStatus, App.DAL.DTO.GroupStatus.GroupStatus>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.InRaidItem, App.DAL.DTO.GroupStatus.InRaidItem>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.InRaidSkill, App.DAL.DTO.GroupStatus.InRaidSkill>().ReverseMap();
        CreateMap<App.BLL.DTO.GroupStatus.InRaidEffect, App.DAL.DTO.GroupStatus.InRaidEffect>().ReverseMap();
        CreateMap<App.BLL.DTO.SkillBook, App.DAL.DTO.SkillBook>().ReverseMap();
    }
}