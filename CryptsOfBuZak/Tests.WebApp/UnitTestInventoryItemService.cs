using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using App.BLL;
using App.BLL.DTO;
using App.BLL.Mappers;
using App.BLL.Services;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using AutoMapper;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests.WebApp;

public class UnitTestInventoryItemService
{
    private readonly ITestOutputHelper _testOutputHelper;

    private readonly IInventoryItemService _inventoryItemService;
    
    private readonly Mock<IInventoryItemRepository> _inventoryItemRepo;
    private readonly IMapper _mapper;

    public UnitTestInventoryItemService(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
        _inventoryItemRepo = new Mock<IInventoryItemRepository>();

        _mapper = new Mapper(new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<AutomapperConfig>();
        }));

        _inventoryItemService = new InventoryItemService(_inventoryItemRepo.Object, new InventoryItemMapper(_mapper));
    }
    
    [Fact]
    public void TestInvItemServiceGetAllByCharacter()
    {
        // Arrange
        Guid characterId = Guid.NewGuid();
        
        _inventoryItemRepo.Setup(x => x.GetAllByCharacterIdAsync(characterId, true)).ReturnsAsync(new List<App.DAL.DTO.InventoryItem>
        {
            new App.DAL.DTO.InventoryItem
            {
                Id = 1,
                ItemId = 1,
                Item = null,
                Amount = 1,
                CharacterId = characterId,
                Used = true
            },
            
            new App.DAL.DTO.InventoryItem
            {
                Id = 2,
                ItemId = 2,
                Item = null,
                Amount = 3,
                CharacterId = characterId,
                Used = true
            }
        });
        
        // Act
        var result = _inventoryItemService.GetAllByCharacterIdAsync(characterId).Result.ToList();

        foreach (var res in result)
        {
            _testOutputHelper.WriteLine(JsonSerializer.Serialize(res.ToString()));
        }
        
        // Assert
        Assert.Equal(2, result.Count);
        Assert.All(result, x => Assert.Equal(characterId, x.CharacterId));
    }
    
    [Fact]
    public void TestInvItemServiceFirstOrDefaultAsync()
    {
        // Arrange
        Guid characterId = Guid.NewGuid();

        _inventoryItemRepo
            .Setup(x => x.FirstOrDefaultAsync(characterId, 1, true))
            .ReturnsAsync(new App.DAL.DTO.InventoryItem
            {
                Id = 1,
                ItemId = 1,
                Amount = 4,
                CharacterId = characterId,
                Used = false
            });
        
        // Act
        var result = _inventoryItemService.FirstOrDefaultAsync(characterId, 1).Result;

        // Assert
        Assert.NotNull(result);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result!.ToString()));

        Assert.Equal(characterId, result.CharacterId);
        Assert.Equal(1, result.ItemId);
    }
    
    [Fact]
    public void TestInvItemServiceExists()
    {
        // Arrange
        Guid characterId = Guid.NewGuid();

        _inventoryItemRepo
            .Setup(x => x.Exists(characterId, 1))
            .Returns(true);
        
        // Act
        var result = _inventoryItemService.Exists(characterId, 1);

        // Assert
        Assert.True(result);
    }
    
    [Fact]
    public void TestInvItemServiceRemoveAsync()
    {
        // Arrange
        Guid characterId = Guid.NewGuid();

        _inventoryItemRepo
            .Setup(x => x.RemoveAsync(characterId, 3))
            .ReturnsAsync(
                new App.DAL.DTO.InventoryItem
                {
                    Id = 1,
                    ItemId = 3,
                    Amount = 2,
                    CharacterId = characterId,
                    Used = false
                }
            );
        
        // Act
        var result = _inventoryItemService.RemoveAsync(characterId, 3).Result;
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        Assert.NotNull(result);
        Assert.Equal(characterId, result!.CharacterId);
        Assert.Equal(3, result!.ItemId);
    }
    
    [Fact]
    public void TestInvItemServiceUpdateHasAmount()
    {
        // Arrange
        Guid characterId = Guid.NewGuid();
        var itemBLL = new InventoryItem
        {
            Id = 1,
            ItemId = 1,
            Amount = 2,
            CharacterId = characterId,
            Used = true
        };

        var itemDAL = new App.DAL.DTO.InventoryItem
        {
            Id = 1,
            ItemId = 1,
            Amount = 2,
            CharacterId = characterId,
            Used = true
        };

        _inventoryItemRepo
            .Setup(x => x.Update(
                It.Is<App.DAL.DTO.InventoryItem>(
                    item => item.CharacterId == characterId 
                            && item.ItemId == 1
                            && item.Amount == 2
                ))
            )
            .Returns(itemDAL);
        _inventoryItemRepo
            .Setup(x => x.Exists(characterId, 1))
            .Returns(true);
        
        // Act
        var result = _inventoryItemService.Update(itemBLL);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        Assert.Equal(characterId, result.CharacterId);
        Assert.Equal(1, result.ItemId);
        Assert.Equal(2, result.Amount);
    }
    
    [Fact]
    public void TestInvItemServiceUpdateNoAmount()
    {
        // Arrange
        Guid characterId = Guid.NewGuid();
        var itemBLL = new InventoryItem
        {
            Id = 1,
            ItemId = 1,
            Amount = 0,
            CharacterId = characterId,
            Used = true
        };

        var itemDAL = new App.DAL.DTO.InventoryItem
        {
            Id = 1,
            ItemId = 1,
            Amount = 0,
            CharacterId = characterId,
            Used = true
        };
        
        _inventoryItemRepo
            .Setup(x => x.Remove(
                It.Is<App.DAL.DTO.InventoryItem>(
                    item => item.CharacterId == characterId 
                            && item.ItemId == 1
                            && item.Amount == 0
                ))
            )
            .Returns(itemDAL);
        _inventoryItemRepo
            .Setup(x => x.Exists(characterId, 1))
            .Returns(true);
        
        // Act
        var result = _inventoryItemService.Update(itemBLL);

        // Assert
        Assert.Equal(characterId, result.CharacterId);
        Assert.Equal(1, result.ItemId);
        Assert.Equal(0, result.Amount);
    }
    
    [Fact]
    public void TestInvItemServiceUpdateNewItem()
    {
        // Arrange
        Guid characterId = Guid.NewGuid();
        var itemBLL = new InventoryItem
        {
            Id = 1,
            ItemId = 1,
            Amount = 2,
            CharacterId = characterId,
            Used = true
        };

        var itemDAL = new App.DAL.DTO.InventoryItem
        {
            Id = 1,
            ItemId = 1,
            Amount = 2,
            CharacterId = characterId,
            Used = true
        };
        
        _inventoryItemRepo
            .Setup(x => x.Add(
                It.Is<App.DAL.DTO.InventoryItem>(
                    item => item.CharacterId == characterId 
                            && item.ItemId == 1
                            && item.Amount == 2
                ))
            )
            .Returns(itemDAL);
        _inventoryItemRepo
            .Setup(x => x.Exists(characterId, 1))
            .Returns(false);
        
        // Act
        var result = _inventoryItemService.Update(itemBLL);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        Assert.Equal(characterId, result.CharacterId);
        Assert.Equal(1, result.ItemId);
        Assert.Equal(2, result.Amount);
    }
}