﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using App.BLL;
using App.BLL.DTO;
using App.BLL.Mappers;
using App.BLL.Services;
using App.Contracts.BLL.Services;
using App.Contracts.DAL;
using AutoMapper;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests.WebApp;

public class UnitTestBaseEntityService
{
    private readonly ITestOutputHelper _testOutputHelper;

    private readonly IEffectService _effectService;
    
    private readonly Mock<IEffectRepository> _effectRepo;
    
    private readonly IMapper _mapper;

    public UnitTestBaseEntityService(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
        
        // In this case, we'll use Effect as an example of BaseEntity object.
        _effectRepo = new Mock<IEffectRepository>();

        _mapper = new Mapper(new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<AutomapperConfig>();
        }));

        _effectService = new EffectService(_effectRepo.Object, new EffectMapper(_mapper));
    }
    
    [Fact]
    public void TestBaseEntityServiceAdd()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();
        int randomDuration = random.Next();

        var effectBLL = new Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };

        var effectDAL = new App.DAL.DTO.Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };
        
        _effectRepo
            .Setup(x => x.Add(
            It.Is<App.DAL.DTO.Effect>(effect => 
                effect.Id == effectDAL.Id && 
                effect.Duration == effectDAL.Duration &&
                effect.HpModifier == effectDAL.HpModifier))
            ).Returns(effectDAL);

        // act
        var result = _effectService.Add(effectBLL);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        
        Assert.Equal(effectId, result.Id);
        Assert.Equal(randomDuration, result.Duration);
        Assert.Equal(1, result.HpModifier);
    }
    
    [Fact]
    public void TestBaseEntityServiceUpdate()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();
        int randomDuration = random.Next();

        var effectBLL = new Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };

        var effectDAL = new App.DAL.DTO.Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };
        
        _effectRepo
            .Setup(x => x.Update(
                It.Is<App.DAL.DTO.Effect>(effect => 
                    effect.Id == effectDAL.Id && 
                    effect.Duration == effectDAL.Duration &&
                    effect.HpModifier == effectDAL.HpModifier))
            ).Returns(effectDAL);

        // act
        var result = _effectService.Update(effectBLL);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        
        Assert.Equal(effectId, result.Id);
        Assert.Equal(randomDuration, result.Duration);
        Assert.Equal(1, result.HpModifier);
    }
    
    [Fact]
    public void TestBaseEntityServiceRemoveById()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();
        int randomDuration = random.Next();

        var effectDAL = new App.DAL.DTO.Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };
        
        _effectRepo
            .Setup(x => x.Remove(effectId)).Returns(effectDAL);

        // act
        var result = _effectService.Remove(effectId);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        
        Assert.Equal(effectId, result.Id);
        Assert.Equal(randomDuration, result.Duration);
        Assert.Equal(1, result.HpModifier);
    }
    
    [Fact]
    public void TestBaseEntityServiceRemoveAsync()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();
        int randomDuration = random.Next();

        var effectDAL = new App.DAL.DTO.Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };
        
        _effectRepo
            .Setup(x => x.RemoveAsync(effectId)).ReturnsAsync(effectDAL);

        // act
        var result = _effectService.RemoveAsync(effectId).Result;
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        
        Assert.Equal(effectId, result.Id);
        Assert.Equal(randomDuration, result.Duration);
        Assert.Equal(1, result.HpModifier);
    }
    
    [Fact]
    public void TestBaseEntityServiceRemoveByEntity()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();
        int randomDuration = random.Next();

        var effectBLL = new Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };

        var effectDAL = new App.DAL.DTO.Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };
        
        _effectRepo
            .Setup(x => x.Remove(
                It.Is<App.DAL.DTO.Effect>(effect => 
                    effect.Id == effectDAL.Id && 
                    effect.Duration == effectDAL.Duration &&
                    effect.HpModifier == effectDAL.HpModifier))
            ).Returns(effectDAL);

        // act
        var result = _effectService.Remove(effectBLL);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        
        Assert.Equal(effectId, result.Id);
        Assert.Equal(randomDuration, result.Duration);
        Assert.Equal(1, result.HpModifier);
    }
    
    [Fact]
    public void TestBaseEntityServiceFirstOrDefault()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();
        int randomDuration = random.Next();

        var effectDAL = new App.DAL.DTO.Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };
        
        _effectRepo
            .Setup(x => x.FirstOrDefault(
                effectId, true)).Returns(effectDAL);

        // act
        var result = _effectService.FirstOrDefault(effectId);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        Assert.Equal(effectId, result!.Id);
        Assert.Equal(randomDuration, result.Duration);
        Assert.Equal(1, result.HpModifier);
    }
    
    [Fact]
    public void TestBaseEntityServiceFirstOrDefaulReturnsNull()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();

        _effectRepo
            .Setup(x => x.FirstOrDefault(
                effectId, true)).Returns((App.DAL.DTO.Effect?) null);

        // act
        var result = _effectService.FirstOrDefault(effectId);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        Assert.Null(result);
    }
    
    
    [Fact]
    public void TestBaseEntityServiceGetFirstOrDefaultAsync()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();
        int randomDuration = random.Next();

        var effectDAL = new App.DAL.DTO.Effect()
        {
            Id = effectId,
            Duration = randomDuration,
            HpModifier = 1
        };
        
        _effectRepo
            .Setup(x => x.FirstOrDefaultAsync(
                effectId, true)).ReturnsAsync(effectDAL);

        // act
        var result = _effectService.FirstOrDefaultAsync(effectId).Result;
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        Assert.Equal(effectId, result!.Id);
        Assert.Equal(randomDuration, result.Duration);
        Assert.Equal(1, result.HpModifier);
    }
    
    [Fact]
    public void TestBaseEntityServiceGetFirstOrDefaultAsyncReturnsNull()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();

        _effectRepo
            .Setup(x => x.FirstOrDefaultAsync(
                effectId, true)).ReturnsAsync((App.DAL.DTO.Effect?) null);

        // act
        var result = _effectService.FirstOrDefaultAsync(effectId).Result;
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        Assert.Null(result);

    }
    
    [Fact]
    public void TestBaseEntityServiceGetAll()
    {
        //Arrange
        Random random = new Random();
        int effectId1 = random.Next();
        int effectId2 = random.Next();
        int randomDuration1 = random.Next();
        int randomDuration2 = random.Next();

        _effectRepo
            .Setup(x => x.GetAll(true)).Returns(new List<App.DAL.DTO.Effect>
            {
                new App.DAL.DTO.Effect()
                {
                    Id = effectId1,
                    Duration = randomDuration1,
                    HpModifier = 1
                },
                new App.DAL.DTO.Effect()
                {
                    Id = effectId2,
                    Duration = randomDuration2,
                    HpModifier = 1
                }
            });

        // act
        var result = _effectService.GetAll().ToList();

        foreach (var res in result)
        {
            _testOutputHelper.WriteLine(JsonSerializer.Serialize(res));
        }

        // Assert
        Assert.Equal(2, result.Count);
        Assert.Contains(result, x => x.Id == effectId1);
        Assert.Contains(result, x => x.Id == effectId2);
        Assert.Contains(result, x => x.Duration == randomDuration1);
        Assert.Contains(result, x => x.Duration == randomDuration2);
    }   
    
    [Fact]
    public void TestBaseEntityServiceGetAllAsync()
    {
        //Arrange
        Random random = new Random();
        int effectId1 = random.Next();
        int effectId2 = random.Next();
        int randomDuration1 = random.Next();
        int randomDuration2 = random.Next();

        _effectRepo
            .Setup(x => x.GetAllAsync(true)).ReturnsAsync(new List<App.DAL.DTO.Effect>
            {
                new App.DAL.DTO.Effect()
                {
                    Id = effectId1,
                    Duration = randomDuration1,
                    HpModifier = 1
                },
                new App.DAL.DTO.Effect()
                {
                    Id = effectId2,
                    Duration = randomDuration2,
                    HpModifier = 1
                }
            });

        // act
        var result = _effectService.GetAllAsync().Result.ToList();

        foreach (var res in result)
        {
            _testOutputHelper.WriteLine(JsonSerializer.Serialize(res));
        }

        // Assert
        Assert.Equal(2, result.Count);
        Assert.Contains(result, x => x.Id == effectId1);
        Assert.Contains(result, x => x.Id == effectId2);
        Assert.Contains(result, x => x.Duration == randomDuration1);
        Assert.Contains(result, x => x.Duration == randomDuration2);
    }   
    
    [Fact]
    public void TestBaseEntityServiceExists()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();
        int randomDuration = random.Next();

        _effectRepo
            .Setup(x => x.Exists(effectId)).Returns(true);

        // act
        var result = _effectService.Exists(effectId);
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        
        Assert.True(result);
    }
    
    [Fact]
    public void TestBaseEntityServiceExistsAsync()
    {
        //Arrange
        Random random = new Random();
        int effectId = random.Next();
        int randomDuration = random.Next();

        _effectRepo
            .Setup(x => x.ExistsAsync(effectId)).ReturnsAsync(true);

        // act
        var result = _effectService.ExistsAsync(effectId).Result;
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(result));

        // Assert
        
        Assert.True(result);
    }
}