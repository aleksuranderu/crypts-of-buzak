﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using App.DAL.EF;
using App.Domain;
using App.Domain.Identity;
using Domain;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Tests.WebApp;

public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup>
    where TStartup : class
{
    private static bool dbInitialized = false;

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureServices(services =>
        {
            // find DbContext
            var descriptor = services.SingleOrDefault(
                d => d.ServiceType ==
                     typeof(DbContextOptions<ApplicationDbContext>));

            // if found - remove
            if (descriptor != null)
            {
                services.Remove(descriptor);
            }

            // and new DbContext
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDbForTesting");
            });

            // data seeding
            // create db and seed data
            var sp = services.BuildServiceProvider();
            using var scope = sp.CreateScope();
            var scopedServices = scope.ServiceProvider;
            var db = scopedServices.GetRequiredService<ApplicationDbContext>();
            var logger = scopedServices
                .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

            db.Database.EnsureCreated();

            try
            {
                if (dbInitialized == false)
                {
                    dbInitialized = true;
                    SeedIdentity(scope);
                    SeedData(db);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An error occurred seeding the " +
                                    "database with test messages. Error: {Message}", ex.Message);
            }
        });
    }

    private void SeedIdentity(IServiceScope serviceScope)
    {
        using var userManager = serviceScope.ServiceProvider.GetService<UserManager<AppUser>>();
        using var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<AppRole>>();

        if (userManager == null || roleManager == null)
        {
            throw new NullReferenceException("userManager or roleManager cannot be null!");
        }

        var roles = new string[]
        {
            "admin",
            "user",
        };

        foreach (var roleInfo in roles)
        {
            var role = roleManager.FindByNameAsync(roleInfo).Result;
            if (role == null)
            {
                var identityResult = roleManager.CreateAsync(new AppRole()
                {
                    Name = roleInfo
                }).Result;
                if (!identityResult.Succeeded)
                {
                    throw new ApplicationException("Role creation failed");
                }
            }
        }
    }

    private void SeedData(ApplicationDbContext db)
    {
        db.Effects.Add(new Effect
            {
                Id = 1,
                Name ="Test Heal",
                Description = "Heals characters by 5 HP.",
                Duration = 1,
                HpModifier = 5,
                MpModifier = 0,
                AtkModifier = 0,
                DefModifier = 0,
            });
        db.Skills.Add(new Skill
            {
                Id = 1,
                Name = "Test Skill",
                Description = "Test Skill",
                EffectId = 1,
                Cooldown = 2,
                ManaCost = 1,
            });
        db.Items.Add(new Item
            {
                Id = 1,
                Name = "Healing Powder (green)",
                Description = "Green powder that has healing properties.",
                Category = EItemCategory.Consumable,
                EffectId = 1,
                Price = 20,
                MinLevelToBuy = 1
            });
        db.Monsters.Add(new Monster
            {
                Id = 1,
                Name = "Monster",
                HealthPoints = 2,
                AttackStat = 1,
                DefenceStat = 1,
            });
        db.Dungeons.Add(new Dungeon
            {
                Id = 1,
                Name = "First Dungeon",
                Description = "Your first dungeon. Time to kill those innocent rats",
                AverageLevel = 1
            });
        // db.SaveChanges();

        db.DungeonZones.Add(new DungeonZone
        {
            Id = 1,
            DungeonId = 1,
            ZoneNumber = 1
        });
        db.MonsterLists.Add(new MonsterList
        {
            MonsterId = 1,
            MonsterAmount = 2,
            DungeonZoneId = 1
        });

        db.SaveChanges();
    }
}
