﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AngleSharp.Html.Dom;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Tests.WebApp.Helpers;
using Xunit;
using Xunit.Abstractions;

namespace Tests.WebApp.Controllers;

public class IntegrationTestFlow : IClassFixture<CustomWebApplicationFactory<Program>>
{
    private readonly HttpClient _client;
    private readonly CustomWebApplicationFactory<Program> _factory;
    private readonly ITestOutputHelper _testOutputHelper;

    public IntegrationTestFlow(CustomWebApplicationFactory<Program> factory,
        ITestOutputHelper testOutputHelper)
    {
        _factory = factory;
        _testOutputHelper = testOutputHelper;
        _client = _factory.CreateClient(
            new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false,
                HandleCookies = false,
            }
        );
    }
    
    [Fact]
    public async Task TestHappyFlowMvc()
    {
        var registerResponse = await RegisterUserAsync();
        
        var identityCookie = registerResponse
            .Headers
            .FirstOrDefault(h => h.Key == "Set-Cookie")
            .Value
            .First();

        var url = "/User/Game";
        
        var characterPageResponse = await CreateCharacterAsync(url, identityCookie);
        var redirectUrl = characterPageResponse.Headers.Location?.OriginalString;
        Assert.NotNull(redirectUrl);
        Assert.NotEmpty(redirectUrl);

        
        var dungeonListResponse = await ChooseDungeonAsync(url, identityCookie);

        var startDungeonResponse = await StartDungeonAsync(url, identityCookie);
        redirectUrl = startDungeonResponse.Headers.Location?.OriginalString;
        Assert.NotNull(redirectUrl);
        Assert.NotEmpty(redirectUrl);

        var nextDungeonZoneResponse = await AdvanceInDungeonAsync(url, identityCookie);
        redirectUrl = nextDungeonZoneResponse.Headers.Location?.OriginalString;
        Assert.NotNull(redirectUrl);
        Assert.NotEmpty(redirectUrl);
        Assert.Equal("/User/Game/Camp", redirectUrl);
        
        var endDungeonResponse = await AdvanceInDungeonAsync(url, identityCookie);
        redirectUrl = endDungeonResponse.Headers.Location?.OriginalString;
        Assert.NotNull(redirectUrl);
        Assert.NotEmpty(redirectUrl);
        Assert.Equal("/User/Game", redirectUrl);
    }
    
    private async Task<HttpResponseMessage> RegisterUserAsync()
    {
        // register new user via web

        // ARRANGE
        const string registerUri = "/Identity/Account/Register";
        // ACT
        // this just gets the headers, body can be xxx length and is loaded later
        var getRegisterResponse = await _client.GetAsync(registerUri);
        // ASSERT
        getRegisterResponse.EnsureSuccessStatusCode();


        // ARRANGE
        // get the actual content from response
        var getRegisterContent = await HtmlHelpers.GetDocumentAsync(getRegisterResponse);


        // get the form element from page content
        var formRegister = (IHtmlFormElement) getRegisterContent.QuerySelector("#registerForm")!;

        // set up the form values - username, pwd, etc
        var formRegisterValues = new Dictionary<string, string>
        {
            ["Input.Email"] = "test@itcollege.ee",
            ["Input.Password"] = "Foo.bar1",
            ["Input.ConfirmPassword"] = "Foo.bar1",
            ["__RequestVerificationToken"] =
                ((IHtmlInputElement) formRegister.Elements.First(e =>
                    (e as IHtmlInputElement)?.Name == "__RequestVerificationToken")).DefaultValue
        };

        // get the antiforgery cookie
        var cookie = getRegisterResponse.Headers.FirstOrDefault(h => h.Key == "Set-Cookie").Value.First();
        
        // ACT
        // send form with data to server, method (POST) is detected from form element
        var postRegisterResponse = await _client.SendAsync(formRegister, formRegisterValues, cookie);

        // ASSERT
        // found - 302 - ie user was created and we should redirect
        // https://en.wikipedia.org/wiki/HTTP_302
        Assert.Equal(HttpStatusCode.Found, postRegisterResponse.StatusCode);

        return postRegisterResponse;

    }
    
    private async Task<HttpResponseMessage> CreateCharacterAsync(string url,string cookie)
    {
        // create new character via web

        // ARRANGE
        var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);  
        requestMessage.Headers.Add("Cookie", cookie);
        var getCreateFormResponse = await _client.SendAsync(requestMessage);

        // ASSERT
        // getCreateFormResponse.EnsureSuccessStatusCode();
        Assert.Equal(StatusCodes.Status302Found, (int)getCreateFormResponse.StatusCode);

        
        
        requestMessage = new HttpRequestMessage(HttpMethod.Get, url + "/CreateCharacter");
        requestMessage.Headers.Add("Cookie", cookie);
        getCreateFormResponse = await _client.SendAsync(requestMessage);
        
        var getCreateContent = await HtmlHelpers.GetDocumentAsync(getCreateFormResponse);
        
        // get the form element from page content
        // first form on page is culture
        // second form on page is logout
        var formCreate = getCreateContent.Forms[2];
        // set up the form values
        var formCreateValues = new Dictionary<string, string>
        {
            ["Name"] = "TEST-C",
            ["__RequestVerificationToken"] =
                ((IHtmlInputElement) formCreate.Elements.First(e =>
                    (e as IHtmlInputElement)?.Name == "__RequestVerificationToken")).DefaultValue
        };

        // get the antiforgery cookie
        var antiforgeryCookie = getCreateFormResponse.Headers.FirstOrDefault(h => h.Key == "Set-Cookie").Value.First();
        cookie = cookie + ";" + antiforgeryCookie;
        
        // ACT
        // send form with data to server, method (POST) is detected from form element
        var postRegisterResponse = await _client.SendAsync(formCreate, formCreateValues, cookie);
        // ASSERT
        Assert.Equal(HttpStatusCode.Found, postRegisterResponse.StatusCode);

        return postRegisterResponse;
    }

    private async Task<HttpResponseMessage> ChooseDungeonAsync(string url, string cookie)
    {
        var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);
        requestMessage.Headers.Add("Cookie", cookie);
        var getPageResponse = await _client.SendAsync(requestMessage);

        // ASSERT
        getPageResponse.EnsureSuccessStatusCode();
        
        var getCreateContent = await HtmlHelpers.GetDocumentAsync(getPageResponse);
        
        // get the first dungeon form element from page content
        // first form on page is culture
        // second form on page is logout
        var formDungeon = getCreateContent.Forms[2];
        // set up the form values
        var formCreateValues = new Dictionary<string, string>
        {
            ["__RequestVerificationToken"] =
                ((IHtmlInputElement) formDungeon.Elements.First(e =>
                    (e as IHtmlInputElement)?.Name == "__RequestVerificationToken")).DefaultValue
        };

        // get the antiforgery cookie
        var antiforgeryCookie = getPageResponse.Headers.FirstOrDefault(h => h.Key == "Set-Cookie").Value.First();
        cookie = cookie + ";" + antiforgeryCookie;
        
        // ACT
        // send form with data to server, method (POST) is detected from form element
        var postDungeonResponse = await _client.SendAsync(formDungeon, formCreateValues, cookie);
        // ASSERT
        postDungeonResponse.EnsureSuccessStatusCode();

        return postDungeonResponse;
    }

    private async Task<HttpResponseMessage> StartDungeonAsync(string url, string cookie)
    {
        var requestMessage = new HttpRequestMessage(HttpMethod.Get, url + "/FormParty/1");
        requestMessage.Headers.Add("Cookie", cookie);
        var getPageResponse = await _client.SendAsync(requestMessage);

        // ASSERT
        getPageResponse.EnsureSuccessStatusCode();
        
        var getCreateContent = await HtmlHelpers.GetDocumentAsync(getPageResponse);
        
        // get the start dungeon form element from page content
        // first form on page is culture
        // second form on page is logout
        var formStart = getCreateContent.Forms[2];
        // set up the form values
        var formCreateValues = new Dictionary<string, string>
        {
            ["__RequestVerificationToken"] =
                ((IHtmlInputElement) formStart.Elements.First(e =>
                    (e as IHtmlInputElement)?.Name == "__RequestVerificationToken")).DefaultValue
        };
        
        // get the antiforgery cookie
        var antiforgeryCookie = getPageResponse.Headers.FirstOrDefault(h => h.Key == "Set-Cookie").Value.First();
        cookie = cookie + ";" + antiforgeryCookie;
        
        // ACT
        // send form with data to server, method (POST) is detected from form element
        var postStartResponse = await _client.SendAsync(formStart, formCreateValues, cookie);
        // ASSERT
        Assert.Equal(HttpStatusCode.Found, postStartResponse.StatusCode);

        return postStartResponse;
    }

    private async Task<HttpResponseMessage> AdvanceInDungeonAsync(string url, string cookie)
    {
        var requestMessage = new HttpRequestMessage(HttpMethod.Get, url + "/Camp");
        requestMessage.Headers.Add("Cookie", cookie);
        var getPageResponse = await _client.SendAsync(requestMessage);

        // ASSERT
        getPageResponse.EnsureSuccessStatusCode();
        
        var getPageContent = await HtmlHelpers.GetDocumentAsync(getPageResponse);
        
        // get the start dungeon form element from page content
        // first form on page is culture
        // second form on page is logout
        var formNext = getPageContent.Forms[2];
        // set up the form values
        var formCreateValues = new Dictionary<string, string>
        {
            ["__RequestVerificationToken"] =
                ((IHtmlInputElement) formNext.Elements.First(e =>
                    (e as IHtmlInputElement)?.Name == "__RequestVerificationToken")).DefaultValue
        };
        
        // get the antiforgery cookie
        var antiforgeryCookie = getPageResponse.Headers.FirstOrDefault(h => h.Key == "Set-Cookie").Value.First();
        cookie = cookie + ";" + antiforgeryCookie;
        
        // ACT
        // send form with data to server, method (POST) is detected from form element
        var postStartResponse = await _client.SendAsync(formNext, formCreateValues, cookie);
        // ASSERT
        Assert.Equal(HttpStatusCode.Found, postStartResponse.StatusCode);

        return postStartResponse;
    }
    
    
}