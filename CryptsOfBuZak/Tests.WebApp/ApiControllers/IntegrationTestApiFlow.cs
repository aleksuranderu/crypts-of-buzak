﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using App.Public.DTO.v1.Identity;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;
using Xunit.Abstractions;

namespace Tests.WebApp.ApiControllers;

public class IntegrationTestApiFlow : IClassFixture<CustomWebApplicationFactory<Program>>
{
    private readonly HttpClient _client;
    private readonly CustomWebApplicationFactory<Program> _factory;
    private readonly ITestOutputHelper _testOutputHelper;
    private readonly JsonSerializerOptions _jsonOptions;

    public IntegrationTestApiFlow(CustomWebApplicationFactory<Program> factory,
        ITestOutputHelper testOutputHelper)
    {
        _factory = factory;
        _testOutputHelper = testOutputHelper;
        _client = _factory.CreateClient(
            new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false,
                HandleCookies = false,
            }
        );
        _jsonOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };
    }

    [Fact]
    public async Task TestApiHappyFlow()
    {
        var registerResponse = await ApiRegisterAsync();
        
        var apiRequest = new HttpRequestMessage();
        apiRequest.Method = HttpMethod.Get;
        apiRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        apiRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", registerResponse.Token);
        
        await GetApiDungeonListAsync(apiRequest);

        apiRequest = new HttpRequestMessage();
        apiRequest.Method = HttpMethod.Get;
        apiRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        apiRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", registerResponse.Token);

        await ApiCreateCharacterAsync(apiRequest);
        
        apiRequest = new HttpRequestMessage();
        apiRequest.Method = HttpMethod.Post;
        apiRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        apiRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", registerResponse.Token);
        
        await ApiCreatePartyAsync(apiRequest);
        
        for (int i = 0; i < 2; i++)
        {
            apiRequest = new HttpRequestMessage();
            apiRequest.Method = HttpMethod.Get;
            apiRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            apiRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", registerResponse.Token);

            await ApiAdvanceDungeonAsync(apiRequest);    
        }
        
        apiRequest = new HttpRequestMessage();
        apiRequest.Method = HttpMethod.Get;
        apiRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        apiRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", registerResponse.Token);

        await ApiEndDungeonAsync(apiRequest);  
    }

    public async Task<JwtResponse> ApiRegisterAsync()
    {
        // Arrange
        var registerDto = new App.Public.DTO.v1.Identity.Register()
        {
            Email = "test@test.test",
            Password = "Test1.test",
            UserName = "Test"
        };
        var jsonStr = System.Text.Json.JsonSerializer.Serialize(registerDto);
        var data = new StringContent(jsonStr, Encoding.UTF8, "application/json");
        var response = await _client.PostAsync("/api/v1/identity/Account/Register/", data);

        response.EnsureSuccessStatusCode();

        var requestContent = await response.Content.ReadAsStringAsync();

        var resultJwt = System.Text.Json.JsonSerializer.Deserialize<JwtResponse>(
            requestContent,
            new JsonSerializerOptions() {PropertyNamingPolicy = JsonNamingPolicy.CamelCase}
        );
        
        return resultJwt!;
    }
    
    public async Task GetApiDungeonListAsync(HttpRequestMessage apiRequest)
    {
        // Arrange
        apiRequest.RequestUri = new Uri("/api/v1/Dungeons", UriKind.Relative);
        
        // Act
        var apiResponse = await _client.SendAsync(apiRequest);

        // Assert
        apiResponse.EnsureSuccessStatusCode();

        var content = await apiResponse.Content.ReadAsStringAsync();
        
        
        var resultData = JsonSerializer.Deserialize<List<App.Public.DTO.v1.Dungeon>>(content, _jsonOptions);
        Assert.NotNull(resultData);
        Assert.Single(resultData);
        Assert.Equal(1, resultData!.First().Id);
    }

    public async Task ApiCreateCharacterAsync(HttpRequestMessage apiRequest)
    {
        // Arrange
        apiRequest.RequestUri = new Uri("/api/v1/Characters/User/TestC", UriKind.Relative);
        
        // Act
        var apiResponse = await _client.SendAsync(apiRequest);

        // Assert
        apiResponse.EnsureSuccessStatusCode();
        
        var requestContent = await apiResponse.Content.ReadAsStringAsync();

        var result = JsonSerializer.Deserialize<App.Public.DTO.v1.Character>(requestContent, _jsonOptions);
        Assert.NotNull(result);
        Assert.Equal("TestC", result!.Name);
    }
    
    public async Task ApiCreatePartyAsync(HttpRequestMessage apiRequest)
    {
        // Arrange
        apiRequest.RequestUri = new Uri("/api/v1/Game/Create/1", UriKind.Relative);
        
        // Act
        var apiResponse = await _client.SendAsync(apiRequest);

        // Assert
        apiResponse.EnsureSuccessStatusCode();

        var content = await apiResponse.Content.ReadAsStringAsync();
        
        
        var resultData = JsonSerializer.Deserialize<App.Public.DTO.v1.GroupStatus.GroupStatus>(content, _jsonOptions);
        Assert.NotNull(resultData);
        Assert.Equal(1, resultData!.DungeonId);
    }

    public async Task ApiAdvanceDungeonAsync(HttpRequestMessage apiRequest)
    {
        // Arrange
        apiRequest.RequestUri = new Uri("/api/v1/Game/Play", UriKind.Relative);
        
        // Act
        var apiResponse = await _client.SendAsync(apiRequest);

        // Assert
        apiResponse.EnsureSuccessStatusCode();

        var content = await apiResponse.Content.ReadAsStringAsync();
        var resultData = JsonSerializer.Deserialize<App.Public.DTO.v1.GroupStatus.GroupStatus>(content, _jsonOptions);
        Assert.NotNull(resultData);
        Assert.Equal(1, resultData!.DungeonId);
    }

    public async Task ApiEndDungeonAsync(HttpRequestMessage apiRequest)
    {
        // Arrange
        apiRequest.RequestUri = new Uri("/api/v1/Game/Play", UriKind.Relative);
        
        // Act
        var apiResponse = await _client.SendAsync(apiRequest);

        // Assert
        apiResponse.EnsureSuccessStatusCode();
        Assert.Equal(HttpStatusCode.NoContent, apiResponse.StatusCode);
    }
}