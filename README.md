# README #

# TODO #


#### Database migration and update
~~~sh
dotnet ef migrations add --project App.DAL.EF --startup-project WebApp Initial
dotnet ef migrations remove --project App.DAL.EF --startup-project WebApp
dotnet ef database update --project App.DAL.EF --startup-project WebApp
dotnet ef database drop --project App.DAL.EF --startup-project WebApp
~~~

#### Web Controllers
~~~sh
dotnet aspnet-codegenerator controller -name CharacterController -actions -m App.Domain.Character -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name GroupController -actions -m App.Domain.Group -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name EffectController -actions -m App.Domain.Effect -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name SkillController -actions -m App.Domain.Skill -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name ItemController -actions -m App.Domain.Item -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name SellListController -actions -m App.Domain.SellList -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name MerchantController -actions -m App.Domain.Merchant -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name MonsterController -actions -m App.Domain.Monster -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name MonsterListController -actions -m App.Domain.MonsterList -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name DungeonZoneController -actions -m App.Domain.DungeonZone -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name DungeonController -actions -m App.Domain.Dungeon -dc ApplicationDbContext -outDir Areas\Admin\Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
~~~

#### WebApi

~~~sh
cd WebApp
dotnet aspnet-codegenerator controller -name CharactersController     -m App.Domain.Character     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name DungeonsController     -m App.Domain.Dungeon     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name DungeonZonesController     -m App.Domain.DungeonZone     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name EffectsController     -m App.Domain.Effect     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name FriendsController     -m App.Domain.Friend     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name InventoryItemsController     -m App.Domain.InventoryItem     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name ItemsController     -m App.Domain.Item     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name SkillsController     -m App.Domain.Skill     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name SkillBooksController     -m App.Domain.SkillBook     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f

dotnet aspnet-codegenerator controller -name DropTableItemsController     -m App.Domain.DropTableItem     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name MonstersController     -m App.Domain.Monster     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name MonsterListsController     -m App.Domain.MonsterList     -actions -dc ApplicationDbContext -outDir ApiControllers -api --useAsyncActions  -f
~~~


#### Docker

~~~sh
docker build -t {image-name} .
docker tag {image-name} {new-name}:latest
docker login -u {username}
docker push {new-name}:latest
~~~